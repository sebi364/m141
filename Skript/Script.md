![](../x_res/tbz_logo.png)

# M141 - DB-Systeme in Betrieb nehmen


**Version 6.4**

*Autoren: Gerd Gesell ![](../x_res/bee.png) (Kellenberger Michael) 2024*

![](../x_res/Cartoon.png)

**Inhalt**

[TOC]

# 1 Einführung

## 1.1 Was ist eine Datenbank ?

Im üblichen Sprachgebrauch wird der Begriff Datenbank oft verwendet für die eigentlichen Daten, die Datenbankdateien, das Datenbanksystem oder den Datenbank-Client. Entsprechend gross kann die Verwirrung sein, wenn sich zwei Personen über Datenbanken unterhalten.

![](../x_res/Aufbau.png)

Abb. 1: Aufbau von Datenbanksystemen

Aufgabe eines DB-Systems ist nicht nur die sichere Speicherung der Daten, sondern auch die Verarbeitung von Befehlen zur Abfrage, Sortierung und Analyse vorhandener Daten, zum Eingeben, Ändern und Löschen von Daten.

-   Datenbank (DB) strukturierte Sammlung von Daten
-   DBMS: Database Management System, Programm für die Verwaltung der Datenbank(en), DB-Engine
-   Datenbasis: die eigentlichen Daten

## 1.2 DB-Server und DB-Client

Einfache Datenbanksysteme laufen standalone lokal auf einem Rechner für einen Benutzer, andere werden gleichzeitig von Tausenden von Benutzern beansprucht und verteilen die Daten über mehrere Rechner und Festplatten und genügen zum Thema Sicherheit und Verfügbarkeit hohen Ansprüchen. Die Rechner, die Dienstleistungen eines Datenbanksystems zur Verfügung stellen, werden Datenbank-Server genannt. Die Programme, die mit dem Datenbanksystem in Verbindung stehen, werden Datenbank-Client genannt.

Der Datenbankclient ist für den Anwender die Schnittstelle zum Datenbankserver. Die Clientsoftware kann vom einfachen Konsolenfenster bis zum komfortablen Dialogprogramm mit Formularen, Eingabehilfen und grafischer Unterstützung reichen, um nach Daten zu suchen, neue Daten einzugeben oder Systemparameter zu verwalten und zu kontrollieren.

![](../x_res/Architekturen.png)

Abb. 2: DB-System-Architekturen

In professionellen Applikationsumgebungen steht nicht nur ein Datenbankserver zur Verfügung, sondern werden mehrere spezialisierte (dedicated) Servertypen verwendet: Webserver, Netzwerkserver, Applikationsserver usw..

Aufteilung der Aufgaben in der Client Server-Architektur (*verteilte Intelligenz*):

-   Client (Frontend): am Arbeitsplatz, für anwendernahe Dialog- und Präsentationsaufgaben
-   Server (Backend): zentral, für gemeinsame Dienste, z.B. DB-Dienste

## 1.3 Datenbank-Modelle

Es ist noch offen, wie sich der Markt für nichtrelationale Datenbanken entwickeln wird. Aber seit einigen Jahren gibt es eine ganze Reihe an Produkten unter der Überschrift NoSQL – not only SQLentstanden, die sich in Bezug auf ihre Speichertechnologie in 4 Kategorien einteilen lassen:

-   Dokumentenorientierte Datenbanken (z. B. MongoDB und CouchDB; Cassandra und Redis?)
-   Grafdatenbanken (z. B. Neo4j)
-   Big-Table-Datenbanken (z. B. Doug Judd von Hypertable Inc., Big Table von Google)
-   Key-Value-Datenbanken (z.B. MySQL Gemini)

Welche Praxisrelevanz diese DB-Art in Zukunft erlangt ist nicht vorhersehbar, muss von innovativen Informatikerinnen aber weiter verfolgt werden.

| DB Art            | Struktur / Beispiel                                                                    |
|-----------------------------|----------------------------------------------------------------------------------------------------------------|
| Hierarchische DB            | hierarchische Baumstruktur, z.B. IMS (IBM)                                                                     |
| Netzwerk-DB                 | netzwerkförmige Struktur; z.B. IDMS (General Electric)                                                         |
| Relationale DB (RDB)        | DB-Typ mit Tabellen und Beziehungen, z.B. Oracle, DB2, MySQL, MariaDB ; SQL-Server, Access, Ingres, PostgreSQL |
| Objektorientierte DB (OODB) | enthält Informationen direkt in Form von Objekten, z.B. Object-Store, O2, Caché                                |
| XML-Datenbank               | speziell für XML-Dokumente, z.B. Tamino                                                                        |
| NoSQL                       | Siehe oben                                                                                                     |

Tab. 1.1: Datenbankarten

Das heute am häufigsten verwendete Datenbankmodell ist die relationale Datenbank. Rein objektorientierte Datenbanksysteme sind immer noch die Ausnahme. Allerdings bemühen sich die meisten Anbieter relationaler Datenbanksysteme darum, die OO-Welt in der relationalen Umgebung einzubetten. Das Ergebnis sind dann sogenannte objektrelationale Datenbanken. Eine gute Übersicht was aktuell auf dem Markt passiert, finden Sie hier: <https://db-engines.com/en/ranking> - interessant ist auch die Methode, die zur Statistik führt. Diese ist ebenfalls auf der Seite beschrieben.

---

---

![Picto](../x_res/XAMPP.jpg)

# 2 MySQL/MariaDB – Umgebung XAMPP und MySQL-Worbench

## 2.1 Entstehung MariaDB und MySQL

Grundsätzlich öden mich Geschichtsabhandlungen in technischen Dokumenten eher an. Um den Unterschied zwischen den beiden relationalen DB-Systemen MariaDB und MySQL zu verstehen, sind jedoch ein paar Kenntnisse hilfreich. Wer sich für detaillierte Entwicklungsschritte interessiert, kann dies zum Beispiel unter <https://de.wikipedia.org/wiki/MySQL> nachlesen. Seit einigen Jahren läuft übrigens auch Wikipedia unter MariaDB und nicht mehr unter MySQL

Entwicklung ca. ab 1994 unter Michael Widenius. Im Februar 2008 wird es dann von Sun Microsystems übernommen (Kaufpreis ca. 1 Mrd. Dollar). Anfang 2010 wird Sun dann von Oracle aufgekauft. Logischerweise ist die Open-Source-Entwicklerszene davon nicht riesig begeistert. Michael Widenius beginnt bereits kurz vorher einen Fork von MySQL Version 5.5 mit dem Namen MariaDB zu starten und unter der GPL zu entwickeln.

Was die Geschichte betrifft, reicht das – finde ich! Zu Beginn haben sich die beiden Systeme nicht wesentlich in der Entwicklung unterschieden. In den letzten Monaten ist MariaDB jedoch in vielen Bereichen vorangezogen. Einen gut lesbaren Bericht zu den Neuentwicklungen findet ihr unter anderem hier: <https://www.informatik-aktuell.de/betrieb/datenbanken/mariadb-und-mysql-vergleich-der-features.html>.

## 2.2 MariaDB und Modul 141

Für dieses Modul ist der Unterschied zwischen MariaDB und MySQL unwesentlich. Erst für ein grösseres Projekt sind die Software-Architektur, Hochverfügbarkeit und Leistungsoptimierung dann relevant. Auch in den aktuellen Installationen von XAMPP werden die Bezeichnungen beider DBMS (=Datenbankmanagement-Systeme) recht locker parallel verwendet. Schon beim Installationsprozess gibt es eine Checkbox MySQL und gemeint ist MariaDB.

Auch in Dokumentationen, Anleitungen und im XAMPP-Controlpanel werden diese beiden Namen nicht immer klar getrennt. Für den Beginner ist es da fast wichtiger auf eine lauffähige PHP-Version zu achten. Die aktuellsten XAMPP-Versionen enthalten PHP 7. Wer also viel mit PHP arbeiten möchte sollte daher lieber überprüfen, ob die gewählte Software bereit unter PHP 7 lauffähig ist und sonst halt die XAMPP-Version mit PHP 5.6 verwenden.

## 2.3 Arbeitsumgebung installieren

Sie sollten auf Ihrem Rechner oder einer virtuellen Umgebung XAMPP oder die einzelnen Komponenten – mindestens jedoch den Apache Webserver und eine SQL-Server-Datenbank installiert haben. Die aktuelle Version finden Sie unter <https://www.apachefriends.org/de/index.html> – inklusive Dokumentation und Anleitung. Die Installation ist ausreichend dokumentiert &#8594; konsultieren Sie also das [Manual](https://www.apachefriends.org/docs/). 

Ausserdem benötigen Sie die MySQL-Workbench: <https://dev.mysql.com/downloads/workbench/> und phpMyAdmin (letzteres ist im XAMPP-Bundle bereits enthalten).


![](../x_res/Komponenten.jpeg)``

Abb. 3: XAMPP Setup Auswahl

> **Achtung:** XAMPP ist zur Entwicklung konstruiert. Daher sind die Sicherheitsvorkehrungen bewusst ausgeschaltet, um bei der Entwicklung und beim Testen (gute Informatiker machen das!) ohne zusätzliche Hürden arbeiten zu können. Ein produktiver Server sollte niemals mit dem XAMPP-Bundle aufgesetzt werden. Im Juli 2011 wurde das Observationssystem „PATRAS“ der deutschen Bundespolizei vom Hacker-Netzwerk „no name crew“ (kurz „nn-Crew) gehackt. Ziemlich peinlich, denn dieses System enthielt sensible Daten und wurde mittels eines XAMPP-Bundles auf einem Server betrieben.

## 2.4 Datenbankserver-Daemon mysqld.exe

Hinter dieser Datei verbirgt sich das Datenbankmanagementsystem. Der Server muss laufen, damit Sie von irgendeinem Client mit der Datenbank kommunizieren können – sei es als Prozess oder als Service.

## 2.5 Kommandozeilen-Client mysql.exe

Für den Zugriff auf den Datenbankserver gibt es viele Clientprogramme. Das einfachste Client-Programm ist die Kommandozeilen-Applikation *mysql.exe*. Dabei handelt es sich um ein einfaches Konsolenfenster, in dem SQL-Befehle und verschiedene Kommandos abgesetzt werden können. Diese rudimentäre Methode wird durch eine Vielzahl an grafischen Tools vereinfacht. Für das Verständnis der grafischen Tools sollten Sie zu Beginn die Übungen mit dem Konsolenfenster durchführen. Die Grafiktools machen ja nichts anderes, als SQL-Kommandos aus einer animierten Umgebung abzusetzen und deren Resultate optisch ansprechend darzustellen.

![](../x_res/cmd.png)

Abb. 4: cmd Konsolenfenster

![](../x_res/PowerShell.png)

Abb. 5: PowerShell als Commandline-Tool

Zum Start des MySQL-Konsolen-Clients führen Sie das Programm mysql.exe von der Betriebssystem-Konsole oder irgendeinem anderen Commandline-Tool aus. Dazu öffnen wir zum Beispiel das Commandline-Fenster «cmd» des Betriebssystems. In den Abbildungen sehen Sie, wie Sie in das richtige Verzeichnis navigieren können und dort das Programm aufrufen. Wenn Sie nur mysql ohne weitere Parameter aufrufen, starten Sie den Client mit stark eingeschränkten Rechten (Gastrechte) und sehen nur wenige Datenbanken. Wenn Sie die Parameter

-   –u (steht für User – also zu Beginn mal als root anmelden) und
-   –p (steht für Passwort – ist beim User root standardmässig leer – also enter drücken oder -p weglassen)

hinzufügen, haben Sie mehr Rechte und sehen alle vorhandenen Datenbanken. Verwenden Sie für den Test das SQL-Statement `SHOW DATABASES;`

Ergebnisse von SQL-Statements, die einen grösseren Output generieren sind im Konsolenfenster nicht sehr übersichtlich dargestellt und es gibt auch nicht viele Formatierungsmöglichkeiten – es ist ja kein GUI. Hilfreich ist in dem Zusammenhang der Modifier `\G`: ersetzen Sie das Semikolon am Ende eines Statements mit diesem Modifier und die Ausgabe ist deutlich übersichtlicher formatiert.

Diese Graphik-Tools werden laufend weiterentwickelt und es kommen immer wieder neue Produkte auf den Markt. In den letzten Jahren hatten sich die MySQL GUI Tools als zuverlässige Werkzeuge etabliert und sind vor einiger Zeit in der MySQL-Workbench zusammengefasst worden. Im Kern verwenden diese Tools natürlich auch nur die SQL-Statements und .exe-Dateien, die wir genauso gut manuell im Konsolenfenster aufrufen können.

## 2.6 MySQL-Workbench

Die MySQL Workbench hat sich in den letzten Jahren als die zentrale Plattform durchgesetzt, die viele – mittlerweile veraltete - Tools vereint. Sie beinhaltet alle Möglichkeiten vom grafisch unterstützten Entwurf, über die DB-Erstellung und die Serveradministration. Für die Arbeit in diesem Modul ist die Workbench optional. Die Erklärungen in diesem Skript möchte ich aber so gering wie möglich halten. Denn es gibt eine sehr gute Dokumentation im Internet: <http://www.mysql.de/products/workbench/> und ausserdem wechseln Benutzeroberflächen bei jedem neuen Release ihr Erscheinungsbild. Online-Dokumentationen sind daher viel zuverlässiger. Je nach Installation und Betriebssystem gibt es zusätzlich Unterschiede. Also nicht verwirren lassen, sondern ausprobieren, Manuals und Foren verwenden und immer mal wieder zu den Nachbarn schauen.

![](../x_res/Workbench.png)

Abb. 6: Startfenster der Workbench Version 6.x

Die Möglichkeiten zur „Server Administration“ werden wir näher betrachten. Bereits kennengelernt haben wir die Möglichkeiten der Datenmodellierung. Sie sind aber immer wieder relevant, um in der Workbench ein Datenmodell grafisch zu erstellen und dieses Modell via *forward-engineering*, *backward-engineering* oder *synchronization* mit der realen Datenbank abzugleichen.

> Nur wenn der DB-Server als Service läuft, kann er via MySQL-Workbench gestartet und gestoppt werden. Siehe: MySQL als Service installieren

## 2.7 phpMyAdmin

Ein Tool mit sehr hoher Verbreitung ist phpMyAdmin. Die Bezeichnung php im Namen bedeutet nicht, dass wir damit PHP administrieren, sondern lediglich, dass dieses Tool in PHP realisiert wurde. Der häufigste Einsatz ist die Erstellung und Verwaltung von Datenbanken. Viele Web-Provider, die Webspace inklusive MySQL-Datenbank zur Verfügung stellen, bieten ihren Kunden dieses Tool zur Datenbankverwaltung an. Daher ist es trotz der Workbench sinnvoll von der phpMyAdmin-Oberfläche arbeiten zu können.

So sind auch im phpMyAdmin umfangreiche Serverüberwachungsfunktionen enthalten (siehe eingekreiste Funktionen in Abb. 7: Serverfunktionen von phpMyAdmin). Mit welchen Tools und Komponenten Sie am Ende arbeiten, hängt von Ihren Berechtigungen und Ihrer Rolle ab. Je nach Rechtesituation sind bestimmte Funktionen deaktiviert. Sie können zum Beispiel bei den meisten Webprovidern mit fertig installierter Datenbank die DB Namen nicht selber definieren.

![](../x_res/phpMyAdmin.png)

Abb. 7: Serverfunktionen von phpMyAdmin

Die Erstellung der Datenbanken und Tabellen und deren Pflege sind im phpMyAdmin ohne Kenntnis von SQL möglich. Es können auch Daten erfasst und modifiziert werden (Funktion „Einfügen“ – wenn eine konkrete Tabelle ausgewählt wurde). In der Praxis wird das aber meistens über spezielle Applikationen gemacht.

![](../x_res/Datenbankpflege.png)

Abb. 8: Funktionen zur Datenbankpflege

## 2.8 MySQL Dokumentation

Die Dokumentation von MySQL und MariaDB ist umfangreich, verständlich und teilweise auch auf Deutsch vorhanden. Online finden Sie die Informationen unter: <https://www.mysql.com/> und <https://mariadb.com>. Siehe bei Bedarf auch auf dem TBZ-Wiki für die aktuellen Links.

---
---

## 2.9 Zeichenkodierung

Wenn man mit dem Computer und dem Internet arbeitet begegnet man des Öfteren der Möglichkeit, die Zeichencodierung auswählen zu können, so z.B. im Browser oder im Email-Programm. Die Wenigsten kennen überhaupt den Unterschied, daher stelle ich auf dieser Seite einmal die verschiedenen Codierungen vor.

Zeichensatzcodierungen sind Tabellen, die bestimmten Byte-Werten konkrete Zeichen zuordnen. Es sind also nur „Vereinbarungen“ welches Zeichen durch welche Bitkombination dargestellt wird. Zu Beginn der Computergeschichte waren dies nur alphanumerische Zeichen, heute umfassen Zeichensätze unter anderem auch Runen oder kyrillische Zeichen.

-   ASCII (Veraltet): Der ASCII (American Standard Code for Information Interchange) entstand in den 60er Jahren, ist 7-Bit codiert, nutzt allerdings nur die ersten 128 Byte. Er umfasst alphanumerische, Sonder- und Steuerzeichen und ist von den verwendeten Zeichen her auf die englische Sprache ausgerichtet.  
    Neben dem Standard ASCII-Code gibt es noch den erweiterten ASCII-Code, bei dem nicht nur die ersten 128 Byte genutzt werden, sondern auch noch die "letzten" 128 Byte. Die Varianten des erweiterten ASCII-Codes nennt man *ANSI* und *ISO-8859*.
-   ANSI: ANSI ist eine Erweiterung des ASCII-Codes, er ist genormt vom **A**merican **N**ational-**S**tandards **I**nstitute (daher der Name) und hat sich als Standard auf den Windows- und Macintosh-Betriebssystemen durchgesetzt.
-   ISO-8859: Bei diesem Zeichensatz handelt es sich erstmal um eine Normserie mit länderspezifischen Zeichensätzen der ISO (International Organization for Standardization). Der Zeichensatz ISO-8859-1 (Latin) ist der erste von insgesamt 16 ländereigenen Zeichensätzen und ist für Westeuropa gemacht, beinhaltet also z.B. deutsche, französische oder skandinavische Sonderzeichen.
-   Unicode: Entstanden Ende der 80er Jahre mit dem Ziel alle Sprachen der Welt in einem Zeichensatz zu vereinen, ist der Unicode der größte und umfassendste Zeichensatz. Anfangs 16-Bit codiert, allerdings 2001 umgestellt auf 32-Bit beinhaltete Unicode 4 im Jahre 2003 ca. 100000 verschiedene Zeichen. Der Unicode vereint tote wie auch lebende Sprachen, so sind z.B. auch Runen Bestandteil.
-   UTF-8 (Standard): UTF-8 ist die häufigste verwendete Kodierung für Unicode-Zeichen mit hohem ASCII-Anteil – die ersten 128 Zeichen entsprechen der ASCII-Tabelle. Die Abkürzung bedeutet „UCS Transformation Format 8-Bit“. Die Abkürzung UCS wiederum steht für Universal Character Set. Dieser Zeichensatz ist besonders im Internet weit verbreitet. Dies ist allerdings keine Verpflichtung, sondern nur eine Empfehlung oder "Bitte". Und das macht das Informatikerleben nicht einfacher. Aber immerhin: im Mai 2017 verwendeten 89,0 % aller Websites UTF-8, im März 2018 bereits 91,4 % und im März 2020 95%. Für aktuelle Daten können Sie hier nachschauen: <https://w3techs.com/technologies/history_overview/character_encoding>

## 2.10 Byte Order Mark - BOM (Siehe M114)

Beim Einsatz von UTF-8 *kann* als Unterscheidung zum ISO-8859-Zeichensatz, bei UTF-16 oder UTF-32 *muss* die Reihenfolge der Bytes angegeben werden (Little- oder Big-Endian). Dazu gibt es ein "BOM" (Byte Order Mark) ganz am Anfang der Datei. Das BOM besteht aus der Bytesequenz "FF FE" bei UTF-16/32. Bei UTF-8 KANN (zur Unterscheidung von ISO8859-Zeichensätzen) das BOM "EF BB BF" eingesetzt werden. Einige ältere Texteditoren und MySQL interpretieren leider diese Zeichenfolge meist als ISO-8859-Zeichen ï»¿ und sein für Kompatibilitätsprobleme verantwortlich.

Gemeinerweise ist diese Markierung in normalen Texteditoren nicht sichtbar, da den Editoren bekannt ist, dass es sich nur um Metadaten handelt. Erst wenn Sie die Datei mit einem Hex-Editor anschauen, erkennen Sie den Unterschied zwischen den Dateien mit und ohne BOM.

![](../x_res/oBOM.png)

Abb. 9: Datei ohne BOM im Hex-Editor

![](../x_res/mBOM.png)

Abb. 10: Datei mit BOM im Hex-Editor

Einfache Editoren erlauben keine detaillierten Angaben zur Kodierung bei der Speicherung. Beim Notepad++ finden wir die nötigen Einstellungen im Ordner „Kodierung“. Wir müssen die Dateien für unsere Anwendung ohne BOM abspeichern.

![UTF-8 ohne BOM in Notepad++](../x_res/oBOM_NP.png)

Abb. 11: Datei ohne BOM im Notepad++: Kodierungseinstellungen für die Speicherung



## 2.11 Zeichensätze und Sortierfolgen im Allgemeinen

(entnommen dem MySQL-Handbuch – Version 5.1)

Ein *Zeichensatz* ist eine Menge mit Symbolen und Kodierungen. Eine *Sortierfolge* ist ein Regelsatz für den Vergleich von Zeichen in einem Zeichensatz. Folgendes Beispiel, welches einen imaginären Zeichensatz verwendet, soll den Unterschied verdeutlichen.

Angenommen, wir haben ein Alphabet mit vier Buchstaben: ‘**A**’, ‘**B**’, ‘**a**’, ‘**b**’. Jedem Buchstaben weisen wir nun eine Zahl zu: ‘**A**’ = 0, ‘**B**’ = 1, ‘**a**’ = 2, ‘**b**’ = 3. Der Buchstabe ‘**A**’ ist ein Symbol, die Zahl 0 die **Kodierung** für ‘**A**’. Die Kombination aller vier Buchstaben und ihrer Kodierungen bildet den **Zeichensatz**.

Angenommen, wir wollen zwei String-Werte ‘**A**’ und ‘**B**’ miteinander vergleichen. Die einfachste Möglichkeit, dies zu tun, besteht in einem Blick auf die Kodierungen: 0 für ‘**A**’ und 1 für ‘**B**’. Da 0 kleiner als 1 ist, sagen wir, dass ‘**A**’ kleiner als ‘**B**’ ist. Was wir gerade getan haben, war die Anwendung einer Sortierfolge für unseren Zeichensatz. Die Sortierfolge ist eine Menge von Regeln – wenn auch in diesem Fall nur eine Regel: „Vergleiche die Kodierungen“. Diese einfachste aller möglichen Sortierfolgen nennen wir *Binärsortierung*.

Was aber, wenn wir ausdrücken wollen, dass Klein- und Großbuchstaben äquivalent sind? Dann hätten wir schon zwei Regeln: (1) Betrachte die Kleinbuchstaben ‘**a**’ und ‘**b**’ als äquivalent zu den entsprechenden Großbuchstaben ‘**A**’ und ‘**B**’. (2) Vergleiche die Kodierungen. Dies ist eine *Sortierfolge ohne Unterscheidung der Groß-/Kleinschreibung*. Sie ist ein kleines bisschen komplexer als eine Binärsortierung.

Im wirklichen Leben umfassen die meisten Zeichensätze viele Zeichen: nicht nur ‘**A**’ und ‘**B**’, sondern ganze Alphabete – manchmal sogar mehrere Alphabete oder fernöstliche Schreibsysteme mit Tausenden von Zeichen – sowie viele Sonder- und Interpunktionszeichen. Auch für Sortierfolgen gibt es im wirklichen Leben viele Regeln, die nicht nur die Behandlung der Groß-/Kleinschreibung angeben, sondern auch, ob Akzente und Tremata (die Punkte etwa auf den deutschen Umlauten Ä, Ö und Ü) unterschieden und wie Folgen aus mehreren Zeichen zugeordnet werden (beispielsweise die Regel, dass bei einer der beiden deutschen Sortierfolgen die Gleichung ‘**Ö**’ = ‘**OE**’ gilt).

MySQL erlaubt Ihnen;

-   das Speichern von Strings in einer Vielzahl von Zeichensätzen,
-   das Vergleichen von Strings unter Verwendung einer Vielzahl von Sortierfolgen,
-   das Mischen von Strings verschiedener Zeichensätze oder Sortierfolgen auf demselben Server, in derselben Datenbank oder sogar derselben Tabelle,
-   die Angabe von Zeichensatz und Sortierfolge auf beliebiger Ebene.

In dieser Hinsicht ist MySQL den meisten anderen Datenbanksystemen weit überlegen. Allerdings müssen Sie, um diese Funktionen effizient nutzen zu können, wissen, welche Zeichensätze und Sortierfolgen verfügbar sind, wie Sie die Standardeinstellungen ändern und wie diese das Verhalten von String-Operatoren und -Funktionen beeinflussen.

## 2.12 Kollation oder Collation

Die Angabe einer Kollation definiert die Sortierung innerhalb der Spalten. Einfache Geister könnten jetzt meinen, dass das ja kein Thema sei, da eine Sortierung durch das Alphabet vorgegeben ist. Aber was ist mit den Umlauten der deutschen Sprache und den Sonderzeichen, Akzenten usw. der anderen Sprachen? Auch Gross- und Kleinschrift muss definiert werden, ob es bei der Sortierung berücksichtigt werden soll.

Wer im MySQL eine neue Datenbank anlegt, hat als Standard-Kollation latin1\_swedish\_ci. Der Grund ist nicht technisch, sondern geschichtlich, da die ursprüngliche Firma hinter MySQL eine schwedische Firma namens MySQL AB war.

Die deutsche Norm DIN 5007-1 beschreibt unter dem Titel *„Ordnen von Schriftzeichenfolgen (ABC-Regeln)“* das Sortieren.

| **DIN 5007 Variante 1** <br> (für Wörter verwendet, etwa in Lexika)  | **DIN 5007 Variante 2** <br> (spezielle Sortierung für Namenslisten, etwa in Telefonbüchern)   |
|----------|----|
|  - *ä* und *a* sind gleich <br> - *ö* und *o* sind gleich  <br> - *ü* und *u* sind gleich <br> - *ß* und *ss* sind gleich | - *ä* und *ae* sind gleich <br> - *ö* und *oe* sind gleich  <br> - *ü* und *ue* sind gleich  <br> - *ß* und *ss* sind gleich  |

Ob Gross- und Kleinschrift beachtet wird, ist an der Endemarkierung \_cs oder \_ci zu erkennen: case-sensitive oder case-insensitive. Theoretisch können wir innerhalb einer Datenbank die Tabellen individuell konfigurieren und innerhalb der Tabellen sogar die Spalten mit einer individuellen Kollation versehen. Allerdings empfiehlt es sich bei produktiven Systemen davon abzusehen, denn es müssen bei JOIN-Verknüpfungen charset-encodings durchgeführt werden.

Nun gibt es keine richtigen oder falschen Kollationen. Die Auswahl hängt immer vom Einsatz ab. Für unsere Zwecke haben wir bisher latin1\_german1\_ci oder utf8\_general\_ci empfohlen. Nun ist es leider so, dass die MySQL-Entwickler für utf-8 nur drei statt 4 Byte verwenden. Für die meisten Zeichen reicht das aus – aber das heisst nicht für alle. Speziell Emoticons benötigen häufig 4 Byte. 
  
> Daher sollten Sie utf8**mb4**\_unicode\_ci verwenden.

 ---
 ---

# 3 DB-Server und XAMPP

Wir können MySQL als eigenständigen Server installieren. Es gibt aber ein Variante, die für die Entwicklung einfacher ist und die wir uns hier genauer anschauen wollen: XAMPP. Dabei handelt es sich um ein so genanntes Bundle aus den Produkten **A**pache als Webserver, **M**ySQL als Datenbank und den Skriptsprachen **P**erl und **P**HP. Das **X** stand "mal" nur für die Windows-Version ergo gab es auch eine **L**AMPP und sogar eine **M**AMPP Version (Linux und Mac). Aktuell heissen alle Bundles XAMPP!

## 3.1 Start via cmd – Command Line Tool

Der Server kann direkt von der Betriebssystem-Konsole gestartet werden. Mit der Eingabe   
`mysqld --verbose –-help` können Sie sich sämtliche Optionen und konfigurierbare Systemvariablen anzeigen lassen (oder Sie schauen im MySQL-Manual nach).

> Falls Sie durch eine Übung oder aus Versehen die Berechtigungen des root-Users verstellt haben und keine Berechtigungen mehr haben diese zu ändern, können Sie den Datenbankserver mit dem Befehl   
`mysqld --skip-grant-tables`
aufstarten. Damit haben alle Benutzer alle Rechte. Es ist also eine sehr heikle Operation. Aber so haben Sie wieder Zugriff auf die Berechtigungsdatenbank und können die Berechtigungen korrigieren. Merken Sie sich diesen Befehl!

## 3.2 Start via Control Panel

Wenn Sie auf Ihrem Rechner XAMPP installiert haben, finden Sie im Verzeichnis von xampp diverse Startroutinen. Eine einfache Startmöglichkeit ist das XAMPP-Control-Panel. Skeptische Geister können den Erfolg auch im Task-Manager überprüfen. Manchmal liegt das Problem beim Start auch daran, dass der MySQL-Server bereits gestartet ist. Ein zweiter Start schlägt dann fehl. Sie sollten dieses Verhalten mit den verschiedenen Start-Prozeduren testen.

> Das XAMPP-Controlpanel sollten Sie grundsätzlich als Administrator starten, das für die Windows-Dienste Zugriffe auf die Registry nötig sind. Für den Start als Service und weitere Optionen also unumgänglich.

![control_panel](../x_res/ControlPanel.png)

Abb. 12: XAMPP-Control-Panel und Windows Prozessliste

Es lohnt sich mit den verschiedenen Startoptionen zu experimentieren. Im Control Panel ist der Status jeweils sichtbar. Beenden Sie den Prozess zum Beispiel im Task-Manager und starten Sie den Server direkt indem Sie die Datei `mysqld.exe` im `bin`-Verzeichnis ausführen.

## 3.3 MySQL als Service installieren

Sie können MySQL auch als Service installieren (markieren der Checkbox Svc im Control-Panel). Der Server wird dann in die Diensteliste der Computerverwaltung eingetragen und kann so konfiguriert werden, dass er bei Systemstart automatisch gestartet wird. 

![](../x_res/MySQL-Dienst.jpg)

Abb. 13: Start mit Admin-Rechten und als Service

In diesem Fall kann der Server auch über den Dienst-Manager von Windows gestartet und gestoppt werden oder aus einer **Windows-Konsole** (Cmd / Powershell) heraus:

```BATCH
C:>NET STOP mysql

The MySql service is stopping.
The MySql service was stopped successfully.

C:>NET START mysql

The MySql service is starting.
The MySql service was started successfully.
```

![](../x_res/ccf0dc5fb35e1d8062650d06c260c5dd.jpg)

## 3.4 Administration des Datenbank-Servers

Die puristische Form ist das bereits erwähnte mysql-Konsolenprogramm. Wenn wir mit den entsprechenden Rechten ausgestattet sind, können wir auf sämtliche GUI-Tools verzichten und alle Einstellungen und Kontrollen von hier aus durchführen. Die grafischen Werkzeuge machen nichts anderes, als diese Befehle umzusetzen, nur verpacken sie alles in ein unterstützendes grafisches Outfit.

Das Register Environment enthält einige wichtige Angaben über den MySQL-Server.

| Server-Informationen |                                                          |
|----------------------|----------------------------------------------------------|
| Host Name            | Name des Server-Rechners,                                |
| OS Plattform         | Betriebssystem des Server-Rechners, z.B. Windows NT      |
| Local IP Address     | Netzwerkadresse des Server-Rechners, z.B. 139.79.113.221 |
| Server Info          | Version des Servers mysqld, z.B. 4.0.12                  |
| Uptime               | Betriebszeit seit dem letzten Start,                     |

## 3.5 Server-Status abfragen

Gestartet wird der Monitor in einem CMD-Fenster mit dem Befehl **mysql**. Damit wird eine Verbindung (connection) zum DB-Server erstellt.

![](../x_res/status.png)

Abb. 14: Der Befehl "status" im mysql.exe - Konsolenfenster

| Status-Informationen |                                              |
|----------------------|----------------------------------------------|
| Current database     | Aktuell gewählte Datenbank, z.B. mybooks     |
| Current user         | User- und Hostname (Default: ODBC@localhost) |
| Server Version       | Version der Server-Software,                 |
| Server characterset  | Zeichensatz des DB-Servers, z.B. latin1      |
| TCP port             | vom Server verwendetes Port (Default: 3306)  |
| Uptime               | Betriebszeit seit dem letzten Serverstart    |

## 3.6 Die Optionsdateien my.ini (oder my.cnf)

Beim Start des MySQL Servers (unter Windows), werden diverse Verzeichnisse nach Optionseinstellungen durchsucht: die Datei `my.ini` (oder die Datei *my.cnf* bei Linux/Unix). 

|Config Dateie(en)   | Zweck                                                                                                                                                                                          |
|--------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `System Windows Directory\my.ini` | Global |
| `Windows Directory\my.ini` | 	Global | 
| `C:\my.ini`	 | Global | 
| `INSTALLDIR\my.ini`	 | Server | 
| `INSTALLDIR\data\my.ini`	 | Server | 
| `%MARIADB_HOME%\my.ini` | 	Server (from MariaDB 10.6) | 
| `%MYSQL_HOME%\my.ini`	 | Server
| defaults-extra-file	 | File specified with --defaults-extra-file, if any

- *Quelle*: <https://mariadb.com/kb/en/configuring-mariadb-with-option-files/> <br> 
  *Siehe auch* <https://dev.mysql.com/doc/refman/8.0/en/option-files.html>

*Hinweis: `INSTALLDIR` ist der Installationspfad des MySQL-DBMS. (`C:\xampp\mysql`)

Wer wissen möchte, welche Optionsdateien in welcher Reihenfolge von seinem System verwendet werden, kann im CMD-Fenster folgendes eingeben: 
 
`C:\> mysqld --verbose --help` 

(im richtigen Verzeichnis natürlich) und dann ganz schnell lesen! Gleiches geht auch für andere MySQL-Programme (z.B. **mysql.exe**). Beim Lesen der `.ini`-Dateien werden dann die **[Abschnitte]** ausgegeben, die ausgeführt werden, denn nicht alle Programme benötigen die gesamte Optionsdatei.

Bei fehlenden `.ini`-Dateien wird keine Fehlermeldung ausgegeben. Mind. eine Optionsdatei muss es aber geben ...


![2Do](../x_res/ToDo.png) Untersuchen Sie ihr System und finden Sie heraus, welche .ini-Dateien nach der Installation tatsächlich existieren! 

- Legen Sie dazu eine Kopie/Screenshot des angezeigten Startprozesses für mysql.exe und mysqld.exe in ihr Lernportfolio. 
- Lesen Sie den Kommentar in der Optionsdatei, die sie gefunden haben.
- Listen Sie die `[Abschnitte]` aus der `.ini`-Datei heraus und begutachten Sie die Inhalte. Wir werden Einträge darin bearbeiten...

---
---

# 4 SQL

Es ist nicht das Ziel dieses Kapitels, dass Sie die Datenbanksprache SQL (structured query language) bis ins Detail beherrschen. Allerdings sollten Sie einen Überblick über die Befehlsgruppen haben, einige wichtige Befehle kennen und Informationsquellen kennen, um sich bei Bedarf informieren zu können. Detailliertere Informationen zum Thema SQL – speziell zum SELECT-Befehl – finden Sie im separaten SQL-Skript oder im MySQL-Online-Manual. Wenn Sie mit phpMyAdmin arbeiten, werden Ihnen von sämtlichen Aktionen, die Sie grafisch auslösen, die entsprechenden SQL-Statements angezeigt. Auch auf diese Weise können Sie SQL lernen!

**Datenbanksprache SQL und Befehlsgruppen** 
                                                                                              
| SQL    | Datenbanksprache; Structured Query Language   |
|----------------------------|--------------------|
| **DDL** <br> (Data Definition  Language)         | DB-Objekte definieren, z.B.  <br> - CREATE db-objekt <br> - ALTER db-objekt <br> - DROP db-objekt |
| **DCL** <br> (Data Control  Language)            | Zugriffsrechte verwalten, z.B.   <br> - GRANT <br> - REVOKE                                     |
| **DML** <br>(Data Manipulation  Language)   | Daten abfragen, einfügen, ändern löschen, z.B.: SELECT INSERT UPDATE DELETE                     |
| **TCL** <br> (Transaction  Control  Language)    | <br> - TRANSACTION <br> - COMMIT <br> - ROLLBACK                                                                     |

Tab. 4.1: Befehlsgruppen von SQL

Eine gute Website, um SQL zu lernen oder sich die Syntax anzuschauen, ist [www.w3schools.com](http://www.w3schools.com). Das Internet ist recht voll von Anleitungen und Hilfen zu diesem Thema. Daher gibt es hier jetzt nichts ...

## 4.1 SQL-Skripte (Batch Mode)

Bisher haben wir SQL-Befehle entweder im Konsolenfenster oder im SQL-Fenster von phpMyAdmin eingegeben. Sie können SQL-Statements aber auch im Stapelbetrieb (Batch Mode) benutzen. Dafür schreiben Sie die Befehle, die Sie ausführen wollen, in eine Textdatei, und teilen dem Datenbankserver mit, die Befehle aus dieser Datei zu lesen und auszuführen. Gründe für die Verwendung von Skripts

-   Wiederholtes Ausführen der gleichen Befehle, z.B. jeden Tag oder Woche
-   Fehler in einzelnen Befehlen können einfach korrigiert werden, ohne alles erneut eintippen zu müssen
-   Erstellen von Befehlsfolgen durch Kopieren u. Editieren ähnlicher Skripts
-   Verteilen des Skripts an andere Personen, damit diese die Befehle laufen lassen können
-   Ausführung von Skripts in einer Testumgebung bevor die gleichen Skripte in einer produktiven Umgebung eingesetzt werden.

Wichtige Befehle im Zusammenhang mit Batch-Verarbeitung (**CMD**):

| CMD Befehl (evtl. Pfad angeben)| Kommentar |
|----|---|
| mysql -u root -p db **< pfad\script.sql** | Skript mit SQL-Statements auf die angegebene DB ausführen <br> (Dateiendung ist egal!) | 
| mysql -u root -p db \< pfad\script.sql **> pfad\ausgabe.txt** | Skriptausführung mit Ausgabe in eine Textdatei |

> Achtung bei verwendung der Windows Powershell Konsole: WPS kennt "\<" nicht: Verwenden Sie den Cmdlet GET-CONTENT:
<br>   **gc pfad\script.sql | mysql -u root -p db > pfad\ausgabe.txt**

Wenn Sie bereits im Konsolenklient der Datenbank arbeiten (mysql.exe), können Sie die genannten Umleitugen nicht anwenden. Sie können dennoch Skriptdateien vom SQL-Prompt ausführen:

| MySQL Befehl | Kommentar |
|----|---|
| mysql\> **source** pfad\script.sql | script.sql muss SQL-Statements enthalten |
| mysql\> **\.** pfad\script.sql | ist identisch mit source … |

Damit kennen Sie die wesentlichen Befehle, um den Inhalt von Script-Dateien auszuführen, die SQL-Statements enthalten.

## 4.2 Backup der Datenbank erstellen

Mit einem Backup wird Ihre Datenbank mit allen eingetragenen Daten und der Tabellenstruktur gesichert. Das Programm *mysqldump* erzeugt eine Liste aller SQL-Befehle, um die gesamte Datenbank exakt wiederherzustellen. Sie enthält die DDL-Befehle zur Tabellenerzeugung (CREATE…) und die DML-Befehle für die Datenerzeugung (INSERT…). Diese Informationen werden von mysqldump in eine beliebige Datei umgeleitet.

`C:\> mysqldump Hotel > pfad\hotel-backup.sql`

mysqldump wird nicht innerhalb des SQL-Monitors ausgeführt, sondern vom CMD-Fenster – es ist eine ganz gewöhnliche *.exe-Datei, die mit Parametern aufgerufen werden kann.

> Wenn der Datenbankzugriff nur für bestimmte Benutzer freigegeben ist, müssen beim Aufruf des mysqldump-Befehls Benutzername und die Passworteingabe verlangt werden:

> `C:\> mysqldump -u name -p hotel > pfad\hotel-backup.sql`


Natürlich gibt es auch den Weg zurück: von einer Textdatei (zum Beispiel die gerade erzeugte) in eine Datenbank. Es geht zwar auch über phpMyAdmin, aber wir können jedes funktionierende SQL-Skript auch direkt von der Kommandozeile her in eine bestehende Datenbank einlesen:

> Die Datenbank "Hotel" muss dabei bereits angelegt sein: 

> `mysql> CREATE DATABASE hotel;` <br>
`mysql> USE DATABASE hotel;`


`mysql> SOURCE pfad\hotel-backup.sql;`

Damit werden **alle Tabellen der DB neu erstellt** und alle Daten eingetragen – oder kurz: das Skript wird ausgeführt.   


**Das Backup aktiver online-Datenbanken ist nicht ganz unproblematisch, denn Datensätze auf die zur gleichen Zeit zugegriffen wird, sind gesperrt und können nicht mit gesichert werden. Wir werden das in einem späteren Kapitel genauer untersuchen.**

---
---

#  5 Tabellentypen und Transaktionen

Die Entwickler von MySQL haben sich lange geweigert Mechanismen einzubauen, die nur dem Komfort der Entwickler dienen und sich auch auf andere Weise lösen liessen. Dazu gehört der Einbau der referentiellen Integrität genauso wie die Möglichkeit Transaktionen zu verwenden. Das macht den Standardtabellentyp von MySQL zwar schnell und schlank, ist für einige Anwendungen aber nicht optimal. Seit Mai 2000 ist der Tabellentyp **BDB** (Berkley Database – Key/Value-DB) integriert, der auch Transaktionen unterstützt. Mittlerweile ist das Repertoire noch um **InnoDB** und **Gemini** erweitert worden, die ebenfalls transaktionstauglich sind. Damit sind jetzt auch Techniken möglich, die jedes professionelle relationale Datenbanksystem einfach integriert haben muss.

## 5.1 Tabellentypen und Transaktionsmodelle

Ob **Transaktionen** (siehe weiter unten) und **referentielle Integrität** (*Checkt die Richtigkeit einer Relation*) verwendet werden können, hängt also vom verwendeten Tabellentyp ab. Der Tabellentyp von MySQL ist **MyISAM** und kennt *keine* Transaktionsunterstützung. Die drei oben genannten schon!


Sie können innerhalb einer Datenbank Tabellen *unterschiedlicher Typen* verwenden und so das Tabellenformat Ihren Anforderungen anpassen. Dieses Konzept ist schlau, da Transaktionen nicht für jede Anwendung benötigt werden. Sicherheit und Komfort gehen natürlich zu Lasten von Geschwindigkeit. 

Es gibt verschiedene Möglichkeiten, Transaktionen zu verwalten. Die Tabellentreiber (Engines) der BDB-, InnoDB- und Gemini-Tabellen verwenden jeweils unterschiedliche Transaktionsmodelle. Die Unterschiede betreffen in erster Linie die Art und Weise, **wie Datensätze während einer Transaktion vor Änderungen durch andere Anwender geschützt werden**.


|               | **MyISAM** <br> oder **Aria** bei MariaDB                                                                                                       | InnoDB                                                                                                                                                                        |
|---------------|-------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Eigenschaften | - stabil und schnell <br> - braucht wenig Speicherplatz  <br> - unterstützt Table locking                                                            | - unterstützt Transaktionen und referentielle Integrität  <br> - braucht mehr Speicherplatz  <br> - unterstützt **Row Level locking**                                                                |
| Einsatz       | - wenn Geschwindigkeit wichtig  <br> - bei vielen Daten  <br> - für effiziente Verwaltung                                                             | - wenn Sicherheit wichtig ist  <br> - wenn viele Anwender gleichzeitig Daten ändern                                                                                                     |
| Speicherung   | Jede **MyISAM**-Tabelle wird in 3 Tabellen gespeichert:  <br> - `*.FRM` (Tabellenbeschreibung),  <br> - `*.MYD`  oder `MAD` (Daten)  <br> - `*.MYI`  oder `MAI` (Indexe) | -  `*.FRM`-Datei für jede Tabelle  <br> - Daten in einer gemeinsamen Datei - der **Tablespace**-Datei  <br> - Bei `innodb_file_per_table=1` wird zusätzlich ein Tablespace pro Tabelle angelegt (`*.ibd`) |

Tab. 5.1: Die beiden wichtigsten Tabellentypen von MySQL: MyISAM und InnoDB im Vergleich.

Die Beschreibung jeder **MyISAM**-Tabelle wird in einer `*.FRM`-Datei gespeichert, die in einem Verzeichnis liegt, die dem Namen der DB entspricht. (Und wo sich dieses Verzeichnis befindet ist in der Optionsdatei `my.ini` definiert.)

**InnoDB**-Tabellen werden nicht wie alle anderen Tabellentypen in einfachen Dateien gespeichert. Stattdessen dient eine zentrale Datei, die als **Tablespace** bezeichnet wird, als **virtueller Speicher** für alle `InnoDB`-Tabellen und -Indizes. 

###  5.1.1 InnoDB-Tabellen müssen in der Optionsdatei freigeschaltet sein

Die Standareinstellungen in der `my.ini`-Optionsdatei sieht so aus:

```
### skip-innodb.                                 <<<< muss mit # auskommentiert sein!

innodb_data_home_dir="C:/xampp/mysql/data"    

innodb_data_file_path = ibdata1:10M:autoextend   <<<< Einstellung TABLESPACE

innodb_log_group_home_dir="C:/xampp/mysql/data"
#innodb_log_arch_dir = "C:/xampp/mysql/data"
## You can set .._buffer_pool_size up to 50 - 80 %
## of RAM but beware of setting memory usage too high
innodb_buffer_pool_size=16M
## Set .._log_file_size to 25 % of buffer pool size
innodb_log_file_size=5M
innodb_log_buffer_size=8M
innodb_flush_log_at_trx_commit=1
innodb_lock_wait_timeout=50

innodb_table_locks = 1                           <<<< Standard
```



### 5.1.2 Tabellenformat InnoDB und die Tablespace


![](../x_res/ToDo.png) [Datenpool](../Daten)

- **InnoDB-Tabelle erzeugen**: Um eine InnoDB-Tabelle zu erstellen, wird dies im `CREATE TABLE`-Befehl angegeben. Im phpMyAdmin können Sie diese Angabe in einem Auswahlfenster angeben. Das folgende Beispiel erstellt eine neue DB und die InnoDB-Tabelle *Konten*:

```sql
mysql> CREATE DATABASE innotest;  
mysql> USE innotest;
mysql> CREATE TABLE tbl_konto (id_k INT AUTO_INCREMENT, Name VARCHAR(30), Saldo DECIMAL(10,2), PRIMARY KEY (id_k)) ENGINE = InnoDB;
```
<br><br>


-   **Tabellentyp ändern**: Sie können auch bestehende Tabellen nachträglich in InnoDB-Tabellen umwandeln. Das geht entweder mit dem SQL-Befehl `ALTER TABLE ...` oder unter `Operationen` im phpMyAdmin.

```sql
mysql> ALTER TABLE tblname ENGINE = InnoDB;
```

> ![Achtung](../x_res/caution.png) HINWEIS: Ändern Sie auf keinen Fall den Tabellentyp der Tabellen der Datenbank *mysql*! Diese Tabellen mit MySQL-internen Verwaltungsinformationen (Benutzer- und Zugriffsrechte) müssen immer im MyISAM-Format bleiben!

DB [hotel](../Daten/hotel.sql):

-   Ändern Sie in der Datenbank `hotel` den Typ der Tabelle `benutzer` zu `InnoDB`.

-   Lassen Sie sich den Typ jeder Tabelle anzeigen. Kontrollieren Sie, dass die Tabelle `benutzer ` das Format `InnoDB` hat. 
-   Geben Sie Werte ein und kontrollieren Sie die Veränderungen in Ihrem `data`-Verzeichnis und in dem Verzeichnis der Datenbank `hotel`. 
-   Kontrollieren Sie die Dateien in der Dateiordnerstruktur des Datenbank-Verzeichnisses. 
-   Studieren Sie als Ergänzung die folgenden Themen im [MySQL-Manual](https://mariadb.com/kb/en/innodb/).

### 5.1.3 Tablespace

Standardmässig wird beim ersten Start von MySQL die Datei `ibdata1` mit einer Grösse von 10 MByte (oder 12MB je nach Version) erzeugt. Sobald die darin gespeicherten InnoDB-Tabellen mehr Platz beanspruchen, wird die Datei in 8-MByte-Schritten vergrössert (*autoextend*, Siehe `Einstellung` oben). Zusätzlich werden auch die Logging-Dateien `ib_logfile0`, `-1` und `ib_arch_log_000000000` erzeugt. Alle vier Dateien liegen im binären Format vor und dürfen nicht verändert werden.

![Tablespace](../x_res/Tablespace.png)

Abb. 15: Tablespace als Schicht zwischen DB-Tabellen und DB-Dateien

Wenn Sie feststellen möchten, wie viel Speicherplatz innerhalb des *Tablespace* noch frei ist, führen Sie folgenden SQL-Befehl aus:

`SELECT SPACE,NAME,ROUND((ALLOCATED_SIZE/1024/1024), 2) as "Tablespace Size (MB)"  FROM information_schema.INNODB_SYS_TABLESPACES ORDER BY 3 DESC;`

Der freie Platz (insgesamt und für alle InnoDB-Tabellen einzeln), wird dann in der dritten Spalte (ALLOCATED_SIZE) angezeigt.

Mit der Workbench lässt sich die Tablespace via *Dashboard* visuell analysieren:

![Workbench Dashboard TableSpace](../x_res/Dashboard.png)

> ![Hinweis](../x_res/Hinweis.png) Eine Verkleinerung des Tablespace ist nicht vorgesehen. Der einzige Weg besteht darin, die Tabellen mit `mysqldump` und dem Parameter `--all-databases` zu exportieren, einen verkleinerten Tablespace neu zu erzeugen und die Tabellen dorthin wieder zu importieren.

---


## 5.2 Transaktionen


Mit Transaktionen können **mehrere SQL-Anweisungen in einem Block gekapselt** werden, um diesen Block dann "sicher" auszuführen. Sicher bedeutet hier „**ganz oder gar nicht**“. Transaktionen erhöhen in vielen Fällen die *Sicherheit und Konsistenz* der Daten, erleichtern die Programmierung und erhöhen eventuell auch die Effizienz von Anwendungen. Wir verwenden im Folgenden den Tabellentyp `InnoDB`.

### 5.2.1 Wozu Transaktionen?

Dieser Abschnitt erklärt, warum Transaktionen wichtig sind und wie sie unter MySQL eingesetzt werden. Transaktionen stellen sicher, dass eine Gruppe von SQL-Kommandos, die mit `BEGIN` beginnt und mit `COMMIT` endet, entweder vollständig oder gar nicht ausgeführt wird!

**(1) Transaktionen stellen sicher, dass die Daten nicht quasi gleichzeitig von anderen Anwendern verändert werden.**  

**Beispiel:** Eine derartige Gruppierung mehrerer Kommandos ist beispielsweise dann notwendig, wenn Sie einen Betrag von Konto 1 auf Konto 2 umbuchen möchten. Es darf nicht passieren, dass die Abbuchung von Konto 1 durchgeführt wird, nicht aber die Buchung auf Konto 2. Nehmen Sie also an, Sie müssen CHF 1000.- von Herrn *Vontobel* an Herrn *Nachhut* überweisen.

Die Ausgangslage DB innotest: (Siehe oben oder [Datenpool](../Daten/innotest.sql))

```sql
USE innotest;
INSERT INTO tbl_konto (id_K, Name, Saldo) VALUES (1,'Von', 10000), (2,'Nach',0);
```

Die Saldi zweier Konten sind betroffen und müssen nacheinander bearbeitet werden. 

```sql
UPDATE tbl_konto SET Saldo = Saldo - 1000 WHERE name = 'Von';
```

Nach der Abbuchung die Buchung auf das zweite Konto. Dazwischen könnte viel geschehen ...

```sql
UPDATE tbl_konto SET Saldo = Saldo + 1000 WHERE name = 'Nach';
```

... Ein anderer Client könnte genau zwischen diesen beiden Befehlen ebenfalls abbuchen:

```sql
UPDATE tbl_konto SET Saldo = Saldo - 500 WHERE name = 'Von';
```

Die Folge hier wäre nicht so schlimm, "nur" ne Saldo-Überziehung. Schlimmer wäre es mit Produkten, die eben nicht 2x verkauft werden können!

> ![Achtung](../x_res/caution.png) Wenn eine Änderungen Fremdschlüssel betrifft, dann könnte es sein, dass ein andersweitiger geänderter Fremdschlüssel auf einen unterdessen gelöschten Datensatz verweist! (=> Inkonsistenz!)
 
**(2) Wenn während der Operation der Strom ausfällt oder der Rechner abstürzt, kann es nicht passieren, dass nur ein Teil der Kommandos ausgeführt wird.**

Transaktionen erhöhen schliesslich auch den Programmierkomfort. Eine Transaktion kann jederzeit abgebrochen werden, was die Fehlerabsicherung sehr vereinfacht. Wenn also nach mehreren SQL-Kommandos ein Fehler auftritt, können Sie alle bisher im Rahmen einer Transaktion durchgeführten Kommandos durch ein einfaches `ROLLBACK` widerrufen.

Ob Sie nun tatsächlich Transaktionen benötigen oder nicht, hängt sehr stark davon ab, welche Anwendungen Sie mit MySQL entwickeln. Ein einfaches Diskussionsforum im Internet funktioniert auch ohne Transaktionen zuverlässig. Wenn Sie dagegen ein Buchhaltungsprogramm, eine komplexe Lagerverwaltung oder ähnliche Anwendungen entwickeln oder wenn die von Ihnen verwalteten Daten relativ sensitiv sind - also etwa medizinische oder finanzielle Daten -, dann sind Transaktionen und eine entsprechende Programmierung unabdingbar. Das lange Fehlen von Transaktionen in MySQL ist ein Grund dafür, warum MySQL für derartige Anwendungen bis jetzt kaum zum Einsatz kam.

### 5.2.2 BEGIN, COMMIT und ROLLBACK

Indem Sie eine Tabelle vom Typ BDB, InnoDB oder Gemini erzeugen, haben Sie noch nichts gewonnen. Damit Sie den Vorteil von Transaktionen nutzen können, müssen Sie diese mit `BEGIN` (oder `START TRANSACTION`) einleiten und mit `COMMIT;` beenden. `COMMIT;` führt alle seit `BEGIN` angegebenen SQL-Kommandos tatsächlich aus.

Obig beschriebenes Problem könnte also so realisiert werden:


```sql
SET @uebertrag_var = 1000;   -- Übertrag in User-Var speichern

BEGIN;           -- oder START TRANSACTION

  -- Überprüfung:
  SELECT IF(Saldo >= @uebertrag_var, @uebertrag_var, 0)  INTO  @uebertrag_var
    FROM   tbl_konto WHERE  name = 'Von';  -- Schauen ob Saldo genug hoch ist, sonst Übertrag nullen.

  -- Übertrag:
  UPDATE tbl_konto SET Saldo = Saldo - @uebertrag_var WHERE name = 'Von';  
  UPDATE tbl_konto SET Saldo = Saldo + @uebertrag_var WHERE name = 'Nach';
  
COMMIT; --- oder ROLLBACK;
```

Statt `COMMIT` auszuführen, können Sie die gesamte Transaktion aber auch mit `ROLLBACK` (manuell) widerrufen. `ROLLBACK` wird *automatisch ausgeführt*, falls es zu einem *Verbindungsabbruch* kommt.


> ![Hinweis](../x_res/Hinweis.png) Wie weit offene Transaktionen den *Lesezugriff* auf eine Tabelle und deren Veränderung durch andere Clients blockieren, hängt von der Implementierung der Transaktionsunterstützung ab. (Siehe weiter unten!)


### 5.2.3 Der Autocommit-Modus

MySQL befindet sich normalerweise im Autocommit-Modus (`AUTOCOMMIT=1`). Alle SQL-Kommandos, die nicht durch ein vorheriges `BEGIN` als Transaktion gekennzeichnet sind, werden daher sofort ausgeführt.

Wenn Sie das SQL-Kommando `SET AUTOCOMMIT=0` ausführen, wird der Autocommit-Modus ausgeschaltet. Das bedeutet, dass alle SQL-Kommandos als eine grosse Transaktion gelten. Die Transaktion wird durch `COMMIT` oder `ROLLBACK` abgeschlossen. Damit beginnt automatisch eine neue Transaktion. Sie müssen also kein `BEGIN` mehr ausführen. Allein diese Bequemlichkeit ist oft Grund genug, um mit `SET AUTOCOMMIT=0` zu arbeiten.

Eine wichtige Konsequenz von `AUTOCOMMIT=0` besteht darin, dass bei einem Verbindungsabbruch - egal ob er beabsichtigt oder unbeabsichtigt eingetreten ist - alle SQL-Kommandos, die nicht durch `COMMIT` bestätigt sind, widerrufen werden.

Beachten Sie auch, dass durch `AUTOCOMMIT=0` sehr lange Transaktionen entstehen, wenn Sie nicht regelmässig `COMMIT` oder `ROLLBACK` ausführen. Manche Tabellentreiber kommen mit derart langen Transaktionen schlecht zurecht, das heisst es können Locking-Probleme auftreten, die die Effizienz beeinträchtigen. Aufgrund seiner Architektur kommt der InnoDB-Tabellentreiber mit derartigen Transaktionen überdurchschnittlich gut zurecht.

### 5.2.4 Locking

Ohne Transaktionen können Sie einen Schutz gegen oben beschriebene Probleme nur durch `LOCK`-Kommandos erreichen. Damit wird aber kurzzeitig eine *ganze Tabelle* für alle Clients blockiert, was sehr ineffizient ist. Bei den transaktionstauglichen Tabellentypen werden *nur die Datensätze blockiert, die tatsächlich verändert werden* sollen. 

Bei **MyISAM**-Tabellen kann nur die gesamte Tabelle durch `LOCK` geschützt werden. Bei **BDB**-Tabellen werden während einer Transaktion automatisch (also ohne ein explizites `LOCK`-Kommando) alle erforderlichen Datensätze gesperrt. Intern werden dabei allerdings ganze Speicherseiten gesperrt, weshalb es passieren kann, dass einige in der Nähe gespeicherte Datensätze während der Transaktion ebenfalls nicht mehr zugänglich sind.

Auch bei **InnoDB**- und **Gemini**-Tabellen werden die Datensätze **automatisch** gesperrt. Allerdings ist das Locking hier durch zusätzliche SQL-Kommandos genauer steuerbar als bei **BDB**-Tabellen. Ein weiterer Vorteil besteht darin, dass hier wirklich nur die betroffenen Datensätze gesperrt werden. Diese intelligenteste Form des Locking hat insbesondere den Vorteil, dass andere MySQL-Anwender so wenig wie möglich behindert werden.

Alle drei Tabellentreiber erkennen im Regelfall automatisch **Deadlock**-Situationen, bei denen einzelne Prozesse endlos aufeinander warten. In solchen Fällen wird bei dem Prozess, der den Deadlock ausgelöst hat, automatisch ein `ROLLBACK` (siehe weiter unten) ausgeführt.

Als zusätzliches Feature versprechen sowohl InnoDB als auch Gemini ein automatisches **Crash-Recovery**. Das bedeutet, dass alle Tabellen beim nächsten Start nach einem Stromausfall automatisch wiederhergestellt werden, wobei die Software alle bis zum Stromausfall vollständig ausgeführten Transaktionen berücksichtigt.

| Locking-Mechanismen |                                    |
|---------------------|------------------------------------|
| MyISAM              | **Table**-Locking                      |
| BDB                 | **Page-Level**-Locking - Speicherseite |
| Gemini              | **Page-Level**-Locking – Speicherseite      |
| InnoDB              | **Row-Level**-Locking - Datensatz      |

### 5.2.5 Locking-Verhalten von InnoDB

**(1) Auto locking:** Der InnoDB-Tabellentreiber kümmert sich selbständig um alle notwendigen Locking-Operationen. ![Hinweis:](../x_res/Hinweis.png) Beachten Sie also, dass Sie das Kommando `LOCK TABLE` nicht ausführen sollten! Alle durch `INSERT`, `UPDATE`, `DELETE` veränderten Datensätze werden *automatisch* bis zum Ende der *Transaktion* durch einen so genannten **Exclusive Lock** gesperrt. Das heisst: Andere Transaktionen können diese Datensätze nicht verändern.

**Eine Besonderheit von InnoDB besteht darin, dass gewöhnliche `SELECT`-Kommandos trotz gesperrter Datensätze immer sofort ausgeführt werden.** Allerdings ignorieren die zurückgegebenen Resultate noch laufende Transaktionen anderer Klienten, liefern also eventuell veraltete Daten (Siehe weiter unten im Kasten "Transaktionsbeispiel": Verbindung B zum Zeitpunkt 1). 

**(2) LOCK TABLE[S]:** MySQL/MariaDB ermöglicht es den Klienten (bei MyISAM-Tabellen), explizit Tabellen zu sperren, um mit anderen Sitzungen für den Zugriff auf Tabellen zusammenzuarbeiten oder um zu verhindern, dass andere Sitzungen Tabellen in Zeiten ändern, in denen eine Sitzung exklusiven Zugriff auf sie benötigt. Sperren können verwendet werden, um Transaktionen zu emulieren oder um eine *höhere Geschwindigkeit* bei der Aktualisierung von Tabellen zu erreichen. **UNLOCK-TABLE** hebt die Sperrung wieder auf. [Siehe Manual](https://mariadb.com/kb/en/lock-tables/)

![Hinweis:](../x_res/Hinweis.png) **LOCK TABLES** funktioniert bei InnoDB-Tabellen nur, wenn die Systemvariable `innodb_table_locks` auf `1` (Standard) und `autocommit` auf `0` (`1` ist Standard) gesetzt ist. Bitte beachten Sie, dass bei LOCK TABLES mit `innodb_table_locks = 0` keine Fehlermeldung ausgegeben wird.

**(3) SELECT ...FOR UPDATE:** Wenn dieses Default-Verhalten für Ihre Anwendung nicht optimal ist, bieten zwei Erweiterungen des `SELECT`-Kommandos die Möglichkeit, das Locking-Verhalten gezielt zu steuern: Zum einen können Sie einzelne Datensätze auch ohne eine tatsächliche Veränderung exklusiv sperren. Das empfiehlt sich, wenn Sie die Datensätze zuerst lesen und anschliessend ändern möchten. Dazu verwenden Sie das `SELECT`-Kommando mit der Erweiterung `FOR UPDATE`:

```sql
BEGIN
SELECT * FROM table WHERE x>10 FOR UPDATE
-- die ausgewählten Datensätze
-- sind jetzt exklusiv gesperrt
COMMIT; -- oder ROLLBACK;
```

**(4) SELECT ... LOCK IN SHARE MODE:** Sie können auch mit `SELECT ... LOCK IN SHARE MODE` explizit darauf warten, bis alle noch offenen Transaktionen abgeschlossen sind - genau genommen wartet `SELECT`, bis alle anderen **Exclusive Locks** auf den Datensatz aufgelöst sind.

Gleichzeitig werden die von `SELECT` gefundenen Datensätze nun mit einem so genannten **Shared Lock** gesperrt. Ein Shared Lock ist nicht ganz so stark wie ein Exclusive Lock. Die Daten können von anderen Anwendern ebenfalls nicht verändert werden, lassen sich aber mit `SELECT ... LOCK IN SHARE MODE` lesen.

```sql
BEGIN

SELECT * FROM table WHERE x>10 LOCK IN SHARE MODE
-- die ausgewählten Datensätze sind
-- jetzt durch shared locks gesperrt

COMMIT; -- oder ROLLBACK;
```

`SELECT ...LOCK IN SHARE MODE` sollte dann eingesetzt werden, wenn Sie die Datensätze zwar selbst nicht verändern, aber sicher sein möchten, dass die Daten auch von anderen Anwendern nicht geändert werden.

Die maximale Wartezeit auf die Freigabe gesperrter Datensätze wird in der MySQL-Konfigurationsdatei durch `set-variable=innodb_lock_wait_timeout=n` eingestellt (in Sekunden). Verstreicht diese Zeit, wird die gesamte Transaktion durch `ROLLBACK` abgebrochen.


### 5.2.6 Nachteile von Transaktionen

Normale Tabellenoperationen sind in Tabellen mit Transaktionsunterstützung im Regelfall langsamer, weil der Verwaltungsaufwand grösser ist. Genau das ist auch der Grund, warum sich die MySQL-Entwickler lange geweigert haben, Transaktionen zu unterstützen.

Transaktionen stellen schliesslich grössere Anforderungen an den Programmierer: Zum einen muss er sich mit dem Transaktionsmodell des jeweiligen Tabellentreibers auseinandersetzen und die SQL-Kommandos zur Steuerung kennen und verstehen lernen. Zum anderen ist es nötig, den Ablauf besser als bisher abzusichern: Aufgrund von Transaktionen kann es dazu kommen, dass SQL-Kommandos momentan nicht ausgeführt werden können, weil andere Anwender die Daten gerade ändern. Wenn es dabei zu Verzögerungen kommt, wird Ihr SQL-Kommando nach einer bestimmten Zeit abgebrochen.

## 5.3 Transaktionen ausprobieren


![](../x_res/ToDo.png) Das Beispiel geht davon aus, dass es in der Datenbank *test* die **InnoDB**-Tabelle *table1* gibt. In dieser Tabelle muss sich mindestens ein Datensatz befinden. Natürlich werden Sie in der Praxis mehr Datensätze haben, aber zur Demonstration des Mechanismus reicht ein Datensatz aus.


```sql
USE test;
CREATE TABLE table1 (colA INT AUTO_INCREMENT, colB INT, PRIMARY KEY (colA)) ENGINE=InnoDB;
INSERT INTO table1 (colB) VALUES (10);
```

Um Transaktionen ausprobieren zu können, müssen Sie **zwei** Verbindungen zur Datenbank `test` herstellen. Am einfachsten führen Sie dazu den Monitor `mysql.exe` in zwei Fenstern aus. Anschliessend führen Sie die im Kasten "Transaktionsbeispiel" dargestellten Kommandos zeitlich abgestimmt mal in dem einen, mal im anderen Fenster aus.

Zum **Zeitpunkt 1** sieht der Inhalt von *table1* aus Sicht der beiden Verbindungen unterschiedlich aus. Für Verbindung A gilt für den Datensatz mit *colA=1* bereits *colB=11*. Da diese Transaktion T1 aber noch nicht tatsächlich ausgeführt ist, sieht Verbindung B für denselben Datensatz *colB=10*.

Zum **Zeitpunkt 2** beginnt B eine Transaktion T2; *colB* des Datensatzes mit *colA=1* soll um drei vergrössert werden. Der InnoDB-Tabellentreiber erkennt, dass er dieses Kommando zur Zeit nicht ausführen kann, und blockiert B, die jetzt darauf wartet, dass A die Transaktion beendet.

Zum **Zeitpunkt 3** schliesst A die Transaktion T1 mit `COMMIT`. Damit enthält *colB* definitiv den Wert 11. Jetzt kann auch das `UPDATE`-Kommando von B abgeschlossen werden.

Zum **Zeitpunkt 4** sieht A *colB=11*. Für B sieht es so aus, als hätte *colB* bereits den Wert *14*. Nun widerruft B die Transaktion T2.

Damit sehen A und B zum **Zeitpunkt 5** beide den tatsächlich gespeicherten Wert *colB=11*.

![](../x_res/Transaktion.png)

Abb. 16: Ablauf Transaktion


---

## 5.4 ACID


Die Abkürzung **ACID** steht für die notwendigen Eigenschaften, um Transaktionen auf Datenbankmanagementsystemen (DBMS) einsetzen zu können. Es steht für **A**tomarität (atomicity), **K**onsistenz (consistency), **I**soliertheit (isolation) und **D**auerhaftigkeit (durability). Im Deutschen spricht man gelegentlich auch von AKID.

### 5.4.1 Atomarität

Ein Block von SQL-Anweisungen (eine oder mehrere) wird entweder ganz oder gar nicht ausgeführt: Das Datenbanksystem behandelt diese Anweisungen wie eine einzelne, unteilbare (daher atomare) Anweisungen. Gibt es bei einem Statement technische Probleme oder tritt ein definierter Zustand ein, der einen Abbruch erfordert (z. B. Kontoüberziehung), werden auch die bereits durchgeführten Operationen nicht auf der Datenbank wirksam.

### 5.4.2 Konsistenz

Nach Abschluss der Transaktion befindet sich die Datenbank in einem konsistenten Zustand. Das bedeutet, dass Widerspruchsfreiheit der Daten gewährleistet sein muss. Das gilt für alle definierten Integritätsbedingungen genauso, wie für die Schlüssel- und Fremdschlüsselverknüpfungen.

### 5.4.3 Isoliertheit

Die Ausführungen verschiedener Datenbankmanipulationen dürfen sich nicht gegenseitig beeinflussen. Mittels Sperrkonzepten oder Timestamps wird sichergestellt, dass durch eine Transaktion verwendete Daten nicht vor Beendigung dieser Transaktion verändert werden. Die Sperrungen müssen so kurz und begrenzt wie möglich gehalten werden, da sie die Performance der anderen Operationen beeinflusst.

### 5.4.4 Dauerhaftigkeit

Nach Abschluss einer Transaktion müssen die Manipulationen dauerhaft in der Datenbank gespeichert sein. Der Pufferpool speichert häufig verwendete Teile einer Datenbank im Arbeitsspeicher. Diese müssen aber nach Abschluss auf der Festplatte gespeichert werden.

## 5.5 Locking ausprobieren


![](../x_res/ToDo.png) Probieren Sie die oben beschrieben **Lock**-Mechanismen mit ebenfalls zwei Konsolen `user1` und `user2` aus. Erstellen Sie dazu kleine SQL-Scripte, z.B. auf die DB `hotel`:

-  Stellen Sie zuerst die zu benutzenden Tabellen in `hotel` auf **InnoDB** um!

-  Einfaches **LOCK**ing: Verhalten SELECT und UPDATE durch User2


-  Locking **SELECT ... FOR UPDATE**: Verhalten SELECT und UPDATE durch User2


-  **SELECT ... LOCK IN SHARE MODE**: Verhalten SELECT und UPDATE durch User2



---

---


# 6 Datenbank-Sicherheit

Bei den bisherigen Aufgaben sind Sie kaum mit der Benutzerverwaltung von MySQL in Berührung gekommen. Wenn Sie den MySql-Klient gestartet haben, hatten Sie für bestimmte Datenbanken nicht genügend Rechte, daher mussten wir `mysql –u root` mit der Benutzerkennung für den root-User eingeben. 

![](../x_res/Hinweis.png )Auch bei der Einrichtung des Datenbankverzeichnisses auf dem eigenen Laufwerk waren Aktionen notwendig, die die Benutzerverwaltung betreffen: die **`mysql`-Datenbasis** muss im `datadir`-Verzeichnis vorhanden sein, sonst lässt sich der Server gar nicht aufstarten.

![](../x_res/System-Tabellen.png)


Abb. 17: MySQL System-Tabellen in phpMyAdmin

Der `GRANT`-Befehl vom SQL, den Sie bereits bei einer Übung eingesetzt haben, nimmt die entsprechenden Einträge in den Berechtigungs-Tabellen (**\*_priv**) vor. Die Einschränkungen können bis auf Attributsebene vorgenommen werden. Je nach Berechtigung kann ein Benutzer also nicht alle Spalten einer Tabelle sehen oder verändern. Die für uns wichtigen Tabellen sind: **global\_priv, db, tables\_priv und colums\_priv**. (Vergl. Abb.17)

## 6.1 Zugriffssystem

Die Zugriffskontrolle führt MySQL in zwei Phasen durch. Die erste Phase - der Klient baut die Verbindung zum Server auf – nennt sich **Authentifizierung**. Sie überprüft drei Angaben, nämlich Benutzername, Passwort und die Rechner, von dem aus zugegriffen wird. Diese Informationen findet MySQL in der  Ansicht (View)  `user`. Im positiven Fall werden anschliessend bei jedem Request an die Datenbank die Berechtigungen überprüft: die **Autorisierung**.

![](../x_res/View_User.png)


###   6.1.1 Authentifizierung (Identitätsprüfung: Wer?): 

Der Server prüft, ob sich der Benutzer mit dem Server verbinden darf. Identifiziert wird jeder Benutzer durch seinen Benutzer- und Hostnamen. (Dieser Benutzername gilt für den DB-Server und hat nichts mit dem Benutzernamen des Betriebssystems zu tun.) Das Sicherheitssystem von MySQL beruht auf folgende Informationen:

| Sicherheitsinformationen |                                                   |
|--------------------------|---------------------------------------------------|
| **`Benutzername`**       | Name für die DB-Anmeldung (Default: leer)         |
| **`Passwort`**           | Wird verschlüsselt abgespeichert (Default: leer)  |
| **`Hostname`**           | Rechnername des Benutzers  <br> <br>  - `localhost`: User darf auf dem *Server* einloggen <br> - `'%'` (Default): User darf von *überall*, ausser vom Server einloggen  <br> -` 172.16.17.111`: User darf von Klient mit *IP* einloggen <br> - `'name.local'`:  User darf von Klient mit *Hostnamen* einloggen |

Tab. 6.3: Die 3 Informationen des Sicherheitssystems

Ein Benutzer wird also folgendermassen angelegt (mit und ohne Poasswort):

```
DROP USER IF EXISTS 'user'@'hostname';
CREATE USER 'user'@'hostname' IDENTIFIED BY 'Passw0rt';  -- User with password 'Passw0rt'

CREATE USER 'user2'@localhost;                           -- User with no password
``` 

[Manual CREATE USER](https://mariadb.com/kb/en/create-user/)

### 6.1.2 Passwort (-verschlüsselung)

Das Passwort darf aus Sicherheitsgründen nicht unverschlüsselt abgespeichert werden. Auch sollte obiger Befehl mit Passwort im Klartext nicht in einem Script erscheinen.

Zum Verschlüsseln stellt MySQL die Funktion *password()* zur Verfügung, die einen 41-Bytes Hash erzeugt. Sie können den Hashwert erzeugen und anzeigen mit: 

`SELECT PASSWORD('TBZforever');`

> Beachten Sie den Stern **\*** am Anfang des Hash-Wertes!

Das Passwort eines Users wird folgendermassen gesetzt (oder geändert) und gleich aktiviert:

```
SET PASSWORD FOR 'user'@'%'  = password('TBZforever'); 
SET PASSWORD FOR 'user2'@localhost = '*74B1C21ACE0C2D6B0678A5E503D2A60E8F9651A3');
FLUSH PRIVILEGES;                     -- Aktivierung -- nie vergessen!
```

`FLUSH PRIVILEGES` macht die Änderungen sofort wirksam (statt erst nach dem Server-Neustart).

![note](../x_res/note.png) Gelöscht wird das Passwort durch Eingabe eines leeren Passwortes.

[Manual SET PASSWORD](https://mariadb.com/kb/en/set-password/)


![](../x_res/ToDo.png) **User erstellen:**

- Erstellen Sie ein paar Benutzer mit verschiedenen Passwörter und Hostnamen.
- Untersuchen Sie die Einträge in der Ansicht `mysql.user` und der Datenabsis `mysql.global_priv`.
- Testen Sie die Login. (Welche gehen nicht?)
- Ändern Sie einzelne Passwörter.
- Löschen Sie die Benutzer wieder.

---


### 6.1.3  Autorisierung (Prüfung der Berechtigung, Was?):
   
Die Autorisierung kann entweder *global* oder *lokal* auf Tabellen, usw. vergeben werden. 


![](../x_res/Zugriffsberechtigung.png)

Abb. 18: Zugriffsberechtigung für verschiedene Benutzer einer Datenbank


#### Das MySQL-Zugriffssystem

| Begriffe           |                                                                                                                                                                                                                                                                                                        |
|--------------------|-----|
| Privileg               | Berechtigung auf eine DB, Tabelle oder Spalte                                                                                                                                                                                                                                                          |
| **Globales** Privileg  | Berechtigung für alle DB, Tabellen und Spalten                                                                                                                                                                                                                                                         |
| **SELECT**-Privileg    | Benutzer darf Daten lesen                                                                                                                                                                                                                                                                              |
| **UPDATE**-Privileg    | Benutzer darf Daten ändern                                                                                                                                                                                                                                                                             |
| **GRANT**-Privileg     | Benutzer darf Zugriffsrechte festlegen u. Benutzer erstellen                                                                                                                                                                                                                                           |
| **FILE**-Privileg      | **\*** Werden benötigt, um eine Reihe von Datei-Befehlen auszuführen: `LOAD FILE ()`, `LOAD DATA INFILE …`   |
| **All**                | alle Privilegien (ausser GRANT)                                                                                                                                                                                                                                                                        |
| **Usage**              | keine Privilegien (nur Connect auf den Server)                                                                                                                                                                                                                                                         |

Tab. 6.3: Begriffe betreffend Privilegien


![](../x_res/caution.png)*) Das FILE-Privileg sollte mit Vorsicht eingesetzt werden, da dieser User alle Dateien lesen kann, die öffentlich sind oder vom MySQL-Server gelesen werden können – unabhängig vom Datenbankverzeichnis!

### 6.1.4 Benutzer einrichten und Zugriffsrechte festlegen

Diee Befehle `GRANT` und `REVOKE` (Data Control Language = DCL) werden eingesetzt. Manual [GRANT](https://mariadb.com/kb/en/grant/) / [Revoke](https://mariadb.com/kb/en/revoke/)

![caution](../x_res/caution.png) Voraussetzung: der Benutzer benötigt das `GRANT`-Privileg. Entweder erstellen Sie einen User mit dem Zusatz „`WITH GRANT OPTION`“ oder Sie führen die Beispiele als `root`-User aus.

#### Zugriffsrechte ändern mit GRANT und REVOKE

Mit den Befehlen `GRANT` und `REVOKE` werden Privilegien erteilt bzw. gelöscht. Beachten Sie in der Syntax: `GRANT TO` und `REVOKE FROM` – ist aber ja logisch!

```SQL
GRANT privileg1 [, privileg2, ...]
ON [datenbank.]tabelle
TO user@host [IDENTIFIED BY 'passwort'] [WITH GRANT OPTION] ;

REVOKE privileg1 [, privileg2, ...]
ON [datenbank.]tabelle
FROM user@host ;
```

| Privilegien           |                                                         |
|-----------------------|---------------------------------------------------------|
| **ON** \*.\*              | globale Privilegien (alle DB mit allen Tabellen)        |
| **ON** db.\*              | DB-Privilegien (alle Tabellen der DB)                   |
| **ON** db.tb              | Tabellen-Privilegien (alle Spalten der Tabelle)         |
| (att1, att2) **ON** db.tb | Spalten-Privilegien (Spalten att1 und att2 der Tabelle) |

Tab. 6.4: Die verschiedenen Ebenen der Privilegien

**Zugriffsrechte auf alle Tabellen einer DB festlegen**:

Der folgende Befehl erteilt dem Benutzer `hotel_admin` alle Privilegien (inklusive Grant). Ein bestehendes Passwort wird dabei nicht verändert.

`GRANT ALL ON hotel.* TO hotel_admin@localhost WITH GRANT OPTION;`

Der Befehl um einem User Dateizugriffsberechtigungen zu geben, lautet:

`GRANT FILE ON *.* To Username@‘%‘`

**Lesen und Ändern:**

Der folgende Befehl gibt `hotel_user` das Recht, die Daten in der DB hot``el zu lesen und zu ändern:

`GRANT SELECT, INSERT, UPDATE, DELETE ON hotel.* TO hotel_user@localhost;
`

![note](../x_res/note.png) Mit `REVOKE` kann der Zugriff auf einzelne Tabellen oder Spalten nicht verboten werden, sondern nur auf die ganze DB.

**Zugriffsrechte betrachten:**

Prüfung der Zugriffsrechte einzelner Benutzer.

` SHOW GRANTS FOR hotel_admin@localhost;
`

<br> 

### 6.1.5 Strategie zur Rechtevergabe:

- **Globale Rechte** 

  Werden für Admin-Accounts '`root`' bzw. '`admin`' auf alle DBs (\*.\*) vergeben:
  
  ```
  GRANT ALL PRIVILEGES ON *.* TO 'admin'@localhost;
  FLUSH PRIVILEGES;                                  -- Aktivierung -- nie vergessen!
  SHOW GRANTS FOR 'admin'@localhost;
  ```

-  **Lokale Rechte**

   Werden für User-Accounts vergeben. Die Rechte werden in Gruppenrollen definiert und die Benutzer den Gruppen zugeordnet:
   
   ```
   CREATE ROLE rolle;
   GRANT SELECT, INSERT, UPDATE, DELETE ON db.tbl TO rolle;  -- Rolle wird erzeugt
   GRANT SELECT, ... ON db.tbl TO user@hostname IDENTIFIED BY 'Passw0rd'; -- User wird erzeugt!
   GRANT rolle TO user@hostname;  -- Rolle wird User übertragen                     
   FLUSH PRIVILEGES;                                  -- Aktivierung -- nie vergessen!
   
   SELECT CURRENT_ROLE;  -- Aktive Rollen für aktuellen Benutzer werden angezeigt (Standard NULL - also keine)
   SET ROLE rolle;       -- Rollen dem aktuellen User zuordnen

   ```
   
   [Manual Roles](https://mariadb.com/kb/en/roles_overview/)

   Der Server prüft für jeden einzelnen SQL-Befehl (z.B. `SELECT`, `UPDATE`, `DELETE`, `DROP`), ob der Benutzer ihn ausführen darf. Wenn nicht, bricht die Ausführung des Befehls mit einer Fehlermeldung ab. 

#### Zugriffsmatrix

Für jeden Benutzer bzw. Benutzertyp (Gruppe, Rolle) muss genau festgelegt werden, auf welche Daten er in welcher Form zugreifen darf. Dies kann in Form einer **Zugriffsmatrix** erfolgen.

| Zugriffsmatrix            |                                            |            |       |   |       |       |       |       |   |
|---------------------------|--------------------------------------------|------------|-------|---|-------|-------|-------|-------|---|
| Benutzergruppe =\>        | Verkauf                                    | |       |   |      |  Management    |       |       | 
| Tabellen / Attribute      | S                                          | I          | U     | D | | S     | I     | U     | D     |   
| produkte                  | **x**                                      |            |       |   | | **x** | **x** | **x** | **x** |   
| personal                  |     -                                       |            |       |   | |    -   | **x** |     -  | **x** |   
| **-** lohn                |                                            |            |       |   | | **x** |   -    | **x** |       -|   
| **-** restliche Attribute |                                            |            |       |   | | **x** |   -    | **x** |  -     |   
| rechnungen                | **x**                                      |            |       |   | | **x** | **x** | **x** | **x** |   
| kunden                    | **x**                                      | **x**      | **x** |  |  | **x** | **x** | **x** | **x** |   

*S = Select, I = Insert, U = Update, D = Delete*

Tab. 6.2: Zugriffsmatrix für eine Datenbank

Die Berechtigungen können für jede Tabelle und sogar für jedes Attribut einzeln vergeben werden. So ist z.B. in Tab. 6.2 das Attribut lohn getrennt von den restlichen Attributen eingetragen. I(nsert)- und D(elete)-Berechtigungen können nur für ganze Datensätze vergeben werden.

```
CREATE ROLE verkauf, management;
GRANT SELECT ON firma.produkte TO verkauf;  
GRANT SELECT (lohn, ..., ...) ON firma.personal TO verkauf;
...
GRANT SELECT, INSERT, UPDATE, DELETE ON firma.produkte TO management;
...
GRANT verkauf TO user@host;   --- @user: SET ROLLE nicht vergesssen!
...
```

[YT Erklärvideo zu Rollen](https://www.youtube.com/watch?v=LWWuFMhI6FY)


![](../x_res/ToDo.png) **Zugriffsmatrix umsetzen:** DB Firma

- Erstellen Sie die zwei Rollen `varkauf` und `management`.
- Erstellen Sie pro Gruppe einen Benutzer
- Fügen Sie die Rechte gemäss Zugriffsmatrix den Gruppen zu und übertragen sie diese den entsprechenden Benutzern.
- Untersuchen Sie die Einträge in der Ansicht `mysql.user` und der Datenabsis `mysql.global_priv`.
- Testen Sie den Zugriff beider Benutzer (SET Role x nicht vergessen!)

- Löschen Sie die Gruppen und Benutzer wieder.

---

## 6.2 DB-Server absichern - Default-Sicherheitseinstellungen

![caution](../x_res/caution.png) Nach der Default-Installation von MySQL gilt eine sehr unsichere Einstellung ohne Passwort-Absicherung. Jeder kann sich ohne Passwort vom lokalen Rechner oder von jedem externen Rechner aus als `root` oder sogar ohne Benutzernamen anmelden!

> `root`: Wie unter Unix/Linux spielt `root` bei MySQL die Rolle des *Administrators* (*Superuser*) mit unbeschränkten Rechten. Nach der Installation kann man sich als `root` ohne Passwort anmelden. Die Defaulteinstellungen sind in der Systemdatenbank *mysql* abgespeichert und werden durch das MySQL-Setup-Programm eingetragen.

Die folgenden Massnahmen schränken den Zugang ein. Sie sollten direkt nach der Installation eines DB-Servers erfolgen.

**`root`-Passwort für lokalen Zugang festlegen**: Mit folgendem Befehl wird für den Superuser root ein Passwort für den Zugriff vom Server-Rechner aus festgelegt.

```SQL
C:\>mysql -u root

SET PASSWORD FOR root@localhost = PASSWORD('superpasswort');
FLUSH PRIVILEGES;     -- nie vergessen!

```

![caution](../x_res/caution.png) Passen Sie auf, dass Sie dem Benutzer `root` nicht die nötigen Privilegien wegnehmen, sonst haben Sie u.U. keinen Zugriff mehr auf den eigenen Server !

![note](../x_res/note.png) Von jetzt an muss man sich als root mit -u root mit der Option -p anmelden und das Passwort eingeben.

```
C:\>mysql -u root -p
Enter password: *****
```

Falls Sie diese Änderung über phpMyAdmin durchgeführt haben, haben Sie jetzt ein Problem: Die Verbindung zum Server kann nicht hergestellt werden. Logisch! Wir sind ja als root ohne Passwort angemeldet. Die notwendigen Änderungen müssen in der Datei `config.inc.php` durchgeführt werden. Diese finden Sie in `C:/xampp/phpMyAdmin`. Weitere Informationen finden Sie in der Dokumentation: <https://docs.phpmyadmin.net/en/latest/config.html#basic-example>.


![](../x_res/Anpassung.png)

Abb. 19: notwendige Anpassung in der config.inc.php für root-Passwort

Sie können den auth-type auch auf *http* oder *cookie* (siehe Kommentartext im config-File) setzen und werden dann beim Start von phpMyAdmin nach Name und Passwort gefragt. User- und Passworteinträge können in diesem Fall leer bleiben. Das hat den Vorteil, dass die Anmeldung im phpMyAdmin mit verschiedenen Benutzern erfolgen kann.

**`root`-Zugang von fremden Rechnern aus verhindern**: Löschen des Users`root` von anderen Rechnern (`%`) aus:

```SQL
DROP USER 'root'@'%';  -- oder mindestens REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'root'@'%';
FLUSH PRIVILEGES;
```

Nach dem Löschen ist der Zugang als `root` mit Passwort nur von `localhost` aus möglich;  <br> oder wir setzen zwingend ein Passwort:

`SET PASSWORD FOR 'root'@’%’ = PASSWORD('superpasswort')`

**Kein Zugang ohne Passwort von externen Rechnern**: Auch alle anderen Benutzer von extern müssen ein Passwort haben.

**Lokalen Zugang ohne Passwort ermöglichen**: Vielleicht ist die Absicherung des Servers nun zu einschränkend und man möchte für den lokalen Rechner einen generellen MySQL-Zugang ohne Benutzernamen und Passwort doch wieder zulassen:

```SQL
GRANT USAGE ON *.* TO ''@localhost;
FLUSH PRIVILEGES;
```

![](../x_res/Hinweis.png)`USAGE` erlaubt nur das Anmelden ohne Privilegien.

Die Einträge in der Tabelle `user` in der `mysql`-Datenbank sind vom Administrator sehr kritisch zu untersuchen und den Bedürfnissen entsprechend anzupassen. Diese Schritte sind für eine professionelle Umgebung nur der Anfang.

## 6.3 Der pma-User für phpMyAdmin

Nach der Installation von phpMyAdmin wird ein zusätzlicher Benutzer erzeugt „`pma`“. Er hat zwar nur eingeschränkte Rechte, sollte aber ebenfalls mit Passwort versehen werden. 

```
SHOW GRANTS FOR pma@localhost;
+--------------------------------------------------------------------------------------+
| Grants for pma@localhost                                                             |
+--------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO `pma`@`localhost`                                              |
| GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE ON `phpmyadmin`.* TO `pma`@`localhost` |
+--------------------------------------------------------------------------------------+
2 rows in set (0.000 sec)
```

**Auch dieses Passwort muss natürlich in der User-Tabelle eingetragen** und in der `config.inc.php` abgelegt werden. Sie können phpMyAdmin nur dann aufstarten, wenn Sie einen gültigen Benutzernamen mit Passwort haben **und** wenn die Angaben des „`pma`“-Controlusers korrekt sind!

Wer diesen Benutzer löscht, kann phpMyAdmin nicht mehr verwenden. Wenn dies aus Versehen geschehen ist, muss nicht alles neu installiert werden, sondern Sie können den User neu anlegen. Weitere Informationen finden Sie in der Dokumentation: <https://docs.phpmyadmin.net/en/latest/config.html#example-for-signon-authentication>.

`mysql> SET PASSWORD FOR pma@localhost = PASSWORD('irgendwas');`

![](../x_res/pma.png)

Abb. 20: Damit ist auch der Benutzer "pma" passwortgeschützt

Hinweis: Wenn das Passwort für den `pma`-User gesetzt wurde, kann man MySQL evtl. nicht mehr über das XAMPP-Control-Panel herunterfahren. Bei mir hat auch eine Anpassung in der Datei `mysql_stop.bat` im `xampp`-Verzeichnis keinen Erfolg gebracht. Man muss den MySQL-Server also durch Eingabe folgender Zeile manuell herunterfahren:

`C:\...\mysql\bin\mysqladmin --user=pma --password=irgendwas shutdown`

Alternativ kann man natürlich auch die `mysql_stop.bat` modifizieren und danach aufrufen (z. B. über eine Verknüpfung auf dem Desktop).  


<br><br><br>

---

![](../x_res/ToDo.png) **Fallbeispiel**: Rechte auf Tabellen und Spalten (ohne Rollen) (ca. 1L)


   [Datenpool](../Daten/Blogeinträge.sql)
   
   [Tutorial Blogeinträge](https://gridscale.io/community/tutorials/mysql-feinabstimmung-benutzerrechte/)
    
   [(Lösung SQL Befehle)](./Blogeinträge_Lös.sql)
   
<br> 
    
---    

## 6.4 phpMyAdmin Rechteverwaltung

Zugang zu den Benutzerkonten:

![](../x_res/pmaRechte_1.png)

Globale Privilegien setzen / löschen:

![](../x_res/pmaRechte_2.png)

Lokale Privilegien auf mehrere DBs:

![](../x_res/pmaRechte_3.png)

Lokale Privilegien setzen / löschen:

![](../x_res/pmaRechte_4.png)

<br> 


---

---


# 7 DB-Server im LAN

Die Informatik ist geprägt von der *Dezentralisierung*. Im Zusammenhang mit DB-Applikationen bedeutet dies Verteilung der Aufgaben und/oder der Datenbasis auf mehrere Rechner.

| Warum Dezentralisierung ? |                                                            |
|---------------------------|------------------------------------------------------------|
| Datenverfügbarkeit        | sicherer bei mehreren Servern, z.B. falls eine DB ausfällt |
| Datensicherheit           | Zugriffssteuerung einfacher bei kleineren Datenbeständen   |
| Flexibilität              | einfachere Änderung von Teilsystemen                       |
| Performance               | besser bei mehreren Rechnern                               |
| Kosten                    | tiefer als bei teuren Grosscomputern                       |

Tab. 7.1: Gründe für die Dezentralisierung

### 7.1.1 Zugriff über das Netz

![](../x_res/DB-Client.png)

Abb. 21: Beispiel-Konfiguration mit einem DB-Client und einem DB-Server

Der Zugriff vom Client auf den DB-Server erfolgt wie bisher, es ist nur jeweils zusätzlich die *Netzwerkadresse (IP-Adresse)* bzw. der *Rechnername (Hostname)* des Servers anzugeben. Befindet sich zwischen DB-Client und DB-Server eine Firewall, so muss *Port 3306* geöffnet werden, andernfalls ist eine Kommunikation nicht möglich.

**a) Verbindung zum DB-Servertesten**:

Der folgende Befehl prüft, ob der MySQL-Server auf dem angegebenen Rechner läuft. Mit dem Parameter *-h* ist der Server-Rechner anzugeben.

```SQL
>mysqladmin -h 172.20.0.18 ping

mysqld is alive
```

**b) Über das Netz auf die DB zugreifen**:

Auch der Monitor wird mit dem Parameter *-h* gestartet.

`>mysql -h 172.20.0.18`

Zugriff mit Benutzername und Passwort:

```SQL
>mysql -h 172.20.0.18 -u remote -p

Enter password: ******
```

Bei erstellter Verbindung, sind die Befehle gleich wie im lokalen Betrieb.

**c) Backup und Restore über das Netz ausführen**:

Um ein Backup einer DB auf einem anderen Rechner zu erstellen, ist die Rechner-Adresse anzugeben.

```SQL
C:> mysqldump -h 172.20.0.18 -u remote -p --opt morebooks > H:\backup.sql

******
```

![note](../x_res/note.png) Wegen dem Parameter `-p` erscheint die Passworteingabe-Aufforderung nicht auf Bildschirm sondern am Anfang des SQL-Skripts. Das Passwort ist ohne Aufforderung einzugeben.

```SQL
Enter password: -- MySQL dump 8.21

-- Host: 172.20.0.18 Database: morebooks

---------------------------------------------------------

-- Server version 4.0.12-nt
```

Auf der 1. Zeile des Backup-Skripts ist die Eingabeaufforderung mit dem Texteditor zu löschen. Beim Restore ist die Adresse des Zielrechners anzugeben:

```SQL
C:> mysql -h 172.20.0.18 -u remote -p morebooks \< H:\\backup.sql

Enter password: \*****
```

**d) Netzwerkzugriff auf den DB-Server zeitweise verbieten**:

Mit dem Eintrag `skip-networking` in der Konfigurationsdatei `C:\...\my.ini` des Servers ist ein Zugriff via TCP/IP (auch lokal) nicht mehr möglich.

```
[mysqld]

skip-networking
```

Ein Verbindungsversuch ergibt folgende Fehlermeldung:

`ERROR 2003: Can't connect to MySQL server on '172.20.0.18' (10061)
`

### 7.1.2 Das ODBC-Interface

ODBC (*Open Database Connectivity, middleware*) ist vor allem in der Windows-Welt ein oft verwendeter Mechanismus für den einheitlichen Zugriff auf verschiedene DB-Systeme. Obwohl schon ziemlich alt, wird ODBC von den meisten DB-Systemen unterstützt und wird noch für einige Zeit der wichtigste herstellerunabhängige Standard zur Kommunikation zwischen DB-Systemen sein. ODBC steht auch unter Unix zur Verfügung, wird dort aber seltener eingesetzt.

![](../x_res/ODBC.png)

Abb. 22: Einheitlicher Zugriff einer Applikation via ODBC auf verschiedene Datenbanken

Die meisten DB-Hersteller bieten *ODBC-Treiber* für ihre DB-Server an. Vor Verwendung von ODBC muss der für die entsprechende Datenquelle benötigte Treiber auf dem Client installiert werden. MariaDB unterstützt ODBC mit Hilfe von *mariaODBC (Connector/ODBC)*. MariaODBC ist ein 32/**64**-Bit-ODBC-Treiber für die Anbindung von ODBC-fähigen Applikationen an MariaDB.

(MariaDB.com >> Download >> Connectors)

**a) Installierte ODBC-Treiber kontrollieren**:

Um zu kontrollieren, welche ODBC-Treiber und welche Versionen auf dem Rechner installiert sind, wird über Start > Einstellungen > Systemsteuerung > Verwaltung > **ODBC-Datenquellen (64Bit)** der *ODBC-Administrator* gestartet. (Verion 10.xx oder höher)

![ODBC-Treiber](../x_res/ODBC-Treiber.png)

Abb. 23: Installierte ODBC-Treiber ermitteln (64Bit)

**b) Eine neue Datenquelle (DSN, Data Source Name) erstellen und konfigurieren**:

Das Einrichten von ODBC auf einem DB-Client umfasst a) die Installation des ODBC-Treibers und b) die Registrierung der Datenbank im ODBC Driver-Manager. Das Einrichten eines *DSN* erfolgt im ODBC-Administrator in einem von 3 Registern:

-   *Benutzer-DSN*: steht nur dem Benutzer zur Verfügung, der den DSN definiert
-   *System-DSN*: steht allen Benutzern zur Verfügung
-   *Datei-DSN*: für Dateien, auf die via ODBC zugegriffen wird (z.B. Excel-Tabelle)

Der DB-Zugriff erfolgt via DSN. Mit dem Knopf *Hinzufügen* wird ein neuer DSN erstellt.

![morebooks 1](../x_res/morebooks0.png)

![](../x_res/morebooks1.png)

![morebooks 2](../x_res/morebooks2.png)

Abb. 24, 25, 26: DSN für die MySQL-Datenbank morebooks konfigurieren

Mit *Data Source Name* wird angegeben, unter welchem Namen die Datenquelle in Zukunft angesprochen wird (z.B. DB- oder Projektname). "Test DSN" überprüft die Verbindung! Next drücken.

*Host/Server Name* ist der Name oder die IP-Adresse des Server-Rechners (localhost für den lokalen Rechner). In *Database Name* wird der Name der DB angegeben. Für *User* und *Password* gilt das Zugriffssystem des Servers (s. Kap. 6). Werden die beiden Felder leer gelassen, so sind die Angaben zu Beginn der 1. ODBC-Verbindung einzugeben. Restliche Einstellungen durchklicken.

![note](../x_res/note.png) Soll auf mehrere Datenbanken zugegriffen werden, muss für jede DB 1 DSN definiert werden.

**c) Konfigurierte DSN testen**:

Mit dem evtl. vorhandenen Knopf *Test Data Source* kann getestet werden, ob die Verbindung zur Datenbank funktioniert. Nicht alle ODBC-Driver unterstützen einen solchen Test.

**d) MyODBC-Optionen einstellen**:

Der Knopf *Next* führt zum weiteren Dialog mit mehreren Einstellungen.

### 7.1.3 Zugriff von Access auf eine MySQL-DB

Access und MySQL sind zwei grundverschiedene Programme. Access hat eine ausgereifte Benutzeroberfläche, wird aber langsam, wenn mehrere Benutzer gleichzeitig zugreifen. MySQL ist im Multi-User-Betrieb effizienter und sicherer, hat aber nicht die komfortable Bedienoberfläche von Access. Es liegt nahe, die Vorteile aus beiden Welten zu kombinieren, z.B.

-   Access als Benutzeroberfläche (GUI) für Zugriff und Veränderung der Daten in einer MySQL-DB
-   Access als Reportgenerator für Daten aus MySQL
-   Daten aus Access- und MySQL-Datenbanken kombinieren
-   Das Schema einer neuen DB zuerst in Access entwickeln und dann nach MySQL exportieren
-   In Access ER-Diagramme von MySQL-Datenbanken erstellen

![note](../x_res/note.png) Oft ergeben sich Probleme mit inkompatiblen Datentypen. In MySQL-Tabellen sollte DOUBLE statt FLOAT, DATETIME statt DATE verwendet werden. Die Datentypen BIGINT, SET und ENUM sind zu vermeiden, da Access diese nicht kennt.

**a) Tabellen einer externen DB in Access einbinden**:

Um von Access aus auf Daten einer anderen DB zuzugreifen, können deren Tabellen in Access eingebunden werden mit

-   Externe Daten > Neue Datenquellen > Aus anderen Quellen > ODBC_Datenbank ...
-   "Erstellen Sie eine Verknüpfung zur Datenquelle" wählen
-   Im "Datenquelle auswählen"-Dialog im Register *Computerdatenquelle* eine Datenquelle wählen

    ![](../x_res/Auswahl1.png) 
    
    ![](../x_res/Auswahl2.png)

    Abb. 27 & 28: Auswahl der SQL-Datenquelle

**b) ODBC-Zugriff auf verknüpfte Tabellen testen**:

Eingebundene Tabellen werden wie lokale Tabellen verwendet. Die Daten können editiert und erweitert werden.

![note](../x_res/note.png) Die Struktur eingebundener Tabellen lässt sich via ODBC nicht ändern. Das Löschen einer eingebundenen Tabelle löscht lediglich die Verbindung, die Tabelle selber bleibt bestehen.

**c) Abfrage mit verknüpften MySQL-Tabellen erstellen**:

Wie mit lokalen Tabellen lassen sich mit verknüpften Tabellen Abfragen und Formulare erstellen.

**d) Access-Formulare erstellen**:

Normalerweise arbeitet der Benutzer nicht direkt mit DB-Tabellen und SQL-Befehlen sondern verwendet entsprechende *Formulare*, *Masken* und *Menüs*.

---
---


# 8 Server-Administration

## 8.1 Server-Konfiguration

Nach der Standardinstallation funktioniert der DB-Server mysqld in den meisten Fällen problemlos. Für besondere Wünsche (z.B. andere Sprache oder Zeichensatz, bessere Performance) können beim Start des Servers (mysqld) sowie für das Ausführen von Admin-Programmen (z.B. mysql) Parameter (Optionen) angegeben werden.

Konfigurationsparameter werden auf der Kommandozeile oder in Konfigurationsdateien festgelegt.

**a) Parameter auf der Kommandozeile angeben**:

Der folgende CMD-Befehl startet den DB-Server mit der Option für Fehlermeldungen in Deutsch.

`C:> mysqld --language=german`

Der folgende Befehl startet den Monitor mit Benutzernamen und weiteren Parametern.

`C:> mysql --user=kunde --i-am-a-dummy --silent`

**b) Parameter in die Konfigurationsdateien eintragen (my.ini, my.cnf)**:

Da der DB-Server meistens als Dienst gestartet wird, ist es nicht möglich, direkt Parameter anzugeben. Darum werden Server-Parameter fast immer in einer Konfigurationsdatei angegeben. Lese-Reihenfolge siehe Seite 18!

`C:\...\mysql\data\my.ini C:\...\my.cnf`

Es gilt der zuletzt gelesene Wert. Werte auf der Kommandozeile haben Vorrang vor Einträgen in den Konfigurationsdateien. Es gelten die gleichen Angaben wie auf der Eingabezeile, aber ohne `--`.

```
# Server-spezifische Eintraege

[mysqld]

language=german
```

Statt beim Ausführen von Administrationsprogrammen immer wieder die gleichen Optionen anzugeben, können sie ebenfalls in eine der Konfigurationsdateien eingetragen werden.

```
# Monitor-spezifische Eintraege

[mysql]

user=meier

silent
```

### 8.1.1 MySQL-Konfigurationsparameter

Die Konfigurations-Optionen werden mit einem Texteditor oder mit der Workbench in die Konfigurationsdatei eingetragen. Angezeigt werden die Serverparameter mit dem Befehl

`mysqld --help`

Änderungen in den Konfigurationsdateien werden erst nach einem Neustart des betroffenen Programms wirksam. Für Server-spezifische Optionen muss der Server neu gestartet werden. **Auch unter Windows sind in den Konfigurationsdateien Pfade wie bei UNIX mit / anzugeben (statt mit \\).
**
![caution](../x_res/caution.png) Bei einem falschen Eintrag in einer Konfigurationsdatei (z.B. Tippfehler, - statt _) kann der Server nicht gestartet werden.

### 8.1.2 Systemvariablen

Diverse Statusinformationen des DB-Servers sind als Systemvariablen zugänglich. Angezeigt werden diese mit dem Befehl SHOW VARIABLES, z.B. eingestellter und vorhandene Zeichensätze:

`mysql> SHOW VARIABLES LIKE '%log%';`

## 8.2 Logging

Logging bezeichnet die Protokollierung von Änderungen in der Datenbank.

| Wozu Logging ? |                                                                                  |
|----------------|----------------------------------------------------------------------------------|
| **Monitoring**     | Protokoll von Start, Shutdown und Fehlern.  Wer hat wann welche Daten verändert? |
| **Sicherheit**     | Datenwiederherstellung zwischen regulären Backups                                |
| **Optimierung**    | Aufzeichnung von aufwendigen DB-Abfragen                                         |
| **Replikation**    | Logging des Master-Rechners für die DB-Synchronisierung                          |
| **Transaktionen**  | Durchführen von Transaktionen nach einem DB-Absturz                              |

Tab. 8.1: Ziele des Logging

Nachteile des Logging sind **Verlangsamung** des DB-Servers und zusätzlicher **Platzverbrauch** auf der Festplatte. Deshalb sind in der Standardeinstellung ausser dem Error-Log alle Protokolle ausgeschaltet.

![](../x_res/Log-Dateien.png) 

Abb. 29: Log-Dateien mit Konfigurationsparametern und Verarbeitungs-Tools

![note](../x_res/note.png) Änderungen der Logging-Parameter werden erst nach dem Neustart des Servers wirksam.

Für maximale Geschwindigkeit und Sicherheit sollten die Log-Dateien auf einer anderen Festplatte gespeichert werden als die DB-Dateien.

### 8.2.1 Binärdateien lesen mit mysqlbinlog.exe

Die meisten Logging-Protokolle werden in Binärdateien gespeichert. Sie können diese zwar mit einem normalen Editor öffnen, der Inhalt ist aber unverständlich, da binär codiert. Im MySQL-Binärverzeichnis gibt es ein Programm zur Anzeige der Binärlogdateien: mysqlbinlog.exe. Starten Sie dieses Programm und geben die Datei als Parameter mit und der Dateiinhalt wird Ihnen auf dem Konsolenfenster ausgegeben.

### 8.2.2 Error Log (\<host\>.err)

Das Fehlerprotokoll protokolliert jeden *Start* und *Shutdown* des MySQL-Servers sowie alle *Fehlermeldungen* des Servers in Textform.

**a) Error Log konfigurieren**:

```
# Error Log-Datei festlegen (Default: <basedir>\data\<host>.err)

log-error=C:/log/error.txt
```

**b) Error Log kontrollieren**:

Jeder Eintrag ist mit Datum yymmdd und Zeit hh:mm:ss versehen.

`C:\>TYPE C:\mysql\data\al27785.err`

![note](../x_res/note.png) Das Error Log kann nicht ausgeschaltet werden.

### 8.2.3 Binäres update Logging

Der Parameter #log-bin=mysql-bin ist in der Konfigurationsdatei von MySQL standardmässig auskommentiert. Wenn Sie das Kommentarzeichen entfernen, können Sie den Dateinamen anpassen und ab dem nächsten Serverstart wird eine Datei mit der Endung \*.000001 angelegt, die alle Anweisungen enthält, die Daten verändern. Normale Select-Anweisungen werden also nicht abgespeichert.

Bei folgenden Bedingungen wird eine neue Logdatei erstellt und die Extension um eins hochgezählt:

-   beim Neustart des DB-Servers
-   beim Erreichen der Maximalgrösse, die mit dem Parameter max_binlog_size definiert wird
-   durch den Befehl FLUSH LOGS

Zusätzlich wird eine Indexdatei mit der Endung .index erzeugt, die alle Dateinamen der bisherigen Binärlogdateien aufzeichnet.

Seit der MySQL-Version 5.0 ersetzt das Binärlogging das vorherige Update-Logging.

Gemäss MySQL-Manual 5.1 ist die Leistungseinbusse des Datenbankservers durch das aktivierte Binär-Logging nur 1%. Für einige Anwendungen ist es ohnehin essentiell nötig: **Transaktionen**, **Replikationen**, **Recovery** nach Systemabstürzen …

### 8.2.4 Binärlog-Datei anzeigen:

Die Einträge in der Log-Datei sind binär dargestellt. Das heisst, dass wir mit einem gewöhnlichen Text-Editor keine lesbare Darstellung bekommen. Um den Inhalt der Datei anzuzeigen benötigen Sie den speziellen Client mysqlbinlog, der sich im mysql/bin-Verzeichnis befindet.

`C:\...\mysql\bin>mysqlbinlog -u root -p C:\...\MySQL\data\mysql-bin.000001`

### 8.2.5 General Query Log (\<host\>.log)

Das Login- und Operationssprotokoll zeichnet auf, welcher Benutzer welche Daten liest oder ändert. Dazu wird *jeder Verbindungsaufbau und jeder Befehl* an den DB-Server aufgezeichnet. Dieses Protokoll kann nicht für die Wiederherstellung von Daten verwendet werden und wird sehr schnell sehr gross!

`mysql> SET GLOBAL general_log=1;`

### 8.2.6 Slow Query Log (\<host\>-slow.log)

Dieses Protokoll zeigt Datenbankabfragen, die den grössten Aufwand verursachen. Die Analyse dieser Abfragen kann Hinweise für die Verbesserung der *Server-Performance* geben.

### 8.2.7 Transaktions-Log (ib_logfile1, -2, ...)

In den InnoDB-Logging-Dateien wird jede Änderung in *InnoDB-Tabellen* aufgezeichnet. (s. Kap. 5*)*. Dies ermöglicht grosse Transaktionen sowie die Wiederherstellung nach einem Server-Absturz.

Mit `COMMIT` wird eine Transaktion vorerst nur ins Transaktions-Log geschrieben. Die geänderten Tablespace-Seiten werden aus Geschwindigkeitsgründen erst nach und nach auf die Festplatte übertragen. Kommt es in der Zwischenzeit zu einem Absturz, wird der Tablespace mit Hilfe der Log-Dateien wiederhergestellt.  

Hinweis: `maria_log` (mariaDB-Engine für Transaktionen!)

**Transaktions-Log konfigurieren**:

Die Log-Dateien werden der Reihe nach befüllt. Wenn die letzte Datei voll ist, beginnt der *InnoDB-Tabellentreiber*, wieder Daten in die erste Log-Datei zu schreiben.

![note](../x_res/note.png) Transaktions Log-Dateien können nicht mit dem Texteditor kontrolliert werden. Sie sind nur im Server-Betrieb erforderlich und werden nach dem korrekten Server-Shutdown nicht mehr benötigt.

## 8.3 Backup und Restore

Während des Betriebs eines DB-Servers wird regelmässig ein Backup erstellt. Durch einen Crash können sämtliche Daten verloren gehen.

![](../x_res/Backup.png)

Abb. 30: Backup, Logging und Restore

**Restore** (Recovery): Zuerst wird das letzte Backup eingelesen, anschliessend der Reihe nach alle seitdem erstellten Update Log-Dateien. Damit lässt sich der Zustand vor dem Crash herstellen.

| Backup- und Restore-Befehle                    |         |                         |
|------------------------------------------------|---------|-------------------------|
| `mysqldump [optionen] db [tb1 tb2 ...] > datei` | Backup  | allgemeine Syntax       |
| `mysqldump --help`                               | "       | Anzeigen aller Optionen |
| `mysqldump hotel > backup.sql`                  | "       | ganze Datenbank         |
| `mysqldump hotel person buchung > bkp.sql `     | "       | 2 Tabellen              |
| `mysqldump --opt ...`                            | "       | optimale Einstellung    |
| `mysql [optionen] db < datei`                   | Restore | allgemeine Syntax       |

Tab. 8.2: Beispiele von Befehlen für Backup und Restore

**Backup mit mysqldump erstellen**:

Das Programm erzeugt pro Tabelle einen CREATE TABLE-Befehl um die Tabelle samt Indizes zu erzeugen, sowie die INSERT-Befehle, um die Daten einzutragen. Mit mysqldump können einzelne Tabellen oder ganze DBs gesichert werden.

Damit bei der Ausgabeumleitung in eine Datei die Enter password-Zeile nicht in die Zieldatei umgeleitet wird, kann das Passwort mit --password=x direkt eingegeben werden.

Die Option `--opt` fasst mehrere Optionen zusammen und bewirkt eine *optimale Einstellung*.

`C:\>mysqldump --password=pwd --opt hotel > backup.sql`

**Integrität des Backup mit READ Lock gewährleisten**:

Während des Backups dürfen die Daten nicht durch einen anderen Client verändert werden. Ein READ LOCK für die betroffenen Tabellen wird mit dem Parameter `--lock-tables` von mysqldump erreicht.

`C:\>mysqldump --lock-tables hotel > backup.sql`

![note](../x_res/note.png) Ist auch nur eine Tabelle der DB von einem anderen Client mit einem `WRITE LOCK` blockiert, so wird das Backup erst nach der Freigabe (`UNLOCK TABLES`) gestartet.

**Datenbank wiederherstellen (Restore)**:

Dafür wird der Monitor verwendet.

`C:\>mysql hotel < backup.sql`

**Datenbank aus einem Update-Log wiederherstellen**:

Die Rekonstruktion der DB beginnt mit dem Einlesen des letzten vollständigen Backup. Anschliessend werden die Update Log-Dateien benötigt, die seit dem letzten Backup entstanden sind. Die darin enthaltenen SQL-Befehle werden mit Hilfe von `mysqlbinlog` gelesen und mit **|** (=pipe) an mysql übergeben.

```
C:\>mysqlbinlog al27785-bin.004 | mysql

C:\>mysqlbinlog al27785-bin.005 | mysql
```

![note](../x_res/note.png) Die älteste Log-Datei wird zuerst ausgeführt.

## 8.4 Import und Export

Import und Export bezeichnen die Übertragung von Daten zwischen Textdatei und DB-Tabelle.

![](../x_res/Export.png) 

Abb. 31: Export und Import für die Übertragung von Daten

Damit die Datenstruktur bei der Umwandlung in die DB-Tabelle richtig interpretiert wird, müssen die einzelnen Felder und Zeilen markiert werden. Die **Trennzeichen (Delimiter)** sind so zu wählen, dass keine Verwechslung mit dem Textinhalt möglich ist.

| Import- und Export-Befehle u. Parameter |                                                       |
|-----------------------------------------|-------------------------------------------------------|
| `SELECT .. INTO OUTFILE`                  | exportiert das Abfrage-Ergebnis in eine Text-Datei    |
| `TRUNCATE TABLE tb;`                      | löscht den gesamten Tabelleninhalt der Tabelle        |
| `LOAD DATA INFILE `                       | importiert Inhalt einer Textdatei in eine DB-Tabelle  |
| `FIELDS TERMINATED BY ';'`                | z.B. ;, trennt einzelne Felder (Feldtrennzeichen)     |
| `ENCLOSED BY '"' `                        | z.B. ", umschliesst einzelne Textfelder               |
| `LINES TERMINATED BY '\r\n' `           | Zeilentrennzeichen für Windows-Textdateien            |

Tab. 8.3: Beispiele von Import- und Export-Befehlen und Definition von Trennzeichen (Delimiter)

![note](../x_res/note.png) Textimport ist oft problematisch, z.B. wegen Datum-Formatierungen. Warnings > 0 zeigen *Importfehler*, die dann evt. mit Hilfe des `SELECT`-Befehls gefunden werden. Es ist darauf zu achten, dass die importierten Daten die referenzielle Integrität der Datenbank erfüllen.

**Export mit SELECT ... INTO OUTFILE**:

Es handelt sich um einen gewöhnlichen `SELECT`-Befehl mit Ausgabe in eine Textdatei. Die Trennzeichen werden mit den *Exportoptionen* vor dem `FROM`-Teil angegeben.

Der `SELECT`-Befehl kann zusätzlich beliebige Auswahl- und Sortieranweisungen enthalten.

## 8.5 Speicherbedarf abschätzen

Bei der Planung eines DB-Servers ist der voraussichtlich benötigte Speicherplatz zu berücksichtigen. Neben den eigentlichen Benutzerdaten (Tabellendaten) wird auch Speicherplatz für Indexe (Indextabellen) und die Systemverwaltung (DB- und Tabellenbeschreibungen, Systemkatalog, User- und Zugriffsverwaltung) benötigt.

Das Abschätzen des benötigten Speicherplatzes ist ein ungenaues Unterfangen wegen Tabellenspalten variabler Länge (`VARCHAR`), nicht bekannter Anzahl und Grösse der Log-Dateien, temporären Bereichen für bestimmte SQL-Befehle (z.B. `ORDER BY`), Datenträgerfragmentierung etc.

*Speicherbedarf pro Tabelle (MyISAM)*

| Nutzdaten ( \*.MYD )   | Anzahl_Datensätze * [SUM (Bytes_pro_Attribut1..n) + 5] |
|----------------------|---------------------------------------------------------|
| Indizes ( \*.MYI )     | Anzahl_Schlüssel * [(Schlüssellänge + 4) / 0.67]       |
| Systemdaten ( \*.FRM) | 8500 + (Anzahl_Attribute * 40)                         |

Tab. 8.1: Formeln für die ungefähre Berechnung des Speicherverbrauchs pro Tabelle

`Bytes_pro_Attribut` und Schlüssellänge werden dem MySQL-Manual entnommen. Die Indextabellen werden in Blöcken (d.h. Mehrfachen) von 1024 Byte abgespeichert. Für `Anzahl_Schlüssel` kann die Anzahl Datensätze eingesetzt werden.

Die Belegung des variablen Datentyps `VARCHAR` ergibt sich als Mittelwert einer Anzahl Test-Einträge.

---
---

# 9 Optimierung

Die Optimierung des Betriebs einer Datenbank ist eine komplizierte Aufgabe, weil sie ein umfassendes Verständnis des gesamten DB-Systems voraussetzt.

| Wozu Optimierung ?       |                                               |
|--------------------------|-----------------------------------------------|
| **Performance** verbessern   | schnellere Ausführung von SQL-Befehlen        |
| **Speicherplatz** einsparen  | schnellere Übertragung von/auf die Festplatte |
| **Portabilität** ermöglichen | Übertragen der DB auf einen anderen Server    |

Tab. 9.1: Ziele der Optimierung

Das wichtigste, um ein System schnell zu machen, ist das grundlegende Design. Ausserdem ist es wichtig zu wissen, was das System macht und welches mögliche *Flaschenhälse* sind. Engpässe sind z.B.:

-   *Suchvorgänge auf der Festplatte*: Die Festplatte benötigt Zeit, um die angeforderten Daten zu finden. Pro Sekunde können etwa 1000 Suchvorgänge durchgeführt werden. Eine Möglichkeit der Optimierung besteht darin, Daten auf mehrere Platten zu verteilen.

-   *Lesen von / Schreiben auf Festplatte*: In der richtigen Position kann die Festplatte etwa 10 bis 20 MB pro Sekunde übertragen. Dies ist leichter zu optimieren als Suchvorgänge, da von mehreren Festplatten parallel gelesen werden kann.

| Was wird optimiert? |                                                    |
|---------------------|----------------------------------------------------|
| **Datenbankstruktur**   | minimaler Speicherplatz, Index-Verwendung          |
| DB-**Abfragen**         | Abfragen mit EXPLAIN analysieren                   |
| **Locks**               | Geschwindigkeit erhöhen durch Sperren von Tabellen |
| DB-**Server**           | Serverparameter einstellen                         |

Tab. 9.2: Objekte der Optimierung

## 9.1 Laden von Daten, Tabellenspeicherplatz

### 9.1.1 Laden von Daten optimieren

1.  **SQL-Skripts optimieren mit --opt**:

Diese `mysqldump`-Option ergibt ein Skript für das schnellstmögliche Einlesen in einen MySQL-Server (s.a. Kap. 8.3, Seite 46). Der folgende Befehl erstellt ein SQL-Skript für eine einzelne Tabelle der DB mybooks.

`C:\> mysqldump --opt hotel person > backup.sql`

Der Parameter `--opt` umfasst die Optionen

| Parameter           | Kommentar |
|-----------------|--------------------------------------------------------------------------|
| `quick`           | beschleunigt das Erstellen durch Vermeiden des Zwischenspeicherns im RAM |
| `add-drop-table`  | fügt vor jedes CREATE TABLE einen DROP TABLE-Befehl ein                  |
| `add-locks`       | fügt für schnelles Einlesen LOCK und UNLOCK TABLE-Befehle ein            |
| `extended-insert` | erzeugt für schnelles Einlesen wenige INSERT mit mehreren Datensätzen    |
| `lock-tables`     | führt ein LOCK TABLE READ aus (Datenintegrität)                          |

In den meisten Fällen ist dies eine optimale Einstellung.

**a) Schnelleres Laden von Daten mit LOAD DATA INFILE**:

Viel schneller als das Laden von Daten mit vielen INSERT-Befehlen ist das Importieren der Daten aus einer Textdatei (s.a. Kap. 8.4, Seite 48), z.B.

```
mysql> LOAD DATA INFILE 'c:/person.txt' INTO TABLE person
FIELDS TERMINATED BY ';' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n';
```

### 9.1.2 Tabellenspeicherplatz optimieren (nur MyISAM)

**b) OPTIMIZE TABLE verwenden**:

Werden aus einer Tabelle Datensätze gelöscht oder Änderungen durchgeführt, so ist die Datenbankdatei grösser als notwendig und die Daten sind über die ganze Datei verstreut, was den DB-Zugriff verlangsamt.

`OPTIMIZE TABLE` entfernt nicht genutzten Speicherplatz aus einer Tabellendatei und sorgt dafür, dass zusammengehörende Daten eines Datensatzes auch zusammen gespeichert werden.

`mysql> OPTIMIZE TABLE person;`

Bei Tabellen, deren Inhalt sich häufig ändert (viele DELETE- und UPDATE-Befehle), sollte der Befehl regelmässig ausgeführt werden. Damit wird die Datei verkleinert und der Zugriff beschleunigt.

## 9.2 Einsatz von Indextabellen

Eine Suche ist grundsätzlich nur dann effizient, wenn sie in einem sortierten Suchraum arbeitet. Das beste Beispiel ist das Telefonbuch. Wären die Personen nicht alphabetisch sortiert, bliebe uns bei der Suche nach einer Telefonnummer nicht anderes übrig, als das Telefonbusch sequentiell durchzulesen, bis wir auf die gesuchte Person stossen – im schlimmsten Fall also bis zum Ende.

Um in grossen unsortierten Listen einen gewünschten Wert zu finden müssen wir also die lineare oder sequentielle Suche anwenden. Auch wenn wir nach einem Wert suchen, der gar nicht vorhanden ist, können wir das erst am Ende des Suchraumes feststellen. Aus diesem Grund ist es einfacher, wenn wir Listen – oder konkret in Datenbanken dann Attribute – sortieren, um schnellere Verfahren anwenden zu können.

Bei der binären Suche wird eine sortierte Liste vorausgesetzt. Damit halbiert sich bei jedem Zugriff der Suchraum. Wir benötigen bei N Elementen 2log N Zugriffe. Wenn unser Suchraum aus 16 (=24)Werten besteht, benötigen wir also maximal 4 Zugriffe (vorherige „Glückstreffer“ ausgenommen). Mit nur einem Zugriff mehr, können wir bis zu 32 Werte abdecken. Allerdings benötigen wir auch für 17 Werte maximal 5 Zugriffe. Für dieses Suchverfahren wird auch noch der Begriff „logarithmische Suche“ verwendet.

Eine Erweiterung der binären Suche ist die Interpolationssuche. Bei der binären Suche wird der Suchraum jedes Mal genau in der Mitte geteilt. Die Interpolationssuche berücksichtigt noch die Grösse des gesuchten Wertes in Bezug auf den Suchraum. Es vergleicht also den grössten und den kleinsten Wert, geht von einer gleichmässigen Verteilung der Daten aus und teilt den Suchraum jetzt im Verhältnis zu dem Suchwert. Konkret greift es bei einem sehr hohen Wert bereits im „wahrscheinlicheren“ hinteren Bereich des Suchraumes zu. Jeder erfolgreiche Zugriff schränkt den Suchraum also um mehr als die Hälfte ein. Mit der zusätzlichen Berechnung des Zugriffselementes geht aber ebenfalls Rechenzeit verloren. Daher ist die Interpolationssuche nur bei grossen Suchräumen effizienter.

Ein leicht verständliches Zahlenbeispiel finden Sie bei Wikipedia: Interpolationssuche.

Wenn wir aber Tabellen mit mehreren Attributen haben, auf die wir häufig zugreifen, können wir ja trotzdem nur nach einem Attribut sortieren. Die Lösung ist die Verwendung von Indextabellen, die wir pro Attribut definieren können. Für jedes indizierte Attribut wird eine zusätzliche „Hilfstabelle“ angelegt, die nur das – jetzt sortierte Element – und einen Schlüssel auf die Originaltabelle enthält. Diese Indextabellen werden vom Datenbankmanagementsystem verwaltet. Zugriffe und Updates liegen also weder beim Anwender noch beim Datenbankentwickler.

Dadurch wird die Datenbank etwas grösser (auch die Indextabellen müssen gespeichert werden). Die Einfügeoperationen (INSERT) sind etwas aufwendiger, da auch die Indextabellen aktualisiert werden müssen. Aus dem gleichen Grund werden Aktualisierungen, die indizierte Attribute betreffen aufwendiger. Der Zeitvorteil liegt aber bei den Abfragen. Da aber in der Praxis die Antwortzeit bei Abfragen viel kritischer ist als die Antwortzeit von Einfügeoperationen, sind die Nachteile aktzeptierbar.

## 9.3 Abfrage- und Indexoptimierung

Die Performance einer Datenbank lässt sich erst dann abschätzen, wenn die Datenbank genügend Testdaten enthält. Eine Datenbank mit einigen Hundert Datensätzen befindet sich nach den ersten 3 Abfragen meistens vollständig im RAM, so dass alle Abfragen mit oder ohne Index sehr schnell beantwortet werden. Eine Abfrage-Optimierung lohnt sich nur dann, wenn die Tabellen über 10000 Datensätze enthalten oder die Gesamtgrösse der DB die RAM-Grösse auf dem DB-Server überschreitet.

### 9.3.1 Langsame Abfragen in der Slow Query Log (host-slow.log)

**Slow Query Log kontrollieren (host-slow.log)**:

Dieses Protokoll zeichnet alle langsamen Abfragen samt Ausführungszeit in Textform auf.

`C:\>TYPE C:\...\mysql\data\al27785-slow.log`

### 9.3.2 EXPLAIN SELECT verwenden (Ausführungsplan)

Wird einem SELECT-Statement das Schlüsselwort `EXPLAIN` vorangestellt, so erklärt MySQL, wie das `SELECT` ausgeführt würde. Die angegebenen Informationen zeigen, wie und in welcher Reihenfolge die Tabellen verknüpft werden (*Ausführungsplan*). Mit Hilfe von `EXPLAIN` lässt sich erkennen, wo Indexe hinzugefügt werden müssen.

Die zu untersuchende Abfrage ermittelt die Anzahl Titel aller Verlage, deren Name mit 'A' beginnt.

```SQL
mysql> EXPLAIN SELECT COUNT(*)
FROM buchung, person
WHERE buchung.PersID = person.PersID;
```

Die `table`-Spalte zeigt die Tabellen in der Reihenfolge, in der sie gelesen würden. Die `key`-Spalte gibt den Index an, den MySQL benutzen würde und ist NULL, wenn kein Index verwendet wird. Die `rows`-Spalte gibt die Anzahl von Zeilen an, die für die Abfrage voraussichtlich untersucht werden müssen.

![note](../x_res/note.png) Einen Anhaltspunkt über den Suchaufwand erhält man durch Multiplizieren aller Werte in der *rows*-Spalte. Das Resultat sollte möglichst klein sein.

| Optimierungshinweise                                                       |
|----------------------------------------------------------------------------|
| Indexe beschleunigen die Abfrage aber verlangsamen Änderungen              |
| Index auf Primär- und Fremdschlüssel                                       |
| Indexe auf Attribute, die häufig sortiert werden                           |
| NOT und <> können nicht optimiert werden                                 |
| Abfragen mit LIKE sind nur optimierbar, wenn % nicht am Musteranfang steht |
| Abfragen mit Funktionen sind nicht optimierbar                             |

Tab. 9.3: Abfrage-Optimierung mit Indizes

Ausführliche Tipps für das Optimieren enthält das MySQL-Manual.

## 9.4 Server-Tuning

Server Tuning bezeichnet die optimale *Konfiguration* des DB-Servers, so dass die Hardware möglichst gut genutzt und die SQL-Befehle so schnell wie möglich ausgeführt werden.

![note](../x_res/note.png) Im allgemeinen lohnt sich Server-Tuning nur bei sehr grossen Datenmengen (GByte) und sehr vielen DB-Zugriffen pro Sekunde.

### 9.4.1 Optimale Speichernutzung

Beim Start des DB-Servers wird ein Teil des Hauptspeichers für bestimmte Aufgaben reserviert, z.B. als Platz zum Sortieren von Daten.

| Wichtige Speicherparameter |                                                               |
|----------------------------|---------------------------------------------------------------|
| `key_buffer_size`            | für Indizes reservierter Speicher (Default: 8M)               |
| `table_cache`                | maximal Anzahl geöffneter Tabellen (Default: 64)              |
| `sort_buffer`                | Buffergrösse zum Sortieren oder Gruppieren (Defaut: 2M)      |
| `read_buffer_size`           | Speicher für sequentielles Lesen (Default: 128K=131072 Bytes) |

Tab. 9.4: Wichtige Konfigurationsparameter für die Speicherverwaltung

### 9.4.2 Query Cache

Die Aufgabe des *Query Cache* besteht darin, die Ergebnisse von SQL-Abfragen zu speichern. Wenn später exakt dieselbe Abfrage wieder durchgeführt wird, so kann das fertige Ergebnis verwendet werden.

**a) Query Cache konfigurieren**:

```
# Query Cache einschalten

query_cache_size = 32M.       <<<<<<<<<<<

# Query Cache Modus 0=Off, 1=On (Default), 2=Demand

query_cache_type = 1

# max. Grösse für eine Abfrage

query_cache_limit = 50K
```

Damit werden 32 MByte RAM reserviert. Es werden nur Abfrage-Resultate gespeichert, die weniger als 50 kByte benötigen, was vermeidet, dass grosse Ergebnisse alle anderen aus dem Cache verdrängen.

**b) Query Cache verändern**:

Der Query Cache kann für eine Verbindung speziell eingestellt, z.B. ausgeschaltet werden.

`mysql> SET query_cache_type=0;`

**c) Query Cache-Status abfragen**:

Mit SHOW STATUS; werden die Werte verschiedener Statusvariablen des Query Cache angezeigt.

`mysql> SHOW STATUS;`

**d) Query Cache leeren**:

Mit folgendem Befehl werden alle Einträge im Cache gelöscht.

`mysql> RESET QUERY CACHE;`



---

---


# Literatur und Linkverzeichnis

1.  Kofler M.: MySQL. Addison-Wesley. 2003. ISBN 3-8273-2046-1
2.  [http://www.apachefriends.de](http://www.apachefriends.de/)
3.  <http://www.mysql.de>
4.  <http://dev.mysql.com/doc/refman/5.1/de/index.html> das Handbuch in deutsch
5.  <http://www.w3schools.com>
6.  <http://selfhtml.teamone.de>
7.  <http://www.phpmyadmin.net>
8.  <http://www.php.net>
9.  <http://www.apache.org>
10. <http://www.convert-in.com-> ... einfach mal selber weitersuchen.
11. Informationssysteme und Datenbanken; Carl August Zehnder; vdf Hochschulverlag AG Zürich; ISBN 3-7281-3002-8; Preis ca. 47,- CHF
12. Theorie und Praxis relationaler Datenbanken; René Steiner; vieweg-Verlag; ISBN 3-528-25427-0; Preis der neueren Ausgabe ca. 35,- CHF
13. Herdt-Verlag; Datenbanken aus der Reihe ECDL Modul 5; Preis ca. 22,- CHF
14. Objektorientierte Datenbanken; John G. Hughes; Hanser Verlag; ISBN 3-446-16583-5
15. Der Software-Entwicklungsprozess; Andreas Frick; Hanser Verlag; ISBN 3-446-17777-9
16. Datenmodellierung und Datenbankentwurf; Josef L. Staud; Springer Verlag 2005; ISBN 3-540-20577-2; Preis 76,50 CHF
17. Database System Concepts; Silberschatz, Korth, Sudarshan; Verlag McGraw-Hill; ISBN 0-07-295886-3; Preis 109,- CHF für die „fifth edition“ – seit 01/10 gibt es die 6th edition
