![TBZ Logo](../x_res/tbz_logo.png)

![TBZ Logo](../x_res/XAMPP.jpg)

# M141 - Datenbanksystem in Betrieb nehmen

## Script und Aufgaben (BiVo 2014 BI)

Die Unterlagen zum Modul: [Script](./Script.md)

---

Die Tutorial und Übungen zum Modul: [Aufgaben](./Aufgaben/Skript_V6_3_Aufgaben.pdf). ---> Benötigte Daten finden Sie hier: [Datenpool](../Daten)

Die Repetition Kapitel 2 & 3: [Repetition](./Aufgaben/Rep.Aufgaben_Kap.2&3.pdf)



Die Repetition Kapitel 4: [Repetition](./Aufgaben/Rep.Aufgaben_Kap.4.pdf)

(Werden ausgedruckt abgegeben!)

