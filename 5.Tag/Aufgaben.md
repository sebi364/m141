![](../x_res/tbz_logo.png)

# M141 - DB-Systeme in Betrieb nehmen


# ![](../x_res/CP.png) Checkpoint X.Tag




## 4.2 Dump und Skripte

1.  Wohin werden die SQL-Statements geschrieben, wenn Sie die Hilfsdatei mysqldump.exe nur mit dem Datenbanknamen als Parameter aufrufen (Berechtigungen vorausgesetzt)?  
    Beispiel: C:\\\> … \\mysqldump firma

    ___   
      
2.  Mit welchem Befehl erstellen Sie ein Backup der Datenbank firma?

    ___   
      
3.  Wie führen Sie das SQL-Skript BACKUP.SQL auf die Datenbank FIRMA aus?

    ___   
      
4.  Was passiert hier: C:\\mysql\\bin\>mysql firma \< abfrage.sql \> ergebnis.txt –u benutzername -p


    ___   
      
5.  Verwenden Sie die Datei db_nahrung.sql und editieren Sie sie so, dass beim Einlesen der Datei mittels mysql.exe aus dem Betriebssystem-Konsolenfenster automatisch die neue Datenbank erstellt wird. Falls es eine Datenbank mit dem Namen schon gibt, soll diese gelöscht werden – andernfalls darf keine Fehlermeldung erscheinen

    ___   
      

6.  Öffnen Sie ein weiteres Konsolenfenster und starten Sie den Datenbank-Client. Laden Sie jetzt die Statements der Datei personen.sql mittels des source-Statements.

    ___   
      

7.  Erzeugen Sie eine weitere Tabelle nach eigenen Vorstellungen und erstellen Sie eine Textdatei mit passenden Daten. Lesen Sie die Datei von der Betriebssystem-Konsole mit mysqlimport ein und ausserdem aus dem MySQL-Client-Fenster mit LOAD DATA INFILE. Verwenden Sie die Informationen „Client- und Hilfsprogramme“ aus dem MySQL-Manual (online).


    ___   
      

8.  Erstellen Sie jetzt von der Konsole einen Dump, der vor jedem CREATE TABLE ein DROP TABLE IF EXISTS einführt.

    ___   
      



## 4.4 Skripte erstellen und einsetzen

1.  Erstellen Sie eine Textdatei, die ein SQL-Select-Statement enthält, das Ihnen alle Einträge der Autorentabelle in eine Textdatei schreibt.  
     SELECT \* INTO OUTFILE 'ergebnis.txt' FROM autoren;  
     mysql buchverwaltung \< select_to_file.txt -u krauthammer -p
2.  Erstellen Sie ein Skript, dass in der Tabelle *author* 2 neue Spalten einfügt: *surname* und *first_name*, beide als *VARCHAR(50)*. Anschliessend sollen die Vor- und die Nachnamen aus dem bestehenden Attribut *author* mit SQL-Textfunktionen herausgefiltert werden und in die neuen Attribute abgespeichert werden. Wenn das erledigt ist, kann das ursprüngliche Attribut entfernt werden.  
     Datei kor_author.sql  
     ALTER TABLE author ADD (first_name VARCHAR(50), surname VARCHAR (50));  
     UPDATE author SET surname=SUBSTRING(author FROM 1 FOR (LOCATE (',', author))-1);  
     UPDATE author SET first_name=SUBSTRING(author,(LOCATE (',', author))+2,20);  
     ALTER TABLE author DROP COLUMN author;
3.  Erstellen Sie ein Skript, dass eine Textdatei mit den Attributen *first_name, surname, title, year, publisher* erzeugt. Die Felder sollen mit Tabulatoren getrennt werden (\\t), die Zeilen mit *line-separators* (\\r\\n) getrennt werden.  
     Datei buchliste.sql  
     SELECT first_name, surname, title, year, publisher  
     INTO OUTFILE 'buchliste.txt'FIELDS TERMINATED BY '\\t' LINES TERMINATED by '\\r\\n'  
     FROM author INNER JOIN rel_title_author  
     ON author.authorID=rel_title_author.authorID  
     INNER JOIN title ON title.titleID=rel_title_author.titleID  
     LEFT JOIN publisher ON title.publisherID=publisher.publisherID
4.  Erstellen Sie eine neue Datenbank. Sie können Sie zum Beispiel *hotel_schlecht* nennen. Importieren Sie den Dump *hotel_schlecht_basis.sql*. Jetzt haben Sie eine einzige Tabelle vollkommen unnormalisiert. Öffnen Sie die Datei *hotel_schlecht.sql* in einem Editor. Analysieren Sie die Statements und überlegen sich, was ausgeführt wird. Lassen Sie die Statements ausführen – einzeln oder insgesamt. Sie können die Statements abändern und damit herumexperimentieren. Notfalls müssen Sie halt alles löschen (DROP) und neu importieren. Einen „Back“-Button gibt es hier nicht. Erweitern Sie die Statements nach eigenen Vorstellungen um die Datenbank in Richtung 3. Normalform zu verbessern.



    
    ___   
      

# 7 DB-Server im LAN

## 7.1 Aufgaben

1.  Netzverbindung zum Server-Rechner testen:

    Verifizieren Sie mit dem entsprechenden Befehl, dass der Server-Rechner vom Client-Rechner aus über das Netz erreichbar ist. Verwenden Sie zur Adressierung des Server-Rechners die vorher bestimmte IP-Adresse.

2.  Verbindung zum DB-Server testen

    Testen Sie mit dem entsprechenden Befehl die Verbindung zum DB-Server. Das Resultat *mysqld is alive* zeigt zudem, dass der DB-Server läuft.

3.  Über das Netz auf den DB-Server zugreifen

    Starten Sie auf dem Client-Rechner den MySQL-Monitor als Benutzer remote.

    Geben Sie die IP-Adresse Ihres DB-Server-Rechners an.

    Fragen Sie den Status des Servers ab und untersuchen Sie die Angaben für *Current user* und *Connection*.

    Testen Sie, ob Sie vom Client aus Zugriff auf alle Tabellen der DB morebooks haben und eine beliebige SQL-Abfrage darauf ausführen können.

4.  Backup und Restore über das Netz ausführen

    Erstellen Sie als Benutzer remote ein Backup der Datenbank morebooks in der Datei C:\\temp\\backup.sql.

    Löschen Sie mit dem Texteditor in der Backup-Datei die Passwort-Aufforderung und überschreiben Sie den ersten Autorennamen mit dem eigenen Namen.

    Spielen Sie das Backup auf den Server zurück und kontrollieren Sie mit einer SQL-Abfrage, dass der erste Autor Ihren Namen hat.

5.  Netzzugriff auf den DB-Server vorübergehend verbieten

    Tragen Sie auf dem Server-Rechner in die Konfigurationsdatei die Option ein, welche den Zugriff über das Netz verbietet. Starten Sie den Server erneut, damit der Eintrag wirksam wird.

    Versuchen Sie, vom Client aus, eine DB-Abfrage auszuführen. Dies sollte nun nicht mehr möglich sein.

    Lassen Sie auf dem Server den Netzzugriff wieder zu und prüfen Sie, dass Sie nun auf dem Client die DB-Abfrage ausführen können.

    Überlegen Sie, in welchen Fällen das temporäre Sperren des Netzzugriffs sinnvoll ist.

## 7.2 ODBC

1.  Installierte ODBC-Treiber auf dem Standard-PC bestimmen

    Suchen Sie auf dem Standard-PC den ODBC-Administrator, d.h. eine Datei *\*odbc\*.exe*.

2.  Datenquelle (DSN) erstellen und testen

    Erstellen Sie im Register *System-DSN* eine neue Datenquelle *hotel* für den von Ihnen auf dem DB-Server erstellten Benutzer *remote*. Tragen Sie dabei als Host die IP-Adresse des Server-PC ein (an Stelle von localhost).

    Testen Sie, dass der von Ihnen konfigurierte DSN funktioniert. Dies können Sie nur ausführen, falls der installierte ODBC-Treiber dies unterstützt.

3.  ODBC ausprobieren. Testen Sie die folgenden Fälle:
-   Zugriff via ODBC bei abgestelltem DB-Server
-   Eintrag einer falschen IP-Adresse im DSN
-   Eintrag eines falschen DB-Namens im DSN
-   Eintrag eines falschen Usernamens im DSN
-   Eintrag eines falschen Passworts im DSN

## 7.3 Zugriff von Access auf MySQL

1.  MySQL-Tabellen in Access einbinden
-   Starten Sie auf dem Client-PC Access und erstellen Sie eine neue Access-DB. Geben Sie ihr den Namen der DB, d.h. *hotel*.
-   Erstellen Sie in Access (im Register Tabellen) die Verknüpfungen zu einigen Tabellen der Datenbank hotel auf dem MySQL-Server. Wählen Sie dabei als Datenquelle die von Ihnen erstellte DSN *hotel* aus.
-   Testen Sie den Zugriff auf jede der Tabellen.
-   Ändern Sie via ODBC einige Einträge in den Tabellen und prüfen Sie die Änderung, indem Sie mit phpMyAdmin die Einträge kontrollieren.
1.  Access-Eingabeformulare erstellen

Erstellen Sie im Register Formulare mit dem Assistenten einfache Formulare für das Erfassen, Ändern und Löschen der Daten in den MySQL-Tabellen.

-   Testen Sie die Formulare, indem Sie damit Daten eingeben, ändern und löschen.
-   Prüfen Sie zusätzlich mit phpMyAdmin diese Änderungen.
1.  Welcher Befehl testet die Verbindung zum Server-Rechner mit Adresse 139.79.124.97?

    - [ ] mysql -h 139.79.124.97

    - [ ] ipconfig

    - [ ] ping 139.79.124.97

    - [ ] mysqladmin -h 139.79.124.97 -u root -p ping

2.  Wozu wird der Parameter -h bei MySQL verwendet?

    - [ ] bewirkt die Abfrage des Passworts

    - [ ] bewirkt die Verbindung als bestimmter Benutzer

    - [ ] Angabe der Adresse des Client-Rechners

    - [ ] Angabe der Adresse des Server-Rechners

3.  Was bewirkt der Befehl 'mysqldump -h 139.79.124.97 hotel \> datei.txt'?

    - [ ] Backup der DB hotel in die Datei datei.txt auf Adresse 139.79.124.97

    - [ ] Backup der angegebenen DB auf dem Server mit der IP-Adresse 139.79.124.97

    - [ ] Restore der Datenbank hotel auf dem Server mit der Adresse 139.79.124.97

    - [ ] Ausführen des SQL-Skripts datei.txt auf Adresse 139.79.124.97 auf die DB hotel

4.  Welche Aufgabe hat der ODBC-Driver?

    - [ ] passt die SQL-Befehle dem entsprechenden DB-Server an

    - [ ] ermöglicht das Erstellen und Konfigurieren von ODBC-Datenquellen (DSN)

    - [ ] ermöglicht den einheitlichen Zugriff einer Applikation auf verschiedene Datenbanken

    - [ ] ermöglicht den Zugriff einer Applikation auf eine bestimmte DB

5.  Wie greifen Sie vom Konsolenfenster auf einen DB-Server mit Adresse 139.79.124.97 zu?

    - [ ] mysqladmin -h 139.79.124.97

    - [ ] mysql -h 139.79.124.97 hotel \< hotel.bkp

    - [ ] mysql -h 139.79.124.97 -u root -p

    - [ ] ping 139.79.124.97

1.  Welche Aufgaben hat der DB-Server im Gegensatz zum DB-Client?

    ___   
      

2.  Weshalb benutzt man MS Access z.B. zusammen mit einem MySQL-Server?

    ___   
      

3.  Wie bestimmen Sie die IP-Adresse des Server-Rechners?

    ___   
      

4.  Wie prüfen Sie, ob der DB-Server auf Adresse 139.79.124.97 läuft?

    ___   
      

5.  Welcher Befehl führt das SQL-Skript xy.sql auf die DB hotel auf Adresse 139.79.124.97 aus?

    ___   
      

6.  Wozu wird ODBC verwendet?

    ___   
      

7.  Was wird benötigt, um von Access mit ODBC z.B. auf einen DB2-Server zuzugreifen?

    ___   
      

8.  Was wird in der Java-Welt anstelle von ODBC verwendet?

    ___   
      

9.  Was sind eingebundene Access-Tabellen?

    ___   
      

# 8 Server-Administration

## 8.1 DB-Server konfigurieren

1.  Konfigurationsparameter anzeigen

    Führen Sie im DOS-Fenster den Befehl mysqld --help aus. Bestimmen Sie aus der Vielzahl der angezeigten Server-Parameter vier Ihnen wichtig erscheinende. Notieren Sie die gewählten Parameter mit einer kurzen Beschreibung.

2.  Sprache der Fehlermeldungen ändern

    Schauen Sie im Verzeichnis mysql\\share\\ nach, welche Sprachen unterstützt werden und tragen Sie mit einem Texteditor eine davon mit dem entsprechenden Parameter in der Konfigurationsdatei my.ini ein.

    Stoppen Sie den Server und starten Sie ihn erneut. Prüfen Sie im Monitor und im Control Center, dass Eingabefehler eine Fehlermeldung in der gewählten Sprache ergeben.

3.  Systemvariablen anzeigen

    Lassen Sie sich mit dem entsprechenden Befehl alle Systemvariablen, die etwas mit dem Logging zu tun haben anzeigen.

    Bestimmen Sie für jede Variable die Bedeutung. Benutzen Sie dazu das MySQL-Manual.

    show status; show variables; show variables like "%log%"

## 8.2 Logging

1.  Error Log benutzen

    Suchen Sie auf Ihrem Rechner das Fehlerprotokoll. Öffnen Sie es mit einem Texteditor und untersuchen und dokumentieren Sie die letzten 4 Einträge.

2.  Update Log konfigurieren

    Schalten Sie in der Konfigurationsdatei die Protokollierung in binärer Form ein (ohne Pfadangabe). Starten Sie den Server erneut, damit die Änderung wirksam wird. Ändern Sie dann in einer Tabelle der hotel-DB mit einem SQL-Befehl einen Datensatz und kontrollieren Sie den entsprechenden Eintrag in der Log-Datei. Verwenden Sie dazu das Programm mysqlbinlog.

3.  Query Log benutzen

    Schalten Sie in der Konfigurationsdatei diese Protokollierung ein.

    Führen Sie nach dem Neustart des Servers im Monitor einige Befehle aus und kontrollieren Sie die Einträge in der Log-Datei.

4.  Transaktions-Logdateien konfigurieren

    Aus Geschwindigkeitsgründen sollen die InnoDB-Logdateien vom Standardverzeichnis C:\\mysql\\data ins neue Verzeichnis D:\\log verlegt werden.

    Erstellen Sie das neue Verzeichnis und stoppen Sie den Server.

    Machen Sie den entsprechenden Konfigurationseintrag und löschen Sie die alten Logdateien.

    Starten Sie den Server und kontrollieren Sie den entsprechenden Start-Eintrag im Error-Log.

    Prüfen Sie, dass die Log-Dateien im neuen Verzeichnis erstellt wurden.

## 8.3 Backup und Restore

1.  Blockiertes Backup freigeben

    Blockieren Sie als DB-Client A im Monitor eine Tabelle der DB mybooks mit einem WRITE LOCK. Starten Sie als Client B in einem zweiten DOS-Fenster ein Backup der gleichen DB. Wie wird erreicht, dass das Backup ausgeführt wird?

2.  DB nach Crash wiederherstellen
-   Mit Hilfe des vorhandenen Backup und der Update-Log soll nach einem Totalverlust die DB wiederhergestellt werden.
-   Prüfen Sie, dass das Update-Log eingeschaltet ist; andernfalls schalten Sie es ein und starten Sie den Server erneut.
-   Erstellen Sie ein Backup der gesamten DB hotel und eröffnen Sie neue Log-Dateien mit dem SQL-Befehl FLUSH LOGS.
-   Tragen Sie in die DB je einen neuen Autor und Verlag ein.
-   Simulieren Sie den Totalverlust der DB, indem Sie den Server stoppen und das Verzeichnis der Datenbank, d.h. C:\\mysql\\data\\hotel löschen.
-   Starten Sie den Server und regenerieren Sie die DB mittels Backup und Update-Log. Bestimmen Sie dazu die Update-Logdatei, die seit dem Backup bis zum Crash entstanden ist. Nur diese ist für das Restore zu verwenden. Prüfen Sie vor dem Ausführen deren Inhalt.
-   Kontrollieren Sie, ob in der regenerierten DB die von Ihnen eingetragenen 2 Datensätze ebenfalls vorhanden sind.
1.  mysqldump –opt

    Untersuchen Sie mit *mysqldump --help* oder mit Hilfe des MySQL-Manuals, warum der Parameter *--opt* eine optimale Einstellung für Backups ergibt.

## 8.4 Repetitionsfragen

1.  Auf welche Arten können Konfigurationsparameter definiert werden?

    - [ ] mit einem INSERT-Befehl

    - [ ] durch Eintrag auf der Kommandozeile

    - [ ] durch Eintrag in einer Konfigurationsdatei

    - [ ] durch Eintrag in einem Logfile

2.  Welcher Befehl erstellt ein Backup der Datenbank kunden in die Datei kunden.sql?

    - [ ] \> mysql KUNDEN \< kunden.sql

    - [ ] \>mysqldump kunden \> kunden.sql -u username -p

    - [ ] SELECT \* FROM kunden ;

    - [ ] CREATE DATABASE kunden ;

3.  Welcher Konfigurationsparameter legt fest, wo die Log-Dateien abgelegt werden?

    - [ ] basedir

    - [ ] datadir

    - [ ] log-bin

    - [ ] logdir

4.  Mit welchem Eintrag beginnen die Server-Parameter in der Konfigurationsdatei?

    - [ ] [mysql]

    - [ ] [WinMySQLadmin]

    - [ ] [mysqldump]

    - [ ] [mysqld]

5.  Wozu kann der DB-Client mysqlshow verwendet werden?

    - [ ] Backup erstellen

    - [ ] DB-Schema anzeigen

    - [ ] Verbindung zum DB-Server testen

    - [ ] Inhalt einer Protokolldatei anschauen

6.  Mit welchem Log-File bestimmen Sie den letzten Start des MySQL-Servers?

    - [ ] Error Log

    - [ ] Update Log

    - [ ] Query Log

	 - [ ] Transaction Log
	 
1.  Welcher Eintrag im Konfigurationsfile schaltet die Protokollierung aller User-Login ein?

    - [ ] log-bin

    - [ ] log-slow-queries

    - [ ] log

    - [ ] log-error=C:/log/err.log

2.  Wie restaurieren Sie nach einem Server-Ausfall eine DB vollständig?

    - [ ] Einlesen des letzten Backup

    - [ ] Verwenden der Option --opt beim Erstellen des Backup

    - [ ] Einlesen des Query-Log

    - [ ] Einlesen aller Update-Logs in der richtigen Reihenfolge (mit Hilfe von mysqlbinlog)

3.  Wie erreichen Sie, dass Änderungen in der Konfigurationsdatei wirksam werden?

    ___   
      

4.  Durch welche Daten wird der von einer DB benötigte Speicherplatz bestimmt?

    ___   
      

5.  Wozu wird das Logging (Protokollierung) verwendet?

    ___   
      

6.  In welcher Log-Datei finden Sie, den Anwender, der bestimmte Daten löschte?

    ___   
      

7.  Welche Informationen finden Sie im Slow Query Log?

    ___   
      

8.  Geben Sie für jede Protokolldatei an, wie Sie deren Inhalt kontrollieren.

    ___   
      

9.  Wie beeinflusst der Parameter --opt beim Erstellen eines Backup das Tabellenlocking?

    ___   
      

10. Beschreiben Sie das Vorgehen, um Daten von MySQL nach ORACLE zu migrieren.

    ___   
      

11. Beschreiben Sie eine praktische Anwendung für den READ-Lock.

    ___   
      

# 9 Optimierung

1.  Welche Möglichkeiten können die Geschwindigkeit eines DB-Server verbessern?

    - [ ] Indexe möglichst vermeiden

    - [ ] Serverparameter einstellen

    - [ ] Transaktionen verwenden

    - [ ] Locks verwenden

2.  Wie werden Daten schneller in eine DB-Tabelle geladen?

    - [ ] durch Komprimieren der Daten vor der Übertragung

    - [ ] durch Verwenden des Parameters --opt beim Erstellen des Backup-Skripts

    - [ ] durch Importieren der Daten aus einer Textdatei

    - [ ] durch Verwenden von vielen INSERT-Befehlen

3.  Was trifft auf den Befehl OPTIMIZE TABLE zu?

    - [ ] entfernt nicht genutzten Speicherplatz aus MyISAM-Tabellendateien

    - [ ] ist auf MyISAM- und InnoDB-Tabellen anwendbar

    - [ ] wird angewendet bei Tabellen, die häufig abgefragt werden

    - [ ] defragmentiert DB-Dateien

4.  Wie finden Sie langsame DB-Abfragen?

    - [ ] mit EXPLAIN SELECT

    - [ ] im Query Log

    - [ ] im Slow Query Log

    - [ ] im Error Log

5.  Welche Aussagen betreffend DB-Optimierung sind korrekt?

    - [ ] Abfragen, die LIKE enthalten, können immer optimiert werden

    - [ ] Indexe beschleunigen Abfragen

    - [ ] Indexe werden allgemein auf Schlüsselattribute gelegt

    - [ ] durch Indexe werden DB-Einträge und -änderungen schneller
    
1.  Wann verwenden Sie den Befehl EXPLAIN?

    - [ ] um Daten schneller in die DB zu laden

    - [ ] immer im Zusammenhang mit SELECT

    - [ ] um langsame Abfragen zu finden

    - [ ] um zu erkennen, wie sich ein Index auf die Geschwindigkeit einer Abfrage auswirkt

1.  Welches sind Gründe für die Verwendung eines Index?

    - [ ] um das Eintragen von Daten in Tabellen bei Unique-Attributen zu beschleunigen

    - [ ] um DB-Abfragen zu beschleunigen

    - [ ] um das Ändern von Daten zu verlangsamen

    - [ ] um einmalige Werte zu gewährleisten
    
1.  Nennen Sie Ziele der DB-Optimierung?

    ___   
      

2.  Was wird optimiert, um die Geschwindigkeit eines DB-Servers zu verbessern?

    ___   
      

3.  Mit welchen 2 prinzipiellen Massnahmen werden DB-Abfragen beschleunigt?

    ___   
      

4.  Beschreiben Sie kurz, wie Sie den Befehl EXPLAIN verwenden.

    ___   
      

5.  Wozu wird der Befehl OPTIMIZE TABLE angewendet?

    ___   
      

6.  Wie werden SELECT-Befehle optimiert?

    ___   
      

7.  Wie viele DB-Tabellen können standardmässig gleichzeitig geöffnet sein?

    ___   
      

8.  Wie schalten Sie den Query Cache ein bzw. aus?

    ___   
      
