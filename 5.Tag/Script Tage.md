
## Logging

MySQL kann Änderungen in der Datenbank wahlweise im Textmodus oder binär protokollieren. Die resultierenden Logging-Dateien können beispielsweise für inkrementelle Backups oder zur Replikation mehrerer Datenbank-Server verwendet werden. Dazu später mehr.

Wenn Sie mit Transaktionen arbeiten, müssen Sie unbedingt binäres Logging verwenden, weil nur diese Variante Transaktionen korrekt behandelt. Wenn Sie das Logging dagegen im Textmodus durchführen, werden auch solche Kommandos protokolliert, die durch ROLLBACK widerrufen werden. Binäres Logging wird durch die Option `log-bin` in der Optionsdatei my.cnf aktiviert.

 
 
## Backup der Datenbank erstellen

Mit einem Backup wird Ihre Datenbank mit allen eingetragenen Daten und der Tabellenstruktur gesichert. Das Programm *mysqldump* erzeugt eine Liste aller SQL-Befehle, um die gesamte Datenbank exakt wiederherzustellen. Sie enthält die DDL-Befehle zur Tabellenerzeugung (CREATE…) und die DML-Befehle für die Datenerzeugung (INSERT…). Diese Informationen werden von mysqldump in eine beliebige Datei umgeleitet.

`C:\> mysqldump Hotel > pfad\hotel-backup.sql`

mysqldump wird nicht innerhalb des SQL-Monitors ausgeführt, sondern vom CMD-Fenster – es ist eine ganz gewöhnliche \*.exe-Datei, die mit Parametern aufgerufen werden kann.

> Wenn der Datenbankzugriff nur für bestimmte Benutzer freigegeben ist, müssen beim Aufruf des mysqldump-Befehls Benutzername und die Passworteingabe verlangt werden:

> `C:\> mysqldump -u name -p hotel > pfad\hotel-backup.sql`


Natürlich gibt es auch den Weg zurück: von einer Textdatei (zum Beispiel die gerade erzeugte) in eine Datenbank. Es geht zwar auch über phpMyAdmin, aber wir können jedes funktionierende SQL-Skript auch direkt von der Kommandozeile her in eine bestehende Datenbank einlesen:

> Die Datenbank "Hotel" muss dabei bereits angelegt sein: 

> `mysql\> CREATE DATABASE hotel;` <br>
`mysql\> USE DATABASE hotel;`


`mysql> SOURCE pfad\hotel-backup.sql;`

Damit werden **alle Tabellen der DB neu erstellt** und alle Daten eingetragen – oder kurz: das Skript wird ausgeführt.   


**Das Backup aktiver online-Datenbanken ist nicht ganz unproblematisch, denn Datensätze auf die zur gleichen Zeit zugegriffen wird, sind gesperrt und können nicht mit gesichert werden. Wir werden das in einem späteren Kapitel genauer untersuchen.**

---
---


---


# DB-Server im LAN

![](../x_res/Learn.png)

Die Informatik ist geprägt von der *Dezentralisierung*. Im Zusammenhang mit DB-Applikationen bedeutet dies Verteilung der Aufgaben und/oder der Datenbasis auf mehrere Rechner.

| Warum Dezentralisierung ? |                                                            |
|---------------------------|------------------------------------------------------------|
| Datenverfügbarkeit        | sicherer bei mehreren Servern, z.B. falls eine DB ausfällt |
| Datensicherheit           | Zugriffssteuerung einfacher bei kleineren Datenbeständen   |
| Flexibilität              | einfachere Änderung von Teilsystemen                       |
| Performance               | besser bei mehreren Rechnern                               |
| Kosten                    | tiefer als bei teuren Grosscomputern                       |

Tab. 7.1: Gründe für die Dezentralisierung

### Zugriff über das Netz

![](../x_res/DB-Client.png)

Abb. 21: Beispiel-Konfiguration mit einem DB-Client und einem DB-Server

Der Zugriff vom Client auf den DB-Server erfolgt wie bisher, es ist nur jeweils zusätzlich die *Netzwerkadresse (IP-Adresse)* bzw. der *Rechnername (Hostname)* des Servers anzugeben. Befindet sich zwischen DB-Client und DB-Server eine Firewall, so muss *Port 3306* geöffnet werden, andernfalls ist eine Kommunikation nicht möglich.

**a) Verbindung zum DB-Servertesten**:

Der folgende Befehl prüft, ob der MySQL-Server auf dem angegebenen Rechner läuft. Mit dem Parameter *-h* ist der Server-Rechner anzugeben.

```SQL
>mysqladmin -h 172.20.0.18 ping

mysqld is alive
```

**b) Über das Netz auf die DB zugreifen**:

Auch der Monitor wird mit dem Parameter *-h* gestartet.

`>mysql -h 172.20.0.18`

Zugriff mit Benutzername und Passwort:

```SQL
>mysql -h 172.20.0.18 -u remote -p

Enter password: ******
```

Bei erstellter Verbindung, sind die Befehle gleich wie im lokalen Betrieb.

**c) Backup und Restore über das Netz ausführen**:

Um ein Backup einer DB auf einem anderen Rechner zu erstellen, ist die Rechner-Adresse anzugeben.

```SQL
C:> mysqldump -h 172.20.0.18 -u remote -p --opt morebooks > H:\backup.sql

******
```

![note](../x_res/note.png) Wegen dem Parameter `-p` erscheint die Passworteingabe-Aufforderung nicht auf Bildschirm sondern am Anfang des SQL-Skripts. Das Passwort ist ohne Aufforderung einzugeben.

```SQL
Enter password: -- MySQL dump 8.21

-- Host: 172.20.0.18 Database: morebooks

---------------------------------------------------------

-- Server version 4.0.12-nt
```

Auf der 1. Zeile des Backup-Skripts ist die Eingabeaufforderung mit dem Texteditor zu löschen. Beim Restore ist die Adresse des Zielrechners anzugeben:

```SQL
C:> mysql -h 172.20.0.18 -u remote -p morebooks \< H:\\backup.sql

Enter password: \*****
```

**d) Netzwerkzugriff auf den DB-Server zeitweise verbieten**:

Mit dem Eintrag `skip-networking` in der Konfigurationsdatei `C:\...\my.ini` des Servers ist ein Zugriff via TCP/IP (auch lokal) nicht mehr möglich.

```
[mysqld]

skip-networking
```

Ein Verbindungsversuch ergibt folgende Fehlermeldung:

`ERROR 2003: Can't connect to MySQL server on '172.20.0.18' (10061)
`

### Das ODBC-Interface

ODBC (*Open Database Connectivity, middleware*) ist vor allem in der Windows-Welt ein oft verwendeter Mechanismus für den einheitlichen Zugriff auf verschiedene DB-Systeme. Obwohl schon ziemlich alt, wird ODBC von den meisten DB-Systemen unterstützt und wird noch für einige Zeit der wichtigste herstellerunabhängige Standard zur Kommunikation zwischen DB-Systemen sein. ODBC steht auch unter Unix zur Verfügung, wird dort aber seltener eingesetzt.

![](../x_res/ODBC.png)

Abb. 22: Einheitlicher Zugriff einer Applikation via ODBC auf verschiedene Datenbanken

Die meisten DB-Hersteller bieten *ODBC-Treiber* für ihre DB-Server an. Vor Verwendung von ODBC muss der für die entsprechende Datenquelle benötigte Treiber auf dem Client installiert werden. MariaDB unterstützt ODBC mit Hilfe von *mariaODBC (Connector/ODBC)*. MariaODBC ist ein 32/**64**-Bit-ODBC-Treiber für die Anbindung von ODBC-fähigen Applikationen an MariaDB.

(MariaDB.com \>\> Download \>\> Connectors)

**a) Installierte ODBC-Treiber kontrollieren**:

Um zu kontrollieren, welche ODBC-Treiber und welche Versionen auf dem Rechner installiert sind, wird über Start \> Einstellungen \> Systemsteuerung \> Verwaltung \> **ODBC-Datenquellen (64Bit)** der *ODBC-Administrator* gestartet. (Verion 10.xx oder höher)

![ODBC-Treiber](../x_res/ODBC-Treiber.png)

Abb. 23: Installierte ODBC-Treiber ermitteln (64Bit)

**b) Eine neue Datenquelle (DSN, Data Source Name) erstellen und konfigurieren**:

Das Einrichten von ODBC auf einem DB-Client umfasst a) die Installation des ODBC-Treibers und b) die Registrierung der Datenbank im ODBC Driver-Manager. Das Einrichten eines *DSN* erfolgt im ODBC-Administrator in einem von 3 Registern:

-   *Benutzer-DSN*: steht nur dem Benutzer zur Verfügung, der den DSN definiert
-   *System-DSN*: steht allen Benutzern zur Verfügung
-   *Datei-DSN*: für Dateien, auf die via ODBC zugegriffen wird (z.B. Excel-Tabelle)

Der DB-Zugriff erfolgt via DSN. Mit dem Knopf *Hinzufügen* wird ein neuer DSN erstellt.

![morebooks 1](../x_res/morebooks0.png)

![](../x_res/morebooks1.png)

![morebooks 2](../x_res/morebooks2.png)

Abb. 24, 25, 26: DSN für die MySQL-Datenbank morebooks konfigurieren

Mit *Data Source Name* wird angegeben, unter welchem Namen die Datenquelle in Zukunft angesprochen wird (z.B. DB- oder Projektname). "Test DSN" überprüft die Verbindung! Next drücken.

*Host/Server Name* ist der Name oder die IP-Adresse des Server-Rechners (localhost für den lokalen Rechner). In *Database Name* wird der Name der DB angegeben. Für *User* und *Password* gilt das Zugriffssystem des Servers (s. Kap. 6). Werden die beiden Felder leer gelassen, so sind die Angaben zu Beginn der 1. ODBC-Verbindung einzugeben. Restliche Einstellungen durchklicken.

![note](../x_res/note.png) Soll auf mehrere Datenbanken zugegriffen werden, muss für jede DB 1 DSN definiert werden.

**c) Konfigurierte DSN testen**:

Mit dem evtl. vorhandenen Knopf *Test Data Source* kann getestet werden, ob die Verbindung zur Datenbank funktioniert. Nicht alle ODBC-Driver unterstützen einen solchen Test.

**d) MyODBC-Optionen einstellen**:

Der Knopf *Next* führt zum weiteren Dialog mit mehreren Einstellungen.

### Zugriff von Access auf eine MySQL-DB

Access und MySQL sind zwei grundverschiedene Programme. Access hat eine ausgereifte Benutzeroberfläche, wird aber langsam, wenn mehrere Benutzer gleichzeitig zugreifen. MySQL ist im Multi-User-Betrieb effizienter und sicherer, hat aber nicht die komfortable Bedienoberfläche von Access. Es liegt nahe, die Vorteile aus beiden Welten zu kombinieren, z.B.

-   Access als Benutzeroberfläche (GUI) für Zugriff und Veränderung der Daten in einer MySQL-DB
-   Access als Reportgenerator für Daten aus MySQL
-   Daten aus Access- und MySQL-Datenbanken kombinieren
-   Das Schema einer neuen DB zuerst in Access entwickeln und dann nach MySQL exportieren
-   In Access ER-Diagramme von MySQL-Datenbanken erstellen

![note](../x_res/note.png) Oft ergeben sich Probleme mit inkompatiblen Datentypen. In MySQL-Tabellen sollte DOUBLE statt FLOAT, DATETIME statt DATE verwendet werden. Die Datentypen BIGINT, SET und ENUM sind zu vermeiden, da Access diese nicht kennt.

**a) Tabellen einer externen DB in Access einbinden**:

Um von Access aus auf Daten einer anderen DB zuzugreifen, können deren Tabellen in Access eingebunden werden mit

-   Externe Daten \> Neue Datenquellen \> Aus anderen Quellen \> ODBC\_Datenbank ...
-   "Erstellen Sie eine Verknüpfung zur Datenquelle" wählen
-   Im "Datenquelle auswählen"-Dialog im Register *Computerdatenquelle* eine Datenquelle wählen

    ![](../x_res/Auswahl1.png) 
    
    ![](../x_res/Auswahl2.png)

    Abb. 27 & 28: Auswahl der SQL-Datenquelle

**b) ODBC-Zugriff auf verknüpfte Tabellen testen**:

Eingebundene Tabellen werden wie lokale Tabellen verwendet. Die Daten können editiert und erweitert werden.

![note](../x_res/note.png) Die Struktur eingebundener Tabellen lässt sich via ODBC nicht ändern. Das Löschen einer eingebundenen Tabelle löscht lediglich die Verbindung, die Tabelle selber bleibt bestehen.

**c) Abfrage mit verknüpften MySQL-Tabellen erstellen**:

Wie mit lokalen Tabellen lassen sich mit verknüpften Tabellen Abfragen und Formulare erstellen.

**d) Access-Formulare erstellen**:

Normalerweise arbeitet der Benutzer nicht direkt mit DB-Tabellen und SQL-Befehlen sondern verwendet entsprechende *Formulare*, *Masken* und *Menüs*.

---
---


# Server-Administration

![](../x_res/Learn.png)

## Server-Konfiguration

Nach der Standardinstallation funktioniert der DB-Server mysqld in den meisten Fällen problemlos. Für besondere Wünsche (z.B. andere Sprache oder Zeichensatz, bessere Performance) können beim Start des Servers (mysqld) sowie für das Ausführen von Admin-Programmen (z.B. mysql) Parameter (Optionen) angegeben werden.

Konfigurationsparameter werden auf der Kommandozeile oder in Konfigurationsdateien festgelegt.

**a) Parameter auf der Kommandozeile angeben**:

Der folgende CMD-Befehl startet den DB-Server mit der Option für Fehlermeldungen in Deutsch.

`C:> mysqld --language=german`

Der folgende Befehl startet den Monitor mit Benutzernamen und weiteren Parametern.

`C:> mysql --user=kunde --i-am-a-dummy --silent`

**b) Parameter in die Konfigurationsdateien eintragen (my.ini, my.cnf)**:

Da der DB-Server meistens als Dienst gestartet wird, ist es nicht möglich, direkt Parameter anzugeben. Darum werden Server-Parameter fast immer in einer Konfigurationsdatei angegeben. Lese-Reihenfolge siehe Seite 18!

`C:\...\mysql\data\my.ini C:\...\my.cnf`

Es gilt der zuletzt gelesene Wert. Werte auf der Kommandozeile haben Vorrang vor Einträgen in den Konfigurationsdateien. Es gelten die gleichen Angaben wie auf der Eingabezeile, aber ohne `--`.

```
# Server-spezifische Eintraege

[mysqld]

language=german
```

Statt beim Ausführen von Administrationsprogrammen immer wieder die gleichen Optionen anzugeben, können sie ebenfalls in eine der Konfigurationsdateien eingetragen werden.

```
# Monitor-spezifische Eintraege

[mysql]

user=meier

silent
```

### MySQL-Konfigurationsparameter

Die Konfigurations-Optionen werden mit einem Texteditor oder mit der Workbench in die Konfigurationsdatei eingetragen. Angezeigt werden die Serverparameter mit dem Befehl

`mysqld --help`

Änderungen in den Konfigurationsdateien werden erst nach einem Neustart des betroffenen Programms wirksam. Für Server-spezifische Optionen muss der Server neu gestartet werden. **Auch unter Windows sind in den Konfigurationsdateien Pfade wie bei UNIX mit / anzugeben (statt mit \\).
**
![caution](../x_res/caution.png) Bei einem falschen Eintrag in einer Konfigurationsdatei (z.B. Tippfehler, - statt \_) kann der Server nicht gestartet werden.

### Systemvariablen

Diverse Statusinformationen des DB-Servers sind als Systemvariablen zugänglich. Angezeigt werden diese mit dem Befehl SHOW VARIABLES, z.B. eingestellter und vorhandene Zeichensätze:

`mysql> SHOW VARIABLES LIKE '%log%';`

## Logging

Logging bezeichnet die Protokollierung von Änderungen in der Datenbank.

| Wozu Logging ? |                                                                                  |
|----------------|----------------------------------------------------------------------------------|
| **Monitoring**     | Protokoll von Start, Shutdown und Fehlern.  Wer hat wann welche Daten verändert? |
| **Sicherheit**     | Datenwiederherstellung zwischen regulären Backups                                |
| **Optimierung**    | Aufzeichnung von aufwendigen DB-Abfragen                                         |
| **Replikation**    | Logging des Master-Rechners für die DB-Synchronisierung                          |
| **Transaktionen**  | Durchführen von Transaktionen nach einem DB-Absturz                              |

Tab. 8.1: Ziele des Logging

Nachteile des Logging sind **Verlangsamung** des DB-Servers und zusätzlicher **Platzverbrauch** auf der Festplatte. Deshalb sind in der Standardeinstellung ausser dem Error-Log alle Protokolle ausgeschaltet.

![](../x_res/Log-Dateien.png) 

Abb. 29: Log-Dateien mit Konfigurationsparametern und Verarbeitungs-Tools

![note](../x_res/note.png) Änderungen der Logging-Parameter werden erst nach dem Neustart des Servers wirksam.

Für maximale Geschwindigkeit und Sicherheit sollten die Log-Dateien auf einer anderen Festplatte gespeichert werden als die DB-Dateien.

### Binärdateien lesen mit mysqlbinlog.exe

Die meisten Logging-Protokolle werden in Binärdateien gespeichert. Sie können diese zwar mit einem normalen Editor öffnen, der Inhalt ist aber unverständlich, da binär codiert. Im MySQL-Binärverzeichnis gibt es ein Programm zur Anzeige der Binärlogdateien: mysqlbinlog.exe. Starten Sie dieses Programm und geben die Datei als Parameter mit und der Dateiinhalt wird Ihnen auf dem Konsolenfenster ausgegeben.

### Error Log (\<host\>.err)

Das Fehlerprotokoll protokolliert jeden *Start* und *Shutdown* des MySQL-Servers sowie alle *Fehlermeldungen* des Servers in Textform.

**a) Error Log konfigurieren**:

```
# Error Log-Datei festlegen (Default: <basedir>\data\<host>.err)

log-error=C:/log/error.txt
```

**b) Error Log kontrollieren**:

Jeder Eintrag ist mit Datum yymmdd und Zeit hh:mm:ss versehen.

`C:\>TYPE C:\mysql\data\al27785.err`

![note](../x_res/note.png) Das Error Log kann nicht ausgeschaltet werden.

### Binäres update Logging

Der Parameter \#log-bin=mysql-bin ist in der Konfigurationsdatei von MySQL standardmässig auskommentiert. Wenn Sie das Kommentarzeichen entfernen, können Sie den Dateinamen anpassen und ab dem nächsten Serverstart wird eine Datei mit der Endung \*.000001 angelegt, die alle Anweisungen enthält, die Daten verändern. Normale Select-Anweisungen werden also nicht abgespeichert.

Bei folgenden Bedingungen wird eine neue Logdatei erstellt und die Extension um eins hochgezählt:

-   beim Neustart des DB-Servers
-   beim Erreichen der Maximalgrösse, die mit dem Parameter max\_binlog\_size definiert wird
-   durch den Befehl FLUSH LOGS

Zusätzlich wird eine Indexdatei mit der Endung .index erzeugt, die alle Dateinamen der bisherigen Binärlogdateien aufzeichnet.

Seit der MySQL-Version 5.0 ersetzt das Binärlogging das vorherige Update-Logging.

Gemäss MySQL-Manual 5.1 ist die Leistungseinbusse des Datenbankservers durch das aktivierte Binär-Logging nur 1%. Für einige Anwendungen ist es ohnehin essentiell nötig: **Transaktionen**, **Replikationen**, **Recovery** nach Systemabstürzen …

### Binärlog-Datei anzeigen:

Die Einträge in der Log-Datei sind binär dargestellt. Das heisst, dass wir mit einem gewöhnlichen Text-Editor keine lesbare Darstellung bekommen. Um den Inhalt der Datei anzuzeigen benötigen Sie den speziellen Client mysqlbinlog, der sich im mysql/bin-Verzeichnis befindet.

`C:\...\mysql\bin>mysqlbinlog -u root -p C:\...\MySQL\data\mysql-bin.000001`

### General Query Log (\<host\>.log)

Das Login- und Operationssprotokoll zeichnet auf, welcher Benutzer welche Daten liest oder ändert. Dazu wird *jeder Verbindungsaufbau und jeder Befehl* an den DB-Server aufgezeichnet. Dieses Protokoll kann nicht für die Wiederherstellung von Daten verwendet werden und wird sehr schnell sehr gross!

`mysql\> SET GLOBAL general_log=1;`

### Slow Query Log (\<host\>-slow.log)

Dieses Protokoll zeigt Datenbankabfragen, die den grössten Aufwand verursachen. Die Analyse dieser Abfragen kann Hinweise für die Verbesserung der *Server-Performance* geben.

### Transaktions-Log (ib\_logfile1, -2, ...)

In den InnoDB-Logging-Dateien wird jede Änderung in *InnoDB-Tabellen* aufgezeichnet. (s. Kap. 5*)*. Dies ermöglicht grosse Transaktionen sowie die Wiederherstellung nach einem Server-Absturz.

Mit `COMMIT` wird eine Transaktion vorerst nur ins Transaktions-Log geschrieben. Die geänderten Tablespace-Seiten werden aus Geschwindigkeitsgründen erst nach und nach auf die Festplatte übertragen. Kommt es in der Zwischenzeit zu einem Absturz, wird der Tablespace mit Hilfe der Log-Dateien wiederhergestellt.  

Hinweis: `maria_log` (mariaDB-Engine für Transaktionen!)

**Transaktions-Log konfigurieren**:

Die Log-Dateien werden der Reihe nach befüllt. Wenn die letzte Datei voll ist, beginnt der *InnoDB-Tabellentreiber*, wieder Daten in die erste Log-Datei zu schreiben.

![note](../x_res/note.png) Transaktions Log-Dateien können nicht mit dem Texteditor kontrolliert werden. Sie sind nur im Server-Betrieb erforderlich und werden nach dem korrekten Server-Shutdown nicht mehr benötigt.

## Backup und Restore

Während des Betriebs eines DB-Servers wird regelmässig ein Backup erstellt. Durch einen Crash können sämtliche Daten verloren gehen.

![](../x_res/Backup.png)

Abb. 30: Backup, Logging und Restore

**Restore** (Recovery): Zuerst wird das letzte Backup eingelesen, anschliessend der Reihe nach alle seitdem erstellten Update Log-Dateien. Damit lässt sich der Zustand vor dem Crash herstellen.

| Backup- und Restore-Befehle                    |         |                         |
|------------------------------------------------|---------|-------------------------|
| `mysqldump [optionen] db [tb1 tb2 ...] > datei` | Backup  | allgemeine Syntax       |
| `mysqldump --help`                               | "       | Anzeigen aller Optionen |
| `mysqldump hotel > backup.sql`                  | "       | ganze Datenbank         |
| `mysqldump hotel person buchung > bkp.sql `     | "       | 2 Tabellen              |
| `mysqldump --opt ...`                            | "       | optimale Einstellung    |
| `mysql [optionen] db < datei`                   | Restore | allgemeine Syntax       |

Tab. 8.2: Beispiele von Befehlen für Backup und Restore

**Backup mit mysqldump erstellen**:

Das Programm erzeugt pro Tabelle einen CREATE TABLE-Befehl um die Tabelle samt Indizes zu erzeugen, sowie die INSERT-Befehle, um die Daten einzutragen. Mit mysqldump können einzelne Tabellen oder ganze DBs gesichert werden.

Damit bei der Ausgabeumleitung in eine Datei die Enter password-Zeile nicht in die Zieldatei umgeleitet wird, kann das Passwort mit --password=x direkt eingegeben werden.

Die Option `--opt` fasst mehrere Optionen zusammen und bewirkt eine *optimale Einstellung*.

`C:\>mysqldump --password=pwd --opt hotel > backup.sql`

**Integrität des Backup mit READ Lock gewährleisten**:

Während des Backups dürfen die Daten nicht durch einen anderen Client verändert werden. Ein READ LOCK für die betroffenen Tabellen wird mit dem Parameter `--lock-tables` von mysqldump erreicht.

`C:\>mysqldump --lock-tables hotel > backup.sql`

![note](../x_res/note.png) Ist auch nur eine Tabelle der DB von einem anderen Client mit einem `WRITE LOCK` blockiert, so wird das Backup erst nach der Freigabe (`UNLOCK TABLES`) gestartet.

**Datenbank wiederherstellen (Restore)**:

Dafür wird der Monitor verwendet.

`C:\>mysql hotel < backup.sql`

**Datenbank aus einem Update-Log wiederherstellen**:

Die Rekonstruktion der DB beginnt mit dem Einlesen des letzten vollständigen Backup. Anschliessend werden die Update Log-Dateien benötigt, die seit dem letzten Backup entstanden sind. Die darin enthaltenen SQL-Befehle werden mit Hilfe von `mysqlbinlog` gelesen und mit **\|** (=pipe) an mysql übergeben.

```
C:\>mysqlbinlog al27785-bin.004 | mysql

C:\>mysqlbinlog al27785-bin.005 | mysql
```

![note](../x_res/note.png) Die älteste Log-Datei wird zuerst ausgeführt.

## Import und Export

Import und Export bezeichnen die Übertragung von Daten zwischen Textdatei und DB-Tabelle.

![](../x_res/Export.png) 

Abb. 31: Export und Import für die Übertragung von Daten

Damit die Datenstruktur bei der Umwandlung in die DB-Tabelle richtig interpretiert wird, müssen die einzelnen Felder und Zeilen markiert werden. Die **Trennzeichen (Delimiter)** sind so zu wählen, dass keine Verwechslung mit dem Textinhalt möglich ist.

| Import- und Export-Befehle u. Parameter |                                                       |
|-----------------------------------------|-------------------------------------------------------|
| `SELECT .. INTO OUTFILE`                  | exportiert das Abfrage-Ergebnis in eine Text-Datei    |
| `TRUNCATE TABLE tb;`                      | löscht den gesamten Tabelleninhalt der Tabelle        |
| `LOAD DATA INFILE `                       | importiert Inhalt einer Textdatei in eine DB-Tabelle  |
| `FIELDS TERMINATED BY ';'`                | z.B. ;, trennt einzelne Felder (Feldtrennzeichen)     |
| `ENCLOSED BY '"' `                        | z.B. ", umschliesst einzelne Textfelder               |
| `LINES TERMINATED BY '\\r\\n' `           | Zeilentrennzeichen für Windows-Textdateien            |

Tab. 8.3: Beispiele von Import- und Export-Befehlen und Definition von Trennzeichen (Delimiter)

![note](../x_res/note.png) Textimport ist oft problematisch, z.B. wegen Datum-Formatierungen. Warnings \> 0 zeigen *Importfehler*, die dann evt. mit Hilfe des `SELECT`-Befehls gefunden werden. Es ist darauf zu achten, dass die importierten Daten die referenzielle Integrität der Datenbank erfüllen.

**Export mit SELECT ... INTO OUTFILE**:

Es handelt sich um einen gewöhnlichen `SELECT`-Befehl mit Ausgabe in eine Textdatei. Die Trennzeichen werden mit den *Exportoptionen* vor dem `FROM`-Teil angegeben.

Der `SELECT`-Befehl kann zusätzlich beliebige Auswahl- und Sortieranweisungen enthalten.

## Speicherbedarf abschätzen

Bei der Planung eines DB-Servers ist der voraussichtlich benötigte Speicherplatz zu berücksichtigen. Neben den eigentlichen Benutzerdaten (Tabellendaten) wird auch Speicherplatz für Indexe (Indextabellen) und die Systemverwaltung (DB- und Tabellenbeschreibungen, Systemkatalog, User- und Zugriffsverwaltung) benötigt.

Das Abschätzen des benötigten Speicherplatzes ist ein ungenaues Unterfangen wegen Tabellenspalten variabler Länge (`VARCHAR`), nicht bekannter Anzahl und Grösse der Log-Dateien, temporären Bereichen für bestimmte SQL-Befehle (z.B. `ORDER BY`), Datenträgerfragmentierung etc.

*Speicherbedarf pro Tabelle (MyISAM)*

| Nutzdaten (\*.MYD)   | Anzahl\_Datensätze \* [SUM (Bytes\_pro\_Attribut1..n) + 5] |
|----------------------|---------------------------------------------------------|
| Indizes (\*.MYI)     | Anzahl\_Schlüssel \* [(Schlüssellänge + 4) / 0.67]       |
| Systemdaten (\*.FRM) | 8500 + (Anzahl\_Attribute \* 40)                         |

Tab. 8.1: Formeln für die ungefähre Berechnung des Speicherverbrauchs pro Tabelle

`Bytes_pro_Attribut` und Schlüssellänge werden dem MySQL-Manual entnommen. Die Indextabellen werden in Blöcken (d.h. Mehrfachen) von 1024 Byte abgespeichert. Für `Anzahl_Schlüssel` kann die Anzahl Datensätze eingesetzt werden.

Die Belegung des variablen Datentyps `VARCHAR` ergibt sich als Mittelwert einer Anzahl Test-Einträge.

[Anzeigen](https://404media.de/sql/wie-du-die-groesse-einer-datenbank-und-tabelle-in-mysql-sql-ermittelst)

---
---

# Optimierung

![](../x_res/Learn.png)

Die Optimierung des Betriebs einer Datenbank ist eine komplizierte Aufgabe, weil sie ein umfassendes Verständnis des gesamten DB-Systems voraussetzt.

| Wozu Optimierung ?       |                                               |
|--------------------------|-----------------------------------------------|
| **Performance** verbessern   | schnellere Ausführung von SQL-Befehlen        |
| **Speicherplatz** einsparen  | schnellere Übertragung von/auf die Festplatte |
| **Portabilität** ermöglichen | Übertragen der DB auf einen anderen Server    |

Tab. 9.1: Ziele der Optimierung

Das wichtigste, um ein System schnell zu machen, ist das grundlegende Design. Ausserdem ist es wichtig zu wissen, was das System macht und welches mögliche *Flaschenhälse* sind. Engpässe sind z.B.:

-   *Suchvorgänge auf der Festplatte*: Die Festplatte benötigt Zeit, um die angeforderten Daten zu finden. Pro Sekunde können etwa 1000 Suchvorgänge durchgeführt werden. Eine Möglichkeit der Optimierung besteht darin, Daten auf mehrere Platten zu verteilen.

-   *Lesen von / Schreiben auf Festplatte*: In der richtigen Position kann die Festplatte etwa 10 bis 20 MB pro Sekunde übertragen. Dies ist leichter zu optimieren als Suchvorgänge, da von mehreren Festplatten parallel gelesen werden kann.

| Was wird optimiert? |                                                    |
|---------------------|----------------------------------------------------|
| **Datenbankstruktur**   | minimaler Speicherplatz, Index-Verwendung          |
| DB-**Abfragen**         | Abfragen mit EXPLAIN analysieren                   |
| **Locks**               | Geschwindigkeit erhöhen durch Sperren von Tabellen |
| DB-**Server**           | Serverparameter einstellen                         |

Tab. 9.2: Objekte der Optimierung

## Laden von Daten, Tabellenspeicherplatz

### Laden von Daten optimieren

1.  **SQL-Skripts optimieren mit --opt**:

Diese `mysqldump`-Option ergibt ein Skript für das schnellstmögliche Einlesen in einen MySQL-Server (s.a. Kap. 8.3, Seite 46). Der folgende Befehl erstellt ein SQL-Skript für eine einzelne Tabelle der DB mybooks.

`C:\> mysqldump --opt hotel person > backup.sql`

Der Parameter `--opt` umfasst die Optionen

| Parameter           | Kommentar |
|-----------------|--------------------------------------------------------------------------|
| `quick`           | beschleunigt das Erstellen durch Vermeiden des Zwischenspeicherns im RAM |
| `add-drop-table`  | fügt vor jedes CREATE TABLE einen DROP TABLE-Befehl ein                  |
| `add-locks`       | fügt für schnelles Einlesen LOCK und UNLOCK TABLE-Befehle ein            |
| `extended-insert` | erzeugt für schnelles Einlesen wenige INSERT mit mehreren Datensätzen    |
| `lock-tables`     | führt ein LOCK TABLE READ aus (Datenintegrität)                          |

In den meisten Fällen ist dies eine optimale Einstellung.

**a) Schnelleres Laden von Daten mit LOAD DATA INFILE**:

Viel schneller als das Laden von Daten mit vielen INSERT-Befehlen ist das Importieren der Daten aus einer Textdatei (s.a. Kap. 8.4, Seite 48), z.B.

```
mysql> LOAD DATA INFILE 'c:/person.txt' INTO TABLE person
FIELDS TERMINATED BY ';' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n';
```

### Tabellenspeicherplatz optimieren (nur MyISAM)

**b) OPTIMIZE TABLE verwenden**:

Werden aus einer Tabelle Datensätze gelöscht oder Änderungen durchgeführt, so ist die Datenbankdatei grösser als notwendig und die Daten sind über die ganze Datei verstreut, was den DB-Zugriff verlangsamt.

`OPTIMIZE TABLE` entfernt nicht genutzten Speicherplatz aus einer Tabellendatei und sorgt dafür, dass zusammengehörende Daten eines Datensatzes auch zusammen gespeichert werden.

`mysql> OPTIMIZE TABLE person;`

Bei Tabellen, deren Inhalt sich häufig ändert (viele DELETE- und UPDATE-Befehle), sollte der Befehl regelmässig ausgeführt werden. Damit wird die Datei verkleinert und der Zugriff beschleunigt.

## Einsatz von Indextabellen

Eine Suche ist grundsätzlich nur dann effizient, wenn sie in einem sortierten Suchraum arbeitet. Das beste Beispiel ist das Telefonbuch. Wären die Personen nicht alphabetisch sortiert, bliebe uns bei der Suche nach einer Telefonnummer nicht anderes übrig, als das Telefonbusch sequentiell durchzulesen, bis wir auf die gesuchte Person stossen – im schlimmsten Fall also bis zum Ende.

Um in grossen unsortierten Listen einen gewünschten Wert zu finden müssen wir also die lineare oder sequentielle Suche anwenden. Auch wenn wir nach einem Wert suchen, der gar nicht vorhanden ist, können wir das erst am Ende des Suchraumes feststellen. Aus diesem Grund ist es einfacher, wenn wir Listen – oder konkret in Datenbanken dann Attribute – sortieren, um schnellere Verfahren anwenden zu können.

Bei der binären Suche wird eine sortierte Liste vorausgesetzt. Damit halbiert sich bei jedem Zugriff der Suchraum. Wir benötigen bei N Elementen 2log N Zugriffe. Wenn unser Suchraum aus 16 (=24)Werten besteht, benötigen wir also maximal 4 Zugriffe (vorherige „Glückstreffer“ ausgenommen). Mit nur einem Zugriff mehr, können wir bis zu 32 Werte abdecken. Allerdings benötigen wir auch für 17 Werte maximal 5 Zugriffe. Für dieses Suchverfahren wird auch noch der Begriff „logarithmische Suche“ verwendet.

Eine Erweiterung der binären Suche ist die Interpolationssuche. Bei der binären Suche wird der Suchraum jedes Mal genau in der Mitte geteilt. Die Interpolationssuche berücksichtigt noch die Grösse des gesuchten Wertes in Bezug auf den Suchraum. Es vergleicht also den grössten und den kleinsten Wert, geht von einer gleichmässigen Verteilung der Daten aus und teilt den Suchraum jetzt im Verhältnis zu dem Suchwert. Konkret greift es bei einem sehr hohen Wert bereits im „wahrscheinlicheren“ hinteren Bereich des Suchraumes zu. Jeder erfolgreiche Zugriff schränkt den Suchraum also um mehr als die Hälfte ein. Mit der zusätzlichen Berechnung des Zugriffselementes geht aber ebenfalls Rechenzeit verloren. Daher ist die Interpolationssuche nur bei grossen Suchräumen effizienter.

Ein leicht verständliches Zahlenbeispiel finden Sie bei Wikipedia: Interpolationssuche.

Wenn wir aber Tabellen mit mehreren Attributen haben, auf die wir häufig zugreifen, können wir ja trotzdem nur nach einem Attribut sortieren. Die Lösung ist die Verwendung von Indextabellen, die wir pro Attribut definieren können. Für jedes indizierte Attribut wird eine zusätzliche „Hilfstabelle“ angelegt, die nur das – jetzt sortierte Element – und einen Schlüssel auf die Originaltabelle enthält. Diese Indextabellen werden vom Datenbankmanagementsystem verwaltet. Zugriffe und Updates liegen also weder beim Anwender noch beim Datenbankentwickler.

Dadurch wird die Datenbank etwas grösser (auch die Indextabellen müssen gespeichert werden). Die Einfügeoperationen (INSERT) sind etwas aufwendiger, da auch die Indextabellen aktualisiert werden müssen. Aus dem gleichen Grund werden Aktualisierungen, die indizierte Attribute betreffen aufwendiger. Der Zeitvorteil liegt aber bei den Abfragen. Da aber in der Praxis die Antwortzeit bei Abfragen viel kritischer ist als die Antwortzeit von Einfügeoperationen, sind die Nachteile aktzeptierbar.

## Abfrage- und Indexoptimierung

Die Performance einer Datenbank lässt sich erst dann abschätzen, wenn die Datenbank genügend Testdaten enthält. Eine Datenbank mit einigen Hundert Datensätzen befindet sich nach den ersten 3 Abfragen meistens vollständig im RAM, so dass alle Abfragen mit oder ohne Index sehr schnell beantwortet werden. Eine Abfrage-Optimierung lohnt sich nur dann, wenn die Tabellen über 10000 Datensätze enthalten oder die Gesamtgrösse der DB die RAM-Grösse auf dem DB-Server überschreitet.

### Langsame Abfragen in der Slow Query Log (host-slow.log)

**Slow Query Log kontrollieren (host-slow.log)**:

Dieses Protokoll zeichnet alle langsamen Abfragen samt Ausführungszeit in Textform auf.

`C:\>TYPE C:\...\mysql\data\al27785-slow.log`

### EXPLAIN SELECT verwenden (Ausführungsplan)

Wird einem SELECT-Statement das Schlüsselwort `EXPLAIN` vorangestellt, so erklärt MySQL, wie das `SELECT` ausgeführt würde. Die angegebenen Informationen zeigen, wie und in welcher Reihenfolge die Tabellen verknüpft werden (*Ausführungsplan*). Mit Hilfe von `EXPLAIN` lässt sich erkennen, wo Indexe hinzugefügt werden müssen.

Die zu untersuchende Abfrage ermittelt die Anzahl Titel aller Verlage, deren Name mit 'A' beginnt.

```SQL
mysql> EXPLAIN SELECT COUNT(*)
FROM buchung, person
WHERE buchung.PersID = person.PersID;
```

Die `table`-Spalte zeigt die Tabellen in der Reihenfolge, in der sie gelesen würden. Die `key`-Spalte gibt den Index an, den MySQL benutzen würde und ist NULL, wenn kein Index verwendet wird. Die `rows`-Spalte gibt die Anzahl von Zeilen an, die für die Abfrage voraussichtlich untersucht werden müssen.

![note](../x_res/note.png) Einen Anhaltspunkt über den Suchaufwand erhält man durch Multiplizieren aller Werte in der *rows*-Spalte. Das Resultat sollte möglichst klein sein.

| Optimierungshinweise                                                       |
|----------------------------------------------------------------------------|
| Indexe beschleunigen die Abfrage aber verlangsamen Änderungen              |
| Index auf Primär- und Fremdschlüssel                                       |
| Indexe auf Attribute, die häufig sortiert werden                           |
| NOT und \<\> können nicht optimiert werden                                 |
| Abfragen mit LIKE sind nur optimierbar, wenn % nicht am Musteranfang steht |
| Abfragen mit Funktionen sind nicht optimierbar                             |

Tab. 9.3: Abfrage-Optimierung mit Indizes

Ausführliche Tipps für das Optimieren enthält das MySQL-Manual.

## Server-Tuning

Server Tuning bezeichnet die optimale *Konfiguration* des DB-Servers, so dass die Hardware möglichst gut genutzt und die SQL-Befehle so schnell wie möglich ausgeführt werden.

![note](../x_res/note.png) Im allgemeinen lohnt sich Server-Tuning nur bei sehr grossen Datenmengen (GByte) und sehr vielen DB-Zugriffen pro Sekunde.

### Optimale Speichernutzung

Beim Start des DB-Servers wird ein Teil des Hauptspeichers für bestimmte Aufgaben reserviert, z.B. als Platz zum Sortieren von Daten.

| Wichtige Speicherparameter |                                                               |
|----------------------------|---------------------------------------------------------------|
| `key_buffer_size`            | für Indizes reservierter Speicher (Default: 8M)               |
| `table_cache`                | maximal Anzahl geöffneter Tabellen (Default: 64)              |
| `sort_buffer`                | Buffergrösse zum Sortieren oder Gruppieren (Defaut: 2M)      |
| `read_buffer_size`           | Speicher für sequentielles Lesen (Default: 128K=131072 Bytes) |

Tab. 9.4: Wichtige Konfigurationsparameter für die Speicherverwaltung

### Query Cache

Die Aufgabe des *Query Cache* besteht darin, die Ergebnisse von SQL-Abfragen zu speichern. Wenn später exakt dieselbe Abfrage wieder durchgeführt wird, so kann das fertige Ergebnis verwendet werden.

**a) Query Cache konfigurieren**:

```
\# Query Cache einschalten

query_cache_size = 32M.       <<<<<<<<<<<

# Query Cache Modus 0=Off, 1=On (Default), 2=Demand

query_cache_type = 1

# max. Grösse für eine Abfrage

query_cache_limit = 50K
```

Damit werden 32 MByte RAM reserviert. Es werden nur Abfrage-Resultate gespeichert, die weniger als 50 kByte benötigen, was vermeidet, dass grosse Ergebnisse alle anderen aus dem Cache verdrängen.

**b) Query Cache verändern**:

Der Query Cache kann für eine Verbindung speziell eingestellt, z.B. ausgeschaltet werden.

`mysql> SET query_cache_type=0;`

**c) Query Cache-Status abfragen**:

Mit SHOW STATUS; werden die Werte verschiedener Statusvariablen des Query Cache angezeigt.

`mysql> SHOW STATUS;`

**d) Query Cache leeren**:

Mit folgendem Befehl werden alle Einträge im Cache gelöscht.

`mysql> RESET QUERY CACHE;`



---


## Literatur und Linkverzeichnis

![](../x_res/Buch.png)

[Verzeichnis](../Literatur.md)