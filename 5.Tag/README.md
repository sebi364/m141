![](../x_res/tbz_logo.png)

# M141 - DB-Systeme in Betrieb nehmen  (3.Tag)

*Autoren: Gerd Gesell ![](../x_res/bee.png) (Kellenberger Michael) 2024*

**Inhalt**

[TOC]

> ([Lösung 2.Tag](../Loesungen/2T_CP_Loes.md))


# Die Optionsdateien my.ini (oder my.cnf)

![](../x_res/Learn.png)
![](../x_res/Train_R1.png)
![](../x_res/Train_D1.png)

<br><br><br>

---

# Daten importieren (Repetition und Vertiefung)

> Dateien zu den folgenden Aufgaben sind hier: [Datenpool!](../Daten)



<br><br><br>



---

# ![](../x_res/CP.png) Checkpoints

[Checkpoint](./3T_CheckPoint.md)


---

## Literatur und Linkverzeichnis

![](../x_res/Buch.png)

[Verzeichnis](../Literatur.md)