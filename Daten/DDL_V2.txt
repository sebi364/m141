CREATE TABLE`firma`.`tbl_Projekte` (
`ID_Projekte` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`ProjektBezeichnung` VARCHAR( 50)NOT NULL ,
`FS_Mitarbeiter` INT NOT NULL
) ENGINE= InnoDB;



CREATE TABLE`firma`.`tbl_TT_Mitarbeiter_Projekte` (
`FS_Mitarbeiter` INT NOT NULL ,
`FS_Projekte` INT NOT NULL ,
PRIMARY KEY (`FS_Mitarbeiter` ,`FS_Projekte`) 
) ENGINE= InnoDB;


CREATE TABLE`firma`.`tbl_Mitarbeiter` (
`ID_Mitarbeiter` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`FamName` VARCHAR( 50)NOT NULL ,
`Vorname` VARCHAR( 30)NOT NULL ,
`FS_Abteilung` INT NOT NULL
) ENGINE= InnoDB;

CREATE TABLE `firma`.`tbl_Abteilungen` (
`ID_Abteilung` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`AbtBezeichnung` VARCHAR( 30)NOT NULL 
) ENGINE= InnoDB;

