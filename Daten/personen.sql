-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mydb` ;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`tbl_Ort`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`tbl_Ort` ;

CREATE TABLE IF NOT EXISTS `mydb`.`tbl_Ort` (
  `ID_Ort` INT NOT NULL,
  `PLZ` VARCHAR(10) NULL,
  `Ortsname` VARCHAR(30) NULL,
  PRIMARY KEY (`ID_Ort`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`tbl_Adresse`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`tbl_Adresse` ;

CREATE TABLE IF NOT EXISTS `mydb`.`tbl_Adresse` (
  `ID_Adresse` INT NOT NULL,
  `Strasse` VARCHAR(30) NULL,
  `Nr` VARCHAR(10) NULL,
  `FS_tbl_Ort` INT NOT NULL,
  PRIMARY KEY (`ID_Adresse`),
  INDEX `REL_tbl_Ort__tbl_Adresse_idx` (`FS_tbl_Ort` ASC) VISIBLE,
  CONSTRAINT `REL_tbl_Ort__tbl_Adresse`
    FOREIGN KEY (`FS_tbl_Ort`)
    REFERENCES `mydb`.`tbl_Ort` (`ID_Ort`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`tbl_Person`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`tbl_Person` ;

CREATE TABLE IF NOT EXISTS `mydb`.`tbl_Person` (
  `ID_Person` INT NOT NULL,
  `Name` VARCHAR(30) NULL,
  `Vorname` VARCHAR(30) NULL,
  `Geburtsdatum` DATETIME NULL,
  `FS_tbl_Adresse` INT NOT NULL,
  PRIMARY KEY (`ID_Person`),
  INDEX `REL_tbl_Adresse__tbl_Person1_idx` (`FS_tbl_Adresse` ASC) VISIBLE,
  CONSTRAINT `REL_tbl_Adresse__tbl_Person1`
    FOREIGN KEY (`FS_tbl_Adresse`)
    REFERENCES `mydb`.`tbl_Adresse` (`ID_Adresse`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
