![](../x_res/tbz_logo.png)

# M141 - DB-Systeme in Betrieb nehmen

*Autoren: Gerd Gesell ![](../x_res/bee.png) (Kellenberger Michael) 2024*


## Tools: Repetition zu Tag 1 & 2

Überlegen Sie sich für jede der unten angegebenen Administrationsaufgaben, mit welchem/n Werkzeug(en) sie ausgeführt werden können. Markieren Sie unten das entsprechende Feld.

|                                        | **Werkzeuge**            |                   |                    |                 |           |                        |                        |            |
|----------------------------------------|--------------------------|-------------------|--------------------|-----------------|-----------|------------------------|------------------------|------------|
| **Administrationsaufgaben**            | Dienst-Manager (Windows) | CMD / WPS-Fenster | XAMPP ControlPanel | mysql (Monitor) | mysqldump | Task-Manager (Windows) | phpMyAdmin / Workbench | Texteditor |
| 1) ein Backup erstellen                |                          |                   |                    |                 |        |                        |                       |            |
| 2) DB aus Backup wiederherstellen      |                          |                   |                    |               |           |                        |                       |            |
| 3) DB-Benutzer erstellen u. verwalten  |                          |                   |                    |                |           |                        |                       |            |
| 4) DB-Server starten                   |                       |                  |                   |                 |           |                     |                        |            |
| 5) DB-Server stoppen                   |                       |                  |                   |                 |           |                     |                        |            |
| 6) Daten in Tabellen kontrollieren     |                          |                   |                    |               |           |                        |                       |            |
| 7) SQL-Skript anpassen oder erstellen  |                          |                   |                    |                 |           |                        |                       |           |
| 8) eine neue Datenbank erstellen       |                          |                   |                    |                |           |                        |                       |            |
| 9) Konfigurationsdateien anschauen     |                          |                  |                   |             |           |                        |                     |           |
| 10) prüfen, ob der Server läuft        |                         |                |                   |             |           |                       |                     |            |
| 11) Server-Installation testen         |                          |                 |                   |               |           |                        |                       |            |
| 12) Server-Status abfragen             |                          |                   |                    |                |           |                        |                       |            |
| 13) SQL-Befehle ausführen              |                          |                   |                    |                |           |                        |                       |            |
| 14) SQL-Skript ausführen               |                          |                   |                    |                |           |                        |                       |            |
| 15) Tabellen erstellen und verwalten   |                          |                   |                    |                |           |                        |                       |         |
| 16) Daten in Tabellen eintragen        |                          |                   |                    |                |           |                        |                       |         |
| 17) Welches ist ein DB-Client?         |                          |                   |                    |                |           |                        |                       |            |

\* unter Vorbehalt, ( ) nicht direkt
