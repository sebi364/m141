![TBZ Logo](./x_res/tbz_logo.png)
![Picto](./x_res/M141_Picto.jpg)



# M141 - Datenbanksystem in Betrieb nehmen

Installiert und konfiguriert ein Datenbanksystem und führt eine Dateninitialisierung durch. <br>
Stellt die Funktionalität sicher und führt die Übergabe in den produktiven Betrieb durch.


### Modulidentifikation BiVo 2019 (Fachrichtungen API & PE)
[Link](https://gitlab.com/modulentwicklungzh/cluster-platform/m141)

### Modulidentifikation BiVo 2014 (Fachrichtung Betriebsinformatiker)
[Modul-ID](./Modul_141_BI.pdf)

---
*Autoren: Gerd Gesell, Kellenberger Michael, Thanam Pangri 2024*


## Lernportfolio

Führen Sie in diesem Modul ein [Lernportfolio](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-Portfolio).


![Selbst-Lern-Zyklus](./x_res/Selbst-Lern-Zyklus_kl.png)
Abb. 1: Selbst-Lern-Zyklus

Organisieren Sie sich anhand des Selbst-Lern-Zyklus mit einem Lernpartner (Tandem).

In den folgenden Unterlagen wird auf die "Lern-Phasen" verwiesen!
 
<br>
<br>

---

**\>>> Hier gehts weiter für Modulunterlagen nach BiVO 2014:** [Unterlagen BiVo 2014](./Skript/README.md)

---


## Lektionenplan BiVo 2019 (Siehe auch Klassenordner)

| Tag  |  Thema | Bemerkung: Auftrag, Übungen | Kompetenzen | 
|:----:|:------ |:-------------- |:---|
|  1   | [Intro & Installation](./1.Tag/README.md)  | Einführung <br> RDBMS Übersicht <br> RDBMS und Tools installieren: <br> MariaDB (Dienst & Klient), Workbench, phpMyAdmin mit Webserver (vzw. XAMPP) | A1 A2 <br> |
|  2   | [Konfiguration & Datenimport](./2.Tag/README.md) | my.ini und Optionen <br> Modell, SQL, CSV, JSON, usw. importieren | B |
|  3   | [Tabellentypen & Transaktionen](./3.Tag/README.md) | MyISAM , InnoDB <br> Locking <br> Konzept Transaktion | C |
|  4   |  [Datenbanksicherheit](./4.Tag/README.md) | Berechtigungen und Zugriff | D |
|  5   | **LB1 Tag 1 - 3**  <br> [RDBMS online](./5.Tag/README.md) |  DB-Server im LAN <br> ODBC-Anbindung                    | C |
|  6   | [Server Administration](./6.Tag/README.md) | Admin Tools <br> Logging <br> Backup & Restore <br> Optimierungen | A C2 D |
|  7   | **Reserve** <br> [Testen & Protokoll](./7.Tag/README.md) | Manipulation verschiedener Parameter mit Penetrationstests <br> Testverfahren verschiedener Speicherformate <br> Leistungstests | E |
|  7/8   |  **LB2 Tag 4 - 6/7** <br> **LB3** [Praxisarbeit](./LB3-Praxisarbeit/README.md) | MySQL in AWS aufsetzen und konfigurieren                     | A B |
|  8/9   | **LB3** [Praxisarbeit](./LB3-Praxisarbeit/README.md) |   DBs von lokaler DBMS auf AWS portieren  (Datenund Berechtigungen)                 | B C D |
|  9/10   | **LB3** [Praxisarbeit](./LB3-Praxisarbeit/README.md) |  DBs von lokaler DBMS auf AWS portieren <br> Dokumentation, Testen                  | D E |

---

![](./x_res/work-in-progress.png)

## Rahmenbedingungen LB3

- Eine bestehende, gesicherte. optimierte, lokale RDBMS mit einer produktiven Datenbasis "locDB"
- Berechtigungen und Benutzerschema auf "locDB" angewendet
- "locDB" getestet
- Migration auf evaluierte cloudbasierte RDBMS "cloudDB".
- Migration mit Script: DB, Berechtigungen und User
- Migration und "cloudDB" getestet
- LB3 Bewertungsraster

---

---

<br>



-------- Aus ID BiVo 2019: -----------

## Lektionenplan Beispiel BiVo 2019

| Anzahl Lektionen | Themen                    | Kompetenzen | Tools                                   |
|---|---|---|---|
| 3 | Inbetriebnahme eines RDBMS<br />Paketinstallation, Konfiguration und Verständnis von Speicherorten | A1, A2 | RDBMS (z.B. [PostgreSQL](https://www.postgresql.org/) oder [MariaDB](https://mariadb.org/)) gemäss Wahl der Lehrperson |
| 6 | Datenimport<br/>Import von einem Dump, CSV-Daten, JSON / XML Import | B |  |
| 3 | Rollen und Berechtigungen<br>Fallstudie mit verschiedenen Usern/Rollen an der DB umsetzen und testen. | C1, D1 |  |
| 2 | Abnahme<br>Abnahmevorgehen, Checkliste, Abnahmeprotokoll, rechtliche Bedeutung | E1 |  |
| 4 | Fokus Datenmigration<br>Strukturänderung migrieren, Fusion verschiedener Quellen in vorhandene DB integrieren. | B1 | |
| 6 | Manipulation verschiedener Parameter mit Penetrationstests<br>Testverfahren verschiedener Speicherformate, Vorteil RAM, Indizes sinnvoll einsetzen, Sicherheit überprüfen | A, C | |
| 8 | Inbetriebnahme eines cloudbasierten DBMS, Datenimport, Berechtigungen, Abnahme (z. B. verschiedene Fallstudien in Gruppen) | A-E | DBMS in AWS (MySQL, MariaDB) <br> Alternative (Key-Value [MongoDB](https://www.mongodb.com/) / Time-Series [Prometheus](https://prometheus.io/) / [Influx DB](https://www.influxdata.com/)) |
| 4 | Organisation, Prüfungen | | |



## Matrix BiVo 2019

| Kompetenzband: | HZ | Grundlagen | Fortgeschritten | Erweitert |
| --- | --- | --- | --- | --- |
| Inbetriebnahme | 1 | A1G: Ich kann ein einfaches RDBMS wie z.B. MySQL/Maria-DB in Betrieb nehmen. | A1F: Ich kann verschiedene Arten von DBMS in Betrieb nehmen. | A1E: Ich kann für den Kunden den passenden Typ eines DBMS evaluieren. |
| | 1 | A2G: Ich kann das DBMS so bereitstellen, dass die Funktionalität gewährleistet ist. | A2F: Ich kann wichtige Parameter des DBMS fachgerecht setzen, um die Funktionalität und Performance zu gewährleisten. | A2E: Ich kann auch in einer anspruchsvollen DBMS-Umgebung  Einflussfaktoren (hoher Speicherbedarf, hohes Transaktionsvolumen und hohe Verfügbarkeit) erkennen und Mittel nennen, um die hohen Anforderungen zu erfüllen. |
| DB/Tabellen einrichten | 2 | B1G: Ich kann Daten (Dump) in ein DBMS laden. | B1F: Ich kann Daten von einem bisherigen DBMS migriert ins vorliegende DBMS-Schema überführen und ins System integrieren. | B1E: Ich kann typenfremde Daten ins vorliegende DBMS-Schema migrieren, validieren und ins System integrieren. |
| Operativer Betrieb | 3 | C1G: Ich kann das DBMS mit Standard-Tools absichern. | C1F: Ich kann das DBMS mit Standard-Tools absichern. Dabei ergänze ich DBMS-Funktionen mit Systemdesign-Möglichkeit wie z. B. RAID oder Cluster. | C1E: Ich kann das DBMS mit Standard-Tools absichern. Meine Lösung ist ganzheitlich durchdacht. |
| | 3,7 | C2G: Ich kann einfache Tests durchführen und diese in einem vorgegebenem Schema festhalten. | C2F: Ich kann das DBMS mit spezifischen Tests überprüfen und das Resultat im Testprotokoll festhalten. | C2E: Ich kann das DBMS mit Benchmarking messen und kann das Testprotokoll mit fachgerechten Verbesserungsvorschlägen ergänzen. |
| Berechtigungen | 4 | D1G: Ich kann spezifische Zugriffsrechte auf DB-Ebene vergeben. | D1F: Ich kann spezifische Zugriffsrechte auf verschiedene Objekte der DB vergeben. | D1E: Ich kann spezifische Zugriffsrechte in komplexen Umgebungen erteilen und fehlerhafte Vergaben korrigieren. |
| | 4 | D2G: Ich kann Stored Procedures und Views nach Vorgabe einrichten. | D2F: Ich kann Stored Procedures und Views einsetzen, um die Zugriffsmöglichkeiten zu verbessern. | D2E: Ich kann Stored Procedures und Views aus einer Praxis-Situation ableiten und einrichten. |
| Abnahme | 5 | E1G: Ich kann ein DBMS fachgerecht dokumentieren. | E1F: Ich kann die Übergabe an den Betrieb planen und das Abnahmeprotokoll mit der fachgerechten Dokumentation bereitstellen. | E1E: Ich kann die Übergabe an den Betrieb planen und das Abnahmeprotokoll mit der vollständigen fachgerechten Dokumentation bereitstellen. |

## Kompetenzstufen

### Grundlagen | Stufe 1 

Diese Stufe ist als Einstieg ins Thema gedacht. Der Fokus liegt hier auf dem Verstehen von Begriffen und Zusammenhängen. 

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 3.0.*

### Fortgeschritten | Stufe 2 

Diese Stufe definiert den Pflichtstoff, den alle Lernenden am Ende des Moduls möglichst beherrschen sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 4.5*

### Erweitert | Stufe 3 

Diese Lerninhalte für Lernende gedacht, die schneller vorankommen und einen zusätzlichen Lernanreiz erhalten sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 6*
