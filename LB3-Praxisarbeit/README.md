![](../x_res/tbz_logo.png)

# M141 - DB-Systeme in Betrieb nehmen (8-10.Tag)

*Autoren: Gerd Gesell ![](../x_res/bee.png) (Kellenberger Michael) 2024*

![](../x_res/work-in-progress.png)

**Inhalt**

[TOC]

# Einführung 

![](../x_res/Learn.png)


## DB-Erhebung

![](../x_res/Train_R1.png)



# XAMPP Arbeitsumgebung installieren

![](../x_res/Train_D1.png)


---

# ![](../x_res/CP.png) Checkpoint

[Checkpoint](1T_Checkpoint.md)

---

## Literatur und Linkverzeichnis

![](../x_res/Buch.png)

[Verzeichnis](../Literatur.md)