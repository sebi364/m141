![](../x_res/tbz_logo.png)

# M141 - DB-Systeme in Betrieb nehmen  (4.Tag)

*Autoren: Gerd Gesell ![](../x_res/bee.png) (Kellenberger Michael) 2024*

**Inhalt**

[TOC]

> ([Lösung 3.Tag](../Loesungen/3T_CP_Loes.md))


# Datenbank-Sicherheit

![](../x_res/Learn.png)

Bei den bisherigen Aufgaben sind Sie kaum mit der Benutzerverwaltung von MySQL in Berührung gekommen. Wenn Sie den MySql-Klient gestartet haben, hatten Sie für bestimmte Datenbanken nicht genügend Rechte, daher mussten wir `mysql –u root` mit der Benutzerkennung für den root-User eingeben. 

![](../x_res/Hinweis.png )Auch bei der Einrichtung des Datenbankverzeichnisses auf dem eigenen Laufwerk waren Aktionen notwendig, die die Benutzerverwaltung betreffen: die **`mysql`-Datenbasis** muss im `datadir`-Verzeichnis vorhanden sein, sonst lässt sich der Server gar nicht aufstarten.

![](../x_res/System-Tabellen.png)


Abb. 17: MySQL System-Tabellen in phpMyAdmin

Der `GRANT`-Befehl vom SQL, den Sie bereits bei einer Übung eingesetzt haben, nimmt die entsprechenden Einträge in den Berechtigungs-Tabellen (**\*_priv**) vor. Die Einschränkungen können bis auf Attributsebene vorgenommen werden. Je nach Berechtigung kann ein Benutzer also nicht alle Spalten einer Tabelle sehen oder verändern. Die für uns wichtigen Tabellen sind: **global\_priv, db, tables\_priv und colums\_priv**. (Vergl. Abb.17)

## Zugriffssystem

Die Zugriffskontrolle führt MySQL in zwei Phasen durch. Die erste Phase - der Klient baut die Verbindung zum Server auf – nennt sich **Authentifizierung**. Sie überprüft drei Angaben, nämlich Benutzername, Passwort und die Rechner, von dem aus zugegriffen wird. Diese Informationen findet MySQL in der  Ansicht (View)  `user`. Im positiven Fall werden anschliessend bei jedem Request an die Datenbank die Berechtigungen überprüft: die **Autorisierung**.

![](../x_res/View_User.png)


###   Authentifizierung (Identitätsprüfung: Wer?): 

Der Server prüft, ob sich der Benutzer mit dem Server verbinden darf. Identifiziert wird jeder Benutzer durch seinen Benutzer- und Hostnamen. (Dieser Benutzername gilt für den DB-Server und hat nichts mit dem Benutzernamen des Betriebssystems zu tun.) Das Sicherheitssystem von MySQL beruht auf folgende Informationen:

| Sicherheitsinformationen |                                                   |
|--------------------------|---------------------------------------------------|
| **`Benutzername`**       | Name für die DB-Anmeldung (Default: leer)         |
| **`Passwort`**           | Wird verschlüsselt abgespeichert (Default: leer)  |
| **`Hostname`**           | Rechnername des Benutzers  <br> <br>  - `localhost`: User darf auf dem *Server* einloggen <br> - `'%'` (Default): User darf von *überall*, ausser vom Server einloggen  <br> -` 172.16.17.111`: User darf von Klient mit *IP* einloggen <br> - `'name.local'`:  User darf von Klient mit *Hostnamen* einloggen |

Tab. 6.3: Die 3 Informationen des Sicherheitssystems

Ein Benutzer wird also folgendermassen angelegt (mit und ohne Poasswort):

```
DROP USER IF EXISTS 'user'@'hostname';
CREATE USER 'user'@'hostname' IDENTIFIED BY 'Passw0rt';  -- User with password 'Passw0rt'

CREATE USER 'user2'@localhost;                           -- User with no password
``` 

[Manual CREATE USER](https://mariadb.com/kb/en/create-user/)

### Passwort (-verschlüsselung)

Das Passwort darf aus Sicherheitsgründen nicht unverschlüsselt abgespeichert werden. Auch sollte obiger Befehl mit Passwort im Klartext nicht in einem Script erscheinen.

Zum Verschlüsseln stellt MySQL die Funktion *password()* zur Verfügung, die einen 41-Bytes Hash erzeugt. Sie können den Hashwert erzeugen und anzeigen mit: 

`SELECT PASSWORD('TBZforever');`

> Beachten Sie den Stern **\*** am Anfang des Hash-Wertes!

Das Passwort eines Users wird folgendermassen gesetzt (oder geändert) und gleich aktiviert:

```
SET PASSWORD FOR 'user'@'%'  = password('TBZforever'); 
SET PASSWORD FOR 'user2'@localhost = '*74B1C21ACE0C2D6B0678A5E503D2A60E8F9651A3');
FLUSH PRIVILEGES;                     -- Aktivierung -- nie vergessen!
```

`FLUSH PRIVILEGES` macht die Änderungen sofort wirksam (statt erst nach dem Server-Neustart).

![note](../x_res/note.png) Gelöscht wird das Passwort durch Eingabe eines leeren Passwortes.

[Manual SET PASSWORD](https://mariadb.com/kb/en/set-password/)

![](../x_res/Train_r1.png)

![](../x_res/ToDo.png) **User erstellen:**

- Erstellen Sie ein paar Benutzer mit verschiedenen Passwörter und Hostnamen.
- Untersuchen Sie die Einträge in der Ansicht `mysql.user` und der Datenabsis `mysql.global_priv`.
- Testen Sie die Login. (Welche gehen nicht?)
- Ändern Sie einzelne Passwörter.
- Löschen Sie die Benutzer wieder.

---

![](../x_res/Learn.png)

###   Autorisierung (Prüfung der Berechtigung, Was?):
   
Die Autorisierung kann entweder *global* oder *lokal* auf Tabellen, usw. vergeben werden. 


![](../x_res/Zugriffsberechtigung.png)

Abb. 18: Zugriffsberechtigung für verschiedene Benutzer einer Datenbank


#### Das MySQL-Zugriffssystem

| Begriffe           |                                                                                                                                                                                                                                                                                                        |
|--------------------|-----|
| Privileg               | Berechtigung auf eine DB, Tabelle oder Spalte                                                                                                                                                                                                                                                          |
| **Globales** Privileg  | Berechtigung für alle DB, Tabellen und Spalten                                                                                                                                                                                                                                                         |
| **SELECT**-Privileg    | Benutzer darf Daten lesen                                                                                                                                                                                                                                                                              |
| **UPDATE**-Privileg    | Benutzer darf Daten ändern                                                                                                                                                                                                                                                                             |
| **GRANT**-Privileg     | Benutzer darf Zugriffsrechte festlegen u. Benutzer erstellen                                                                                                                                                                                                                                           |
| **FILE**-Privileg      | **\*** Werden benötigt, um eine Reihe von Datei-Befehlen auszuführen: `LOAD FILE ()`, `LOAD DATA INFILE …`   |
| **All**                | alle Privilegien (ausser GRANT)                                                                                                                                                                                                                                                                        |
| **Usage**              | keine Privilegien (nur Connect auf den Server)                                                                                                                                                                                                                                                         |

Tab. 6.3: Begriffe betreffend Privilegien


![](../x_res/caution.png)*) Das FILE-Privileg sollte mit Vorsicht eingesetzt werden, da dieser User alle Dateien lesen kann, die öffentlich sind oder vom MySQL-Server gelesen werden können – unabhängig vom Datenbankverzeichnis!

### Benutzer einrichten und Zugriffsrechte festlegen

Diee Befehle `GRANT` und `REVOKE` (Data Control Language = DCL) werden eingesetzt. Manual [GRANT](https://mariadb.com/kb/en/grant/) / [Revoke](https://mariadb.com/kb/en/revoke/)

![caution](../x_res/caution.png) Voraussetzung: der Benutzer benötigt das `GRANT`-Privileg. Entweder erstellen Sie einen User mit dem Zusatz „`WITH GRANT OPTION`“ oder Sie führen die Beispiele als `root`-User aus.

#### Zugriffsrechte ändern mit GRANT und REVOKE

Mit den Befehlen `GRANT` und `REVOKE` werden Privilegien erteilt bzw. gelöscht. Beachten Sie in der Syntax: `GRANT TO` und `REVOKE FROM` – ist aber ja logisch!

```SQL
GRANT privileg1 [, privileg2, ...]
ON [datenbank.]tabelle
TO user@host [IDENTIFIED BY 'passwort'] [WITH GRANT OPTION] ;

REVOKE privileg1 [, privileg2, ...]
ON [datenbank.]tabelle
FROM user@host ;
```

| Privilegien           |                                                         |
|-----------------------|---------------------------------------------------------|
| **ON** \*.\*              | globale Privilegien (alle DB mit allen Tabellen)        |
| **ON** db.\*              | DB-Privilegien (alle Tabellen der DB)                   |
| **ON** db.tb              | Tabellen-Privilegien (alle Spalten der Tabelle)         |
| (att1, att2) **ON** db.tb | Spalten-Privilegien (Spalten att1 und att2 der Tabelle) |

Tab. 6.4: Die verschiedenen Ebenen der Privilegien

**Zugriffsrechte auf alle Tabellen einer DB festlegen**:

Der folgende Befehl erteilt dem Benutzer `hotel_admin` alle Privilegien (inklusive Grant). Ein bestehendes Passwort wird dabei nicht verändert.

`GRANT ALL ON hotel.* TO hotel_admin@localhost WITH GRANT OPTION;`

Der Befehl um einem User Dateizugriffsberechtigungen zu geben, lautet:

`GRANT FILE ON *.* To Username@‘%‘`

**Lesen und Ändern:**

Der folgende Befehl gibt `hotel_user` das Recht, die Daten in der DB hot``el zu lesen und zu ändern:

`GRANT SELECT, INSERT, UPDATE, DELETE ON hotel.* TO hotel_user@localhost;
`

![note](../x_res/note.png) Mit `REVOKE` kann der Zugriff auf einzelne Tabellen oder Spalten nicht verboten werden, sondern nur auf die ganze DB.

**Zugriffsrechte betrachten:**

Prüfung der Zugriffsrechte einzelner Benutzer.

` SHOW GRANTS FOR hotel_admin@localhost;
`

<br> 

### Strategie zur Rechtevergabe:

- **Globale Rechte** 

  Werden für Admin-Accounts '`root`' bzw. '`admin`' auf alle DBs (\*.\*) vergeben:
  
  ```
  GRANT ALL PRIVILEGES ON *.* TO 'admin'@localhost;
  FLUSH PRIVILEGES;                                  -- Aktivierung -- nie vergessen!
  SHOW GRANTS FOR 'admin'@localhost;
  ```

-  **Lokale Rechte**

   Werden für User-Accounts vergeben. Die Rechte werden in Gruppenrollen definiert und die Benutzer den Gruppen zugeordnet:
   
   ```
   CREATE ROLE rolle;
   GRANT SELECT, INSERT, UPDATE, DELETE ON db.tbl TO rolle;  -- Rolle wird erzeugt
   GRANT SELECT, ... ON db.tbl TO user@hostname IDENTIFIED BY 'Passw0rd'; -- User wird erzeugt!
   GRANT rolle TO user@hostname;  -- Rolle wird User übertragen                     
   FLUSH PRIVILEGES;                                  -- Aktivierung -- nie vergessen!
   
   SELECT CURRENT_ROLE;  -- Aktive Rollen für aktuellen Benutzer werden angezeigt (Standard NULL - also keine)
   SET ROLE rolle;       -- Rollen dem aktuellen User zuordnen

   ```
   
   [Manual Roles](https://mariadb.com/kb/en/roles_overview/)

   Der Server prüft für jeden einzelnen SQL-Befehl (z.B. `SELECT`, `UPDATE`, `DELETE`, `DROP`), ob der Benutzer ihn ausführen darf. Wenn nicht, bricht die Ausführung des Befehls mit einer Fehlermeldung ab. 

#### Zugriffsmatrix

Für jeden Benutzer bzw. Benutzertyp (Gruppe, Rolle) muss genau festgelegt werden, auf welche Daten er in welcher Form zugreifen darf. Dies kann in Form einer **Zugriffsmatrix** erfolgen.

| Zugriffsmatrix            |                |       |       |    | |       |       |       |   |
|---------------------------|----------------|-------|-------|----|-|-------|-------|-------|---|
| Benutzergruppe =\>        | Verkauf        |       |       |    | |  Management |       |       | 
| Tabellen / Attribute      | S              | I     | U     | D  | | S     | I     | U     | D     |   
| produkte                  | **x**          |       |       |    | | **x** | **x** | **x** | **x** |   
| personal                  |                |       |       |    | |    -   | **x** |     -  | **x** |   
| **-** lohn                |                | --    |       | -- | | **x** |   --    | **x** |  -- |   
| **-** restliche Attribute |                | --    |       | -- | | **x** |   --    | **x** |  -- |   
| rechnungen                | **x**          |       |       |    | | **x** | **x** | **x** | **x** |   
| kunden                    | **x**          | **x** | **x** |    | | **x** | **x** | **x** | **x** |   

*S = Select, I = Insert, U = Update, D = Delete, -- = nicht möglich, - = nicht mehr möglich*

Tab. 6.2: Zugriffsmatrix für eine Datenbank

Die Berechtigungen können für jede Tabelle und sogar für jedes Attribut einzeln vergeben werden. So ist z.B. in Tab. 6.2 das Attribut lohn getrennt von den restlichen Attributen eingetragen. I(nsert)- und D(elete)-Berechtigungen können nur für ganze Datensätze vergeben werden.

```
CREATE ROLE verkauf, management;
GRANT SELECT ON firma.produkte TO verkauf;  
GRANT SELECT (lohn, ..., ...) ON firma.personal TO verkauf;
...
GRANT SELECT, INSERT, UPDATE, DELETE ON firma.produkte TO management;
...
GRANT verkauf TO user@host;   --- @user: SET ROLLE nicht vergesssen!
...
```

[YT Erklärvideo zu Rollen](https://www.youtube.com/watch?v=LWWuFMhI6FY)

![](../x_res/Train_r1.png)

![](../x_res/ToDo.png) **Zugriffsmatrix umsetzen:** DB Firma

- Erstellen Sie die zwei Rollen `varkauf` und `management`.
- Erstellen Sie pro Gruppe einen Benutzer
- Fügen Sie die Rechte gemäss Zugriffsmatrix den Gruppen zu und übertragen sie diese den entsprechenden Benutzern.
- Untersuchen Sie die Einträge in der Ansicht `mysql.user` und der Datenabsis `mysql.global_priv`.
- Testen Sie den Zugriff beider Benutzer (SET Role x nicht vergessen!)

- Löschen Sie die Gruppen und Benutzer wieder.

---

# DB-Server absichern - Default-Sicherheitseinstellungen

![caution](../x_res/caution.png) Nach der Default-Installation von MySQL gilt eine sehr unsichere Einstellung ohne Passwort-Absicherung. Jeder kann sich ohne Passwort vom lokalen Rechner oder von jedem externen Rechner aus als `root` oder sogar ohne Benutzernamen anmelden!

> `root`: Wie unter Unix/Linux spielt `root` bei MySQL die Rolle des *Administrators* (*Superuser*) mit unbeschränkten Rechten. Nach der Installation kann man sich als `root` ohne Passwort anmelden. Die Defaulteinstellungen sind in der Systemdatenbank *mysql* abgespeichert und werden durch das MySQL-Setup-Programm eingetragen.

Die folgenden Massnahmen schränken den Zugang ein. Sie sollten direkt nach der Installation eines DB-Servers erfolgen.

**`root`-Passwort für lokalen Zugang festlegen**: Mit folgendem Befehl wird für den Superuser root ein Passwort für den Zugriff vom Server-Rechner aus festgelegt.

```SQL
C:\>mysql -u root

SET PASSWORD FOR root@localhost = PASSWORD('superpasswort');
FLUSH PRIVILEGES;     -- nie vergessen!

```

![caution](../x_res/caution.png) Passen Sie auf, dass Sie dem Benutzer `root` nicht die nötigen Privilegien wegnehmen, sonst haben Sie u.U. keinen Zugriff mehr auf den eigenen Server !

![note](../x_res/note.png) Von jetzt an muss man sich als root mit -u root mit der Option -p anmelden und das Passwort eingeben.

```
C:\>mysql -u root -p
Enter password: *****
```

Falls Sie diese Änderung über phpMyAdmin durchgeführt haben, haben Sie jetzt ein Problem: Die Verbindung zum Server kann nicht hergestellt werden. Logisch! Wir sind ja als root ohne Passwort angemeldet. Die notwendigen Änderungen müssen in der Datei `config.inc.php` durchgeführt werden. Diese finden Sie in `C:/xampp/phpMyAdmin`. Weitere Informationen finden Sie in der Dokumentation: <https://docs.phpmyadmin.net/en/latest/config.html#basic-example>.


![](../x_res/Anpassung.png)

Abb. 19: notwendige Anpassung in der config.inc.php für root-Passwort

Sie können den auth-type auch auf *http* oder *cookie* (siehe Kommentartext im config-File) setzen und werden dann beim Start von phpMyAdmin nach Name und Passwort gefragt. User- und Passworteinträge können in diesem Fall leer bleiben. Das hat den Vorteil, dass die Anmeldung im phpMyAdmin mit verschiedenen Benutzern erfolgen kann.

**`root`-Zugang von fremden Rechnern aus verhindern**: Löschen des Users`root` von anderen Rechnern (`%`) aus:

```SQL
DROP USER 'root'@'%';  -- oder mindestens REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'root'@'%';
FLUSH PRIVILEGES;
```

Nach dem Löschen ist der Zugang als `root` mit Passwort nur von `localhost` aus möglich;  <br> oder wir setzen zwingend ein Passwort:

`SET PASSWORD FOR 'root'@’%’ = PASSWORD('superpasswort')`

**Kein Zugang ohne Passwort von externen Rechnern**: Auch alle anderen Benutzer von extern müssen ein Passwort haben.

**Lokalen Zugang ohne Passwort ermöglichen**: Vielleicht ist die Absicherung des Servers nun zu einschränkend und man möchte für den lokalen Rechner einen generellen MySQL-Zugang ohne Benutzernamen und Passwort doch wieder zulassen:

```SQL
GRANT USAGE ON *.* TO ''@localhost;
FLUSH PRIVILEGES;
```

![](../x_res/Hinweis.png)`USAGE` erlaubt nur das Anmelden ohne Privilegien.

Die Einträge in der Tabelle `user` in der `mysql`-Datenbank sind vom Administrator sehr kritisch zu untersuchen und den Bedürfnissen entsprechend anzupassen. Diese Schritte sind für eine professionelle Umgebung nur der Anfang.

## Der pma-User für phpMyAdmin

Nach der Installation von phpMyAdmin wird ein zusätzlicher Benutzer erzeugt „`pma`“. Er hat zwar nur eingeschränkte Rechte, sollte aber ebenfalls mit Passwort versehen werden. 

```
SHOW GRANTS FOR pma@localhost;
+--------------------------------------------------------------------------------------+
| Grants for pma@localhost                                                             |
+--------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO `pma`@`localhost`                                              |
| GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE ON `phpmyadmin`.* TO `pma`@`localhost` |
+--------------------------------------------------------------------------------------+
2 rows in set (0.000 sec)
```

**Auch dieses Passwort muss natürlich in der User-Tabelle eingetragen** und in der `config.inc.php` abgelegt werden. Sie können phpMyAdmin nur dann aufstarten, wenn Sie einen gültigen Benutzernamen mit Passwort haben **und** wenn die Angaben des „`pma`“-Controlusers korrekt sind!

Wer diesen Benutzer löscht, kann phpMyAdmin nicht mehr verwenden. Wenn dies aus Versehen geschehen ist, muss nicht alles neu installiert werden, sondern Sie können den User neu anlegen. Weitere Informationen finden Sie in der Dokumentation: <https://docs.phpmyadmin.net/en/latest/config.html#example-for-signon-authentication>.

`mysql> SET PASSWORD FOR pma@localhost = PASSWORD('irgendwas');`

![](../x_res/pma.png)

Abb. 20: Damit ist auch der Benutzer "pma" passwortgeschützt

Hinweis: Wenn das Passwort für den `pma`-User gesetzt wurde, kann man MySQL evtl. nicht mehr über das XAMPP-Control-Panel herunterfahren. Bei mir hat auch eine Anpassung in der Datei `mysql_stop.bat` im `xampp`-Verzeichnis keinen Erfolg gebracht. Man muss den MySQL-Server also durch Eingabe folgender Zeile manuell herunterfahren:

`C:\...\mysql\bin\mysqladmin --user=pma --password=irgendwas shutdown`

Alternativ kann man natürlich auch die `mysql_stop.bat` modifizieren und danach aufrufen (z. B. über eine Verknüpfung auf dem Desktop).  


<br><br><br>

---

![](../x_res/Train_D1.png)

![](../x_res/ToDo.png) **Fallbeispiel**: Rechte auf Tabellen und Spalten (ohne Rollen) (ca. 1L)


   [Datenpool](../Daten/Blogeinträge.sql)
   
   [Tutorial Blogeinträge](https://gridscale.io/community/tutorials/mysql-feinabstimmung-benutzerrechte/)
    
   [(Lösung SQL Befehle)](./Blogeinträge_Lös.sql)
   
<br> 
    
---    

# phpMyAdmin Rechteverwaltung

Zugang zu den Benutzerkonten:

![](../x_res/pmaRechte_1.png)

Globale Privilegien setzen / löschen:

![](../x_res/pmaRechte_2.png)

Lokale Privilegien auf mehrere DBs:

![](../x_res/pmaRechte_3.png)

Lokale Privilegien setzen / löschen:

![](../x_res/pmaRechte_4.png)

<br> 


---

# ![](../x_res/CP.png) Checkpoints

[Checkpoint](./4T_CheckPoint.md)


---

## Literatur und Linkverzeichnis

![](../x_res/Buch.png)

[Verzeichnis](../Literatur.md) 