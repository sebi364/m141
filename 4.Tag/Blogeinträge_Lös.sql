CREATE USER 'jens'@'localhost' IDENTIFIED BY 'passwort1';
CREATE USER 'katrin'@'localhost' IDENTIFIED BY 'passwort2';
CREATE USER 'natascha'@'localhost' IDENTIFIED BY 'passwort3';
CREATE USER 'michael'@'localhost' IDENTIFIED BY 'passwort4';

--------------------
CREATE DATABASE Blogeinträge;

GRANT ALL PRIVILEGES ON Blogeinträge.* TO 'jens'@'localhost';

USE mysql;
SELECT user,host,db,select_priv,insert_priv FROM db;

--------------------
USE Blogeinträge;

DROP TABLE FAVORITENLISTE;
CREATE TABLE FAVORITENLISTE(
FavoritenlisteID INT PRIMARY KEY /*CHECK (FavoritenlisteID > 0) */);

/*
CREATE TABLE NUTZER(
Benutzername VARCHAR(50) PRIMARY KEY CHECK(LENGTH(Benutzername) > 0), 
EMailAdresse VARCHAR(50) NOT NULL CHECK(EMailAdresse LIKE '%@%.de' OR EMailAdresse LIKE '%@%.com'), 
Geschlecht CHAR(1) NOT NULL CHECK(Geschlecht IN ('w', 'm')), 
Geburtsdatum DATE NOT NULL, 
Passwort VARCHAR(10) NOT NULL CHECK(LENGTH(Passwort) > 0), 
FavoritenlisteID INT NOT NULL CHECK(FavoritenlisteID > 0), 
FOREIGN KEY(FavoritenlisteID) REFERENCES FAVORITENLISTE(FavoritenlisteID));

CREATE TABLE REDAKTEUR(
Benutzername VARCHAR(50) PRIMARY KEY CHECK(LENGTH(Benutzername) > 0), 
Vorname VARCHAR(50) NOT NULL CHECK(LENGTH(Vorname) > 0), 
Name VARCHAR(50) NOT NULL CHECK(LENGTH(Name) > 0), 
Vorstellungstext TEXT CHECK(LENGTH(Vorstellungstext) < 1001), 
FOREIGN KEY(Benutzername) REFERENCES NUTZER(Benutzername));

CREATE TABLE CHEFREDAKTEUR(
Benutzername VARCHAR(50) PRIMARY KEY CHECK(LENGTH(Benutzername) > 0), 
Telefonnummer VARCHAR(20) NOT NULL CHECK(LENGTH(Telefonnummer) > 0), 
FOREIGN KEY(Benutzername) REFERENCES REDAKTEUR(Benutzername));

CREATE TABLE BLOGEINTRAG(
BlogeintragID INT PRIMARY KEY CHECK(BlogeintragID > 0), 
Erstellungsdatum DATE NOT NULL, 
Änderungsdatum DATE CHECK(Änderungsdatum >= Erstellungsdatum), 
Titel VARCHAR(250) NOT NULL CHECK(LENGTH(Titel) > 0), 
Text TEXT CHECK(LENGTH(Text) < 1001), 
Benutzername VARCHAR(50) NOT NULL CHECK(LENGTH(Benutzername) > 0), 
FOREIGN KEY(Benutzername) REFERENCES REDAKTEUR(Benutzername));

CREATE TABLE FAVORITENLISTEenthaltenBLOGEINTRAG(
BlogeintragID INT NOT NULL CHECK(BlogeintragID > 0), 
FavoritenlisteID INT NOT NULL CHECK(FavoritenlisteID > 0), 
PRIMARY KEY(BlogeintragID, FavoritenlisteID), 
FOREIGN KEY(BlogeintragID) REFERENCES BLOGEINTRAG(BlogeintragID), 
FOREIGN KEY(FavoritenlisteID) REFERENCES FAVORITENLISTE(FavoritenlisteID));

*/

CREATE TABLE NUTZER(
Benutzername VARCHAR(50) PRIMARY KEY, 
EMailAdresse VARCHAR(50) NOT NULL, 
Geschlecht CHAR(1) NOT NULL, 
Geburtsdatum DATE NOT NULL, 
Passwort VARCHAR(10) NOT NULL, 
FavoritenlisteID INT NOT NULL, 
FOREIGN KEY(FavoritenlisteID) REFERENCES FAVORITENLISTE(FavoritenlisteID));

CREATE TABLE REDAKTEUR(
Benutzername VARCHAR(50) PRIMARY KEY , 
Vorname VARCHAR(50) NOT NULL , 
Name VARCHAR(50) NOT NULL , 
Vorstellungstext TEXT , 
FOREIGN KEY(Benutzername) REFERENCES NUTZER(Benutzername));

CREATE TABLE CHEFREDAKTEUR(
Benutzername VARCHAR(50) PRIMARY KEY , 
Telefonnummer VARCHAR(20) NOT NULL , 
FOREIGN KEY(Benutzername) REFERENCES REDAKTEUR(Benutzername));

CREATE TABLE BLOGEINTRAG(
BlogeintragID INT PRIMARY KEY , 
Erstellungsdatum DATE NOT NULL, 
Änderungsdatum DATE , 
Titel VARCHAR(250) NOT NULL , 
Text TEXT , 
Benutzername VARCHAR(50) NOT NULL , 
FOREIGN KEY(Benutzername) REFERENCES REDAKTEUR(Benutzername));

CREATE TABLE FAVORITENLISTEenthaltenBLOGEINTRAG(
BlogeintragID INT NOT NULL , 
FavoritenlisteID INT NOT NULL , 
PRIMARY KEY(BlogeintragID, FavoritenlisteID), 
FOREIGN KEY(BlogeintragID) REFERENCES BLOGEINTRAG(BlogeintragID), 
FOREIGN KEY(FavoritenlisteID) REFERENCES FAVORITENLISTE(FavoritenlisteID));

-------------

SHOW TABLES;

--------------
--- GRANT ALL PRIVILEGES ON Blogeinträge.* TO 'katrin'@'localhost';
--- GRANT ALL PRIVILEGES ON Blogeinträge.REDAKTEUR TO 'katrin'@'localhost';
GRANT SELECT (Benutzername, EMailAdresse, FavoritenlisteID) ON Blogeinträge.NUTZER TO 'katrin'@'localhost';
GRANT SELECT ON Blogeinträge.FAVORITENLISTEenthaltenBLOGEINTRAG TO 'katrin'@'localhost';
GRANT SELECT, INSERT, UPDATE (Titel, Text, Benutzername) ON Blogeinträge.BLOGEINTRAG TO 'katrin'@'localhost';

GRANT SELECT (Benutzername, EMailAdresse, FavoritenlisteID) ON Blogeinträge.NUTZER TO 'natascha'@'localhost';
GRANT SELECT ON Blogeinträge.REDAKTEUR TO 'natascha'@'localhost';
GRANT SELECT ON Blogeinträge.BLOGEINTRAG TO 'natascha'@'localhost';
GRANT ALL PRIVILEGES ON Blogeinträge.FAVORITENLISTEenthaltenBLOGEINTRAG TO 'natascha'@'localhost';

----
USE mysql;
SELECT * from tables_priv;
SELECT * from columns_priv;

--------------------------------

INSERT INTO Blogeinträge.FAVORITENLISTE VALUES(1001);
INSERT INTO Blogeinträge.FAVORITENLISTE VALUES(1002);
INSERT INTO Blogeinträge.FAVORITENLISTE VALUES(1003);
INSERT INTO Blogeinträge.FAVORITENLISTE VALUES(1004);

INSERT INTO Blogeinträge.NUTZER VALUES('jens', 'jensmueller@googlemail.com', 'm', '1984-05-30', 'ferrari', 1001);
INSERT INTO Blogeinträge.NUTZER VALUES('katrin', 'katrin-1990@googlemail.com', 'w', '1990-01-16', 'darkeyes', 1002);
INSERT INTO Blogeinträge.NUTZER VALUES('natascha', 'natascha_w@gmx.de', 'w', '1987-08-22', 'starwish', 1003);
INSERT INTO Blogeinträge.NUTZER VALUES('michael', 'michael_steffens@googlemail.com', 'm', '1989-03-14', 'infotech', 1004);

INSERT INTO Blogeinträge.REDAKTEUR VALUES('katrin', 'Katrin', 'Loos', 'Hi, mein Name ist Katrin und ich bin seit ...');
INSERT INTO Blogeinträge.REDAKTEUR VALUES('jens', 'Jens', 'Müller', 'Hey, ich freue mich auf dieser Plattform als ...');

INSERT INTO Blogeinträge.CHEFREDAKTEUR VALUES('jens', '+49 178 3339990');

INSERT INTO Blogeinträge.BLOGEINTRAG VALUES(2001, '2018-02-20', NULL, 'Test-Headline', 'In diesem Blogeintrag möchte ich Euch zeigen ...', 'katrin');
INSERT INTO Blogeinträge.BLOGEINTRAG VALUES(2002, '2018-02-20', '2018-02-24', 'Test-Headline', 'Heute werde ich über ...', 'jens');

INSERT INTO Blogeinträge.FAVORITENLISTEenthaltenBLOGEINTRAG VALUES(2001, 1003);
INSERT INTO Blogeinträge.FAVORITENLISTEenthaltenBLOGEINTRAG VALUES(2001, 1004);
INSERT INTO Blogeinträge.FAVORITENLISTEenthaltenBLOGEINTRAG VALUES(2002, 1002);