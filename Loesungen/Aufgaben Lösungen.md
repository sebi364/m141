![](../x_res/tbz_logo.png)

# M141 - DB-Systeme in Betrieb nehmen


## 4.3 DB und Benutzer erstellen

Alle SQL-Statements sollten von der Konsole abgesetzt werden! Sie können die Auswirkungen aber mit phpMyAdmin überprüfen, korrigieren, erneut absetzen oder was Sie wollen. Die notwendigen Dumps und SQL-Skripte finden Sie auf dem BSCW im Ordner SQL in der zip-Datei *DB_Skripte_Dumps.zip*.

1.  Legen Sie eine neue Datenbank mit dem Namen *buchverwaltung* an.  
     CREATE DATABASE buchverwaltung;
2.  Definieren Sie einen Benutzer mit dem Namen *krauthammer*, der alle Rechte auf der Datenbank *buchverwaltung* hat. Er darf aber nur vom localhost zugreifen. Sein Passwort sei *fuessli*.  
     GRANT ALL ON buchverwaltung.\* TO krauthammer2@localhost   
     IDENTIFIED BY 'fuessli' WITH GRANT OPTION
3.  Testen Sie den neuen User, indem Sie sich von der Konsole her anmelden und mit dem Befehl **show databases;** alle Datenbanken anzeigen lassen, auf die Sie Berechtigungen haben.  
     Anzeige von information_schema und buchverwaltung
4.  Erstellen Sie einen weiteren User, wie in Aufgabe 2. Überprüfen Sie die Einträge in der Datenbank mysql in den Tabellen user und db. Entfernen Sie diesen User wieder mit einem Befehl, der alle Einträge aus diesen Tabellen entfernt.  
     DROP user mysql_user@localhost;
5.  Importieren Sie jetzt das Skript *buchverwaltung.sql* vom Betriebssystem-Konsolenfenster als user *krauthammer*.  
     mysql buchverwaltung \< buchverwaltung.sql –u krauthammer –p
6.  Überprüfen Sie das Ergebnis in der Datenbank (z. B. über phpMyAdmin). Vermutlich sind die Umlaute nicht korrekt dargestellt – siehe Tabelle *publisher*!? Löschen Sie wieder alle Tabellen und führen Sie den vorherigen Schritt nochmals aus mit der Erweiterung --default-character-set=utf8  
     mysql buchverwaltung \< buchverwaltung.sql –u krauthammer –p --default-character-set=utf8
7.  Als nächstes sollten Sie die Datei mehr_autoren.txt mit dem Befehl LOAD DATA INFILE importieren. Melden Sie sich als User krauthammer an. Vorher müssen Sie diesem User noch die Berechtigung für File_Privileges erteilen (entweder per phpMyAdmin oder als SQL-Befehl).  
     GRANT FILE ON \*.\* TO 'krauthammer'@'localhost';  
     LOAD DATA INFILE 'mehr_autoren.txt' INTO TABLE author  
     CHARACTER SET utf8 FIELDS TERMINATED BY '“' LINES TERMINATED by '\\r\\n';
8.  Überprüfen Sie das Resultat der Übung 7. Sind alle Werte eingefügt und die Umlaute korrekt dargestellt? Vermutlich müssen Sie in Ihrem LOAD DATA INFILE Befehl einen CHARACTER SET definieren und die Feld und Zeilenbegrenzungen angeben.  
     Lösung siehe oben
9.  In der Datei *noch_mehr_autoren.sql* sind einige INSERT-Statements. Die Primärschlüssel sind alle auf NULL gesetzt. Was wird bei einem Import passieren? Importieren Sie auch diese Statements so, dass die Umlaute korrekt dargestellt werden.  
     mysql buchverwaltung \< noch_mehr_autoren.sql -u krauthammer -p --default-character-set=utf8

## 4.4 Skripte erstellen und einsetzen

1.  Erstellen Sie eine Textdatei, die ein SQL-Select-Statement enthält, das Ihnen alle Einträge der Autorentabelle in eine Textdatei schreibt.  
     SELECT \* INTO OUTFILE 'ergebnis.txt' FROM autoren;  
     mysql buchverwaltung \< select_to_file.txt -u krauthammer -p
2.  Erstellen Sie ein Skript, dass in der Tabelle *author* 2 neue Spalten einfügt: *surname* und *first_name*, beide als *VARCHAR(50)*. Anschliessend sollen die Vor- und die Nachnamen aus dem bestehenden Attribut *author* mit SQL-Textfunktionen herausgefiltert werden und in die neuen Attribute abgespeichert werden. Wenn das erledigt ist, kann das ursprüngliche Attribut entfernt werden.  
     Datei kor_author.sql  
     ALTER TABLE author ADD (first_name VARCHAR(50), surname VARCHAR (50));  
     UPDATE author SET surname=SUBSTRING(author FROM 1 FOR (LOCATE (',', author))-1);  
     UPDATE author SET first_name=SUBSTRING(author,(LOCATE (',', author))+2,20);  
     ALTER TABLE author DROP COLUMN author;
3.  Erstellen Sie ein Skript, dass eine Textdatei mit den Attributen *first_name, surname, title, year, publisher* erzeugt. Die Felder sollen mit Tabulatoren getrennt werden (\\t), die Zeilen mit *line-separators* (\\r\\n) getrennt werden.  
     Datei buchliste.sql  
     SELECT first_name, surname, title, year, publisher  
     INTO OUTFILE 'buchliste.txt'FIELDS TERMINATED BY '\\t' LINES TERMINATED by '\\r\\n'  
     FROM author INNER JOIN rel_title_author  
     ON author.authorID=rel_title_author.authorID  
     INNER JOIN title ON title.titleID=rel_title_author.titleID  
     LEFT JOIN publisher ON title.publisherID=publisher.publisherID
4.  Erstellen Sie eine neue Datenbank. Sie können Sie zum Beispiel *hotel_schlecht* nennen. Importieren Sie den Dump *hotel_schlecht_basis.sql*. Jetzt haben Sie eine einzige Tabelle vollkommen unnormalisiert. Öffnen Sie die Datei *hotel_schlecht.sql* in einem Editor. Analysieren Sie die Statements und überlegen sich, was ausgeführt wird. Lassen Sie die Statements ausführen – einzeln oder insgesamt. Sie können die Statements abändern und damit herumexperimentieren. Notfalls müssen Sie halt alles löschen (DROP) und neu importieren. Einen „Back“-Button gibt es hier nicht. Erweitern Sie die Statements nach eigenen Vorstellungen um die Datenbank in Richtung 3. Normalform zu verbessern.




















# 6 Datenbank-Sicherheit

1.  Was bedeutet der Begriff Authentifizierung im Zusammenhang mit einem DB-Server?

    - [ ] Prüfung der Privilegien des Benutzers

    - [ ] Antwort auf die Frage Wer?

    - [ ] Identitätsprüfung

    - [ ] Antwort auf die Frage Was?

2.  Wann werden Änderungen im Zugriffssystem von MySQL wirksam?

    - [ ] sofort nach Eingabe der Änderung

    - [ ] nach dem Befehl FLUSH PRIVILEGES

    - [ ] nach dem Neustart des DB-Servers

    - [ ] nach dem Befehl GRANT

3.  Was bewirkt der SQL-Befehl GRANT ... ON ... TO ...;

    - [ ] Privileg(ien) erteilen

    - [ ] Privileg(ien) wegnehmen

    - [ ] User erstellen, falls noch nicht vorhanden

    - [ ] User löschen

4.  Mit welchem Befehl werden Privilegien kontrolliert?

    - [ ] REVOKE ... ON ... FROM ;

    - [ ] SELECT user, host, password FROM user ;

    - [ ] SHOW TABLES;

    - [ ] SHOW GRANTS FOR ... ;

5.  Welches sind die beiden wichtigsten DCL-Befehle (data control)?

    - [ ] SELECT

    - [ ] REVOKE

    - [ ] DELETE

    - [ ] GRANT
    
1.  Was ist nötig, dass Benutzer "meier" keinen Zugang mehr auf den DB-Server hat.

    - [ ] in Systemtabelle user für diesen Benutzer jedes Privileg auf "N" setzen

    - [ ] mit DELETE FROM user WHERE user = 'meier'; und FLUSH PRIVILEGES;

    - [ ] in allen Systemtabellen für diesen Benutzer jedes Privileg auf "N" setzen

    - [ ] dem Benutzer das GRANT-Privileg (Grant_priv) wegnehmen

1.  Erklären Sie den Begriff "Autorisierung" im Zusammenhang mit einem DB-Server.

    Bestimmt WAS auf einer Tabelle ausgeführt werden darf.   
      
    Prüft die Berechtigung des Klienten, welche SQL-Befehle Zugriff erhalten...

2.  Wann wird das Schlüsselwort IDENTIFIED BY verwendet?

    Um bei einem GRANT-Statement ein Passwort mit anzugeben (wird somit gesetzt)

3.  Ergänzen Sie den Befehl REVOKE ... ON ... FROM ... ; mit eigenen Angaben.

    REVOKE SELECT, INSERT ON db.tabelle FROM user@host   
      
    REVOKE ALL ON db.tabelle FROM \*@%

4.  Beschreiben Sie den Begriff der MySQL-Testdatenbank.

    Jeder Benutzer, der sich beim MySQL-Server anmeldet, kann Testdatenbanken   
      
    erstellen. Voraussetzung ist, dass der DB-Name mit den Buchstaben test beginnt.   
      
    Diese Datenbanken können von allen gelesen, gelöscht und auch verändert   
      
    werden; sie sind also völlig ungeschützt.

5.  Mit welchem Befehl ändern Sie das Passwort von Benutzer Meier auf "abc123"?

    SET PASSWORD FOR ’meier@%’ = Password(’abc123’);   
      
    FLUSH PRIVILEGES;

6.  Geben Sie eine Erklärung für folgende Fehlermeldung.  
      
    GRANT USAGE ON \*.\* TO abc IDENTIFIED BY 'a12';  
    ERROR 1045: Access denied for user: '@127.0.0.1'

    User ‚abc’ hat keine Zugriffsberechtigung vom eingeloggten Host aus

7.  Korrigieren Sie den folgenden Befehl:  
      
    REVOKE ALL FROM ''@localhost;  
    ERROR 1064: You have an error

    REVOKE ALL PRIVILEGES FROM ’’@localhost; // ’’ = anonymous !!!



























# 7 DB-Server im LAN

## 7.1 Aufgaben

1.  Netzverbindung zum Server-Rechner testen:

    Verifizieren Sie mit dem entsprechenden Befehl, dass der Server-Rechner vom Client-Rechner aus über das Netz erreichbar ist. Verwenden Sie zur Adressierung des Server-Rechners die vorher bestimmte IP-Adresse.

2.  Verbindung zum DB-Server testen

    Testen Sie mit dem entsprechenden Befehl die Verbindung zum DB-Server. Das Resultat *mysqld is alive* zeigt zudem, dass der DB-Server läuft.

3.  Über das Netz auf den DB-Server zugreifen

    Starten Sie auf dem Client-Rechner den MySQL-Monitor als Benutzer remote.

    Geben Sie die IP-Adresse Ihres DB-Server-Rechners an.

    Fragen Sie den Status des Servers ab und untersuchen Sie die Angaben für *Current user* und *Connection*.

    Testen Sie, ob Sie vom Client aus Zugriff auf alle Tabellen der DB morebooks haben und eine beliebige SQL-Abfrage darauf ausführen können.

4.  Backup und Restore über das Netz ausführen

    Erstellen Sie als Benutzer remote ein Backup der Datenbank morebooks in der Datei C:\\temp\\backup.sql.

    Löschen Sie mit dem Texteditor in der Backup-Datei die Passwort-Aufforderung und überschreiben Sie den ersten Autorennamen mit dem eigenen Namen.

    Spielen Sie das Backup auf den Server zurück und kontrollieren Sie mit einer SQL-Abfrage, dass der erste Autor Ihren Namen hat.

5.  Netzzugriff auf den DB-Server vorübergehend verbieten

    Tragen Sie auf dem Server-Rechner in die Konfigurationsdatei die Option ein, welche den Zugriff über das Netz verbietet. Starten Sie den Server erneut, damit der Eintrag wirksam wird.

    Versuchen Sie, vom Client aus, eine DB-Abfrage auszuführen. Dies sollte nun nicht mehr möglich sein.

    Lassen Sie auf dem Server den Netzzugriff wieder zu und prüfen Sie, dass Sie nun auf dem Client die DB-Abfrage ausführen können.

    Überlegen Sie, in welchen Fällen das temporäre Sperren des Netzzugriffs sinnvoll ist.

## 7.2 ODBC

1.  Installierte ODBC-Treiber auf dem Standard-PC bestimmen

    Suchen Sie auf dem Standard-PC den ODBC-Administrator, d.h. eine Datei *\*odbc\*.exe*.

2.  Datenquelle (DSN) erstellen und testen

    Erstellen Sie im Register *System-DSN* eine neue Datenquelle *hotel* für den von Ihnen auf dem DB-Server erstellten Benutzer *remote*. Tragen Sie dabei als Host die IP-Adresse des Server-PC ein (an Stelle von localhost).

    Testen Sie, dass der von Ihnen konfigurierte DSN funktioniert. Dies können Sie nur ausführen, falls der installierte ODBC-Treiber dies unterstützt.

3.  ODBC ausprobieren. Testen Sie die folgenden Fälle:
-   Zugriff via ODBC bei abgestelltem DB-Server
-   Eintrag einer falschen IP-Adresse im DSN
-   Eintrag eines falschen DB-Namens im DSN
-   Eintrag eines falschen Usernamens im DSN
-   Eintrag eines falschen Passworts im DSN

## 7.3 Zugriff von Access auf MySQL

1.  MySQL-Tabellen in Access einbinden
-   Starten Sie auf dem Client-PC Access und erstellen Sie eine neue Access-DB. Geben Sie ihr den Namen der DB, d.h. *hotel*.
-   Erstellen Sie in Access (im Register Tabellen) die Verknüpfungen zu einigen Tabellen der Datenbank hotel auf dem MySQL-Server. Wählen Sie dabei als Datenquelle die von Ihnen erstellte DSN *hotel* aus.
-   Testen Sie den Zugriff auf jede der Tabellen.
-   Ändern Sie via ODBC einige Einträge in den Tabellen und prüfen Sie die Änderung, indem Sie mit phpMyAdmin die Einträge kontrollieren.
1.  Access-Eingabeformulare erstellen

Erstellen Sie im Register Formulare mit dem Assistenten einfache Formulare für das Erfassen, Ändern und Löschen der Daten in den MySQL-Tabellen.

-   Testen Sie die Formulare, indem Sie damit Daten eingeben, ändern und löschen.
-   Prüfen Sie zusätzlich mit phpMyAdmin diese Änderungen.
1.  Welcher Befehl testet die Verbindung zum Server-Rechner mit Adresse 139.79.124.97?

    - [ ] mysql -h 139.79.124.97

    - [ ] ipconfig

    - [ ] ping 139.79.124.97

    - [ ] mysqladmin -h 139.79.124.97 -u root -p ping

2.  Wozu wird der Parameter -h bei MySQL verwendet?

    - [ ] bewirkt die Abfrage des Passworts

    - [ ] bewirkt die Verbindung als bestimmter Benutzer

    - [ ] Angabe der Adresse des Client-Rechners

    - [ ] Angabe der Adresse des Server-Rechners

3.  Was bewirkt der Befehl 'mysqldump -h 139.79.124.97 hotel \> datei.txt'?

    - [ ] Backup der DB hotel in die Datei datei.txt auf Adresse 139.79.124.97

    - [ ] Backup der angegebenen DB auf dem Server mit der IP-Adresse 139.79.124.97

    - [ ] Restore der Datenbank hotel auf dem Server mit der Adresse 139.79.124.97

    - [ ] Ausführen des SQL-Skripts datei.txt auf Adresse 139.79.124.97 auf die DB hotel

4.  Welche Aufgabe hat der ODBC-Driver?

    - [ ] passt die SQL-Befehle dem entsprechenden DB-Server an

    - [ ] ermöglicht das Erstellen und Konfigurieren von ODBC-Datenquellen (DSN)

    - [ ] ermöglicht den einheitlichen Zugriff einer Applikation auf verschiedene Datenbanken

    - [ ] ermöglicht den Zugriff einer Applikation auf eine bestimmte DB

5.  Wie greifen Sie vom Konsolenfenster auf einen DB-Server mit Adresse 139.79.124.97 zu?

    - [ ] mysqladmin -h 139.79.124.97

    - [ ] mysql -h 139.79.124.97 hotel \< hotel.bkp

    - [ ] mysql -h 139.79.124.97 -u root -p

    - [ ] ping 139.79.124.97

1.  Welche Aufgaben hat der DB-Server im Gegensatz zum DB-Client?

    Stellt Daten(-sätze) bereit und regelt deren Zugriffe   
      
    Bildet Datenstruktur mittels Tabellen und Beziehungen ab

2.  Weshalb benutzt man MS Access z.B. zusammen mit einem MySQL-Server?

    MS Access hat eine ausgereifte Benutzeroberfläche mit Masken/Formulare   
      
    MySQL ist eine effizienter Multi-User-Server

3.  Wie bestimmen Sie die IP-Adresse des Server-Rechners?

    ipconfig

4.  Wie prüfen Sie, ob der DB-Server auf Adresse 139.79.124.97 läuft?

    mysqladmin -h 139.79.124.97 ping –u Benutzername -p

5.  Welcher Befehl führt das SQL-Skript xy.sql auf die DB hotel auf Adresse 139.79.124.97 aus?

    mysql -h 139.79.124.97 -u remote -p hotel \< H:\\xy.sql

6.  Wozu wird ODBC verwendet?

    Stellt eine standardisierte Schnittstelle für den einheitlichen Zugriff auf verschiedene   
      
    DB-Systeme bereit

7.  Was wird benötigt, um von Access mit ODBC z.B. auf einen DB2-Server zuzugreifen?

    Ein ODBC-Treiber muss auf dem DB2-Server installiert sein   
      
    Einen neue Datenquelle (DSN=Data source Name) muss erstellt werden

8.  Was wird in der Java-Welt anstelle von ODBC verwendet?

    Java hat die Schnittstelle Java Database Connectivity JDBC   
      
    Ein sog. JDBC-ODBC-Bridge-Treiber wandelt JDBC- in ODBC-Anfragen um

9.  Was sind eingebundene Access-Tabellen?

    Das sind nicht-lokale Tabellen, die z.B. über den ODBC-Treiber eingebunden werden   
      
    Sie verhalten sich wie eigenen Tabellen, können aber strukturell nicht verändert werden   
      
    Eine Löschung löst lediglich die Verbindung






























# 8 Server-Administration

## 8.1 DB-Server konfigurieren

1.  Konfigurationsparameter anzeigen

    Führen Sie im DOS-Fenster den Befehl mysqld --help aus. Bestimmen Sie aus der Vielzahl der angezeigten Server-Parameter vier Ihnen wichtig erscheinende. Notieren Sie die gewählten Parameter mit einer kurzen Beschreibung.

2.  Sprache der Fehlermeldungen ändern

    Schauen Sie im Verzeichnis mysql\\share\\ nach, welche Sprachen unterstützt werden und tragen Sie mit einem Texteditor eine davon mit dem entsprechenden Parameter in der Konfigurationsdatei my.ini ein.

    Stoppen Sie den Server und starten Sie ihn erneut. Prüfen Sie im Monitor und im Control Center, dass Eingabefehler eine Fehlermeldung in der gewählten Sprache ergeben.

3.  Systemvariablen anzeigen

    Lassen Sie sich mit dem entsprechenden Befehl alle Systemvariablen, die etwas mit dem Logging zu tun haben anzeigen.

    Bestimmen Sie für jede Variable die Bedeutung. Benutzen Sie dazu das MySQL-Manual.

    show status; show variables; show variables like "%log%"

## 8.2 Logging

1.  Error Log benutzen

    Suchen Sie auf Ihrem Rechner das Fehlerprotokoll. Öffnen Sie es mit einem Texteditor und untersuchen und dokumentieren Sie die letzten 4 Einträge.

2.  Update Log konfigurieren

    Schalten Sie in der Konfigurationsdatei die Protokollierung in binärer Form ein (ohne Pfadangabe). Starten Sie den Server erneut, damit die Änderung wirksam wird. Ändern Sie dann in einer Tabelle der hotel-DB mit einem SQL-Befehl einen Datensatz und kontrollieren Sie den entsprechenden Eintrag in der Log-Datei. Verwenden Sie dazu das Programm mysqlbinlog.

3.  Query Log benutzen

    Schalten Sie in der Konfigurationsdatei diese Protokollierung ein.

    Führen Sie nach dem Neustart des Servers im Monitor einige Befehle aus und kontrollieren Sie die Einträge in der Log-Datei.

4.  Transaktions-Logdateien konfigurieren

    Aus Geschwindigkeitsgründen sollen die InnoDB-Logdateien vom Standardverzeichnis C:\\mysql\\data ins neue Verzeichnis D:\\log verlegt werden.

    Erstellen Sie das neue Verzeichnis und stoppen Sie den Server.

    Machen Sie den entsprechenden Konfigurationseintrag und löschen Sie die alten Logdateien.

    Starten Sie den Server und kontrollieren Sie den entsprechenden Start-Eintrag im Error-Log.

    Prüfen Sie, dass die Log-Dateien im neuen Verzeichnis erstellt wurden.

## 8.3 Backup und Restore

1.  Blockiertes Backup freigeben

    Blockieren Sie als DB-Client A im Monitor eine Tabelle der DB mybooks mit einem WRITE LOCK. Starten Sie als Client B in einem zweiten DOS-Fenster ein Backup der gleichen DB. Wie wird erreicht, dass das Backup ausgeführt wird?

2.  DB nach Crash wiederherstellen
-   Mit Hilfe des vorhandenen Backup und der Update-Log soll nach einem Totalverlust die DB wiederhergestellt werden.
-   Prüfen Sie, dass das Update-Log eingeschaltet ist; andernfalls schalten Sie es ein und starten Sie den Server erneut.
-   Erstellen Sie ein Backup der gesamten DB hotel und eröffnen Sie neue Log-Dateien mit dem SQL-Befehl FLUSH LOGS.
-   Tragen Sie in die DB je einen neuen Autor und Verlag ein.
-   Simulieren Sie den Totalverlust der DB, indem Sie den Server stoppen und das Verzeichnis der Datenbank, d.h. C:\\mysql\\data\\hotel löschen.
-   Starten Sie den Server und regenerieren Sie die DB mittels Backup und Update-Log. Bestimmen Sie dazu die Update-Logdatei, die seit dem Backup bis zum Crash entstanden ist. Nur diese ist für das Restore zu verwenden. Prüfen Sie vor dem Ausführen deren Inhalt.
-   Kontrollieren Sie, ob in der regenerierten DB die von Ihnen eingetragenen 2 Datensätze ebenfalls vorhanden sind.
1.  mysqldump –opt

    Untersuchen Sie mit *mysqldump --help* oder mit Hilfe des MySQL-Manuals, warum der Parameter *--opt* eine optimale Einstellung für Backups ergibt.

## 8.4 Repetitionsfragen

1.  Auf welche Arten können Konfigurationsparameter definiert werden?

    - [ ] mit einem INSERT-Befehl

    - [ ] durch Eintrag auf der Kommandozeile

    - [ ] durch Eintrag in einer Konfigurationsdatei

    - [ ] durch Eintrag in einem Logfile

2.  Welcher Befehl erstellt ein Backup der Datenbank kunden in die Datei kunden.sql?

    - [ ] \> mysql KUNDEN \< kunden.sql

    - [ ] \>mysqldump kunden \> kunden.sql -u username -p

    - [ ] SELECT \* FROM kunden ;

    - [ ] CREATE DATABASE kunden ;

3.  Welcher Konfigurationsparameter legt fest, wo die Log-Dateien abgelegt werden?

    - [ ] basedir

    - [ ] datadir

    - [ ] log-bin

    - [ ] logdir

4.  Mit welchem Eintrag beginnen die Server-Parameter in der Konfigurationsdatei?

    - [ ] [mysql]

    - [ ] [WinMySQLadmin]

    - [ ] [mysqldump]

    - [ ] [mysqld]

5.  Wozu kann der DB-Client mysqlshow verwendet werden?

    - [ ] Backup erstellen

    - [ ] DB-Schema anzeigen

    - [ ] Verbindung zum DB-Server testen

    - [ ] Inhalt einer Protokolldatei anschauen

6.  Mit welchem Log-File bestimmen Sie den letzten Start des MySQL-Servers?

    - [ ] Error Log

    - [ ] Update Log

    - [ ] Query Log

	 - [ ] Transaction Log
	 
1.  Welcher Eintrag im Konfigurationsfile schaltet die Protokollierung aller User-Login ein?

    - [ ] log-bin

    - [ ] log-slow-queries

    - [ ] log

    - [ ] log-error=C:/log/err.log

2.  Wie restaurieren Sie nach einem Server-Ausfall eine DB vollständig?

    - [ ] Einlesen des letzten Backup

    - [ ] Verwenden der Option --opt beim Erstellen des Backup

    - [ ] Einlesen des Query-Log

    - [ ] Einlesen aller Update-Logs in der richtigen Reihenfolge (mit Hilfe von mysqlbinlog)

3.  Wie erreichen Sie, dass Änderungen in der Konfigurationsdatei wirksam werden?

    Konfig-Datei abspeichern und Server mysqld.exe neu starten

4.  Durch welche Daten wird der von einer DB benötigte Speicherplatz bestimmt?

    Nutzdaten (Datensätze), Indizes, Systemdaten

5.  Wozu wird das Logging (Protokollierung) verwendet?

    Monitoring (Fehler), Sicherheit(Backup), Optimierung(von Abragen),   
      
    Replikation(Synchronisierung), Transaktionen(nach DB-Absturz)

6.  In welcher Log-Datei finden Sie, den Anwender, der bestimmte Daten löschte?

    Query-Log: Zeichnet jeden User und dessen Aktivitäten (Queries )auf

7.  Welche Informationen finden Sie im Slow Query Log?

    Zeichnet DB-Abfragen auf die den grössten Aufwand verursachen (\>10s)   
      
     Hinweise zur Verbesserung der Server-Performance

8.  Geben Sie für jede Protokolldatei an, wie Sie deren Inhalt kontrollieren.

    Error.log, Query.log, SlowQuery.log Texteditor   
      
    Update.log mysqlbinlog.exe; Transaktions-logs nur für den Server

9.  Wie beeinflusst der Parameter --opt beim Erstellen eines Backup das Tabellenlocking?

    Beim Erstellen der Insert-Befehle im Backup-Script werden diese mit Lock-Befehlen   
      
    „eingepackt“ und die Tabellen werden fürs Lesen gesperrt (--lock-tables)

10. Beschreiben Sie das Vorgehen, um Daten von MySQL nach ORACLE zu migrieren.

    Tabellen aus MySQL exportieren mit SELECT \* INTO OUTFILE und in ORACLE   
      
    importieren; Auf Delimiter, Datumsformate und Kollation (Zeichenformate) achten

11. Beschreiben Sie eine praktische Anwendung für den READ-Lock.

    Gewährleistet die Integrität der Datenbank beim Erstellen eines Backups



























# 9 Optimierung

1.  Welche Möglichkeiten können die Geschwindigkeit eines DB-Server verbessern?

    - [ ] Indexe möglichst vermeiden

    - [ ] Serverparameter einstellen

    - [ ] Transaktionen verwenden

    - [ ] Locks verwenden

2.  Wie werden Daten schneller in eine DB-Tabelle geladen?

    - [ ] durch Komprimieren der Daten vor der Übertragung

    - [ ] durch Verwenden des Parameters --opt beim Erstellen des Backup-Skripts

    - [ ] durch Importieren der Daten aus einer Textdatei

    - [ ] durch Verwenden von vielen INSERT-Befehlen

3.  Was trifft auf den Befehl OPTIMIZE TABLE zu?

    - [ ] entfernt nicht genutzten Speicherplatz aus MyISAM-Tabellendateien

    - [ ] ist auf MyISAM- und InnoDB-Tabellen anwendbar

    - [ ] wird angewendet bei Tabellen, die häufig abgefragt werden

    - [ ] defragmentiert DB-Dateien

4.  Wie finden Sie langsame DB-Abfragen?

    - [ ] mit EXPLAIN SELECT

    - [ ] im Query Log

    - [ ] im Slow Query Log

    - [ ] im Error Log

5.  Welche Aussagen betreffend DB-Optimierung sind korrekt?

    - [ ] Abfragen, die LIKE enthalten, können immer optimiert werden

    - [ ] Indexe beschleunigen Abfragen

    - [ ] Indexe werden allgemein auf Schlüsselattribute gelegt

    - [ ] durch Indexe werden DB-Einträge und -änderungen schneller
    
1.  Wann verwenden Sie den Befehl EXPLAIN?

    - [ ] um Daten schneller in die DB zu laden

    - [ ] immer im Zusammenhang mit SELECT

    - [ ] um langsame Abfragen zu finden

    - [ ] um zu erkennen, wie sich ein Index auf die Geschwindigkeit einer Abfrage auswirkt

1.  Welches sind Gründe für die Verwendung eines Index?

    - [ ] um das Eintragen von Daten in Tabellen bei Unique-Attributen zu beschleunigen

    - [ ] um DB-Abfragen zu beschleunigen

    - [ ] um das Ändern von Daten zu verlangsamen

    - [ ] um einmalige Werte zu gewährleisten
    
1.  Nennen Sie Ziele der DB-Optimierung?

    Performance verbessern, Speicherplatz einsparen   
      
    Portabilität ermöglichen

2.  Was wird optimiert, um die Geschwindigkeit eines DB-Servers zu verbessern?

    Such-, Schreib- und Lesevorgänge: Daten auf mehrere Festplatten verteilen   
      
    (Optimierung von SQL-Scripts und Speicherplatz)

3.  Mit welchen 2 prinzipiellen Massnahmen werden DB-Abfragen beschleunigt?

    Parameter --opt, LOAD DATA INFILE: schnelleres Ausführen von SQL-Scripts   
      
    OPTIMIZE TABLE: entfernt nicht genutzen Tabellenspeicherplatz, Indizes

4.  Beschreiben Sie kurz, wie Sie den Befehl EXPLAIN verwenden.

    Der Präfix vor SELECT erklärt wie dieser Befehl ausgeführt werden würde   
      
     Gibt Auskunft wie die Tabellen verknüpft werden zeigt wo Indizes   
      
    eingefügt werden sollten

5.  Wozu wird der Befehl OPTIMIZE TABLE angewendet?

    Entfernt nicht genutzen Tabellenspeicherplatz, der durch Mutationen entstand

6.  Wie werden SELECT-Befehle optimiert?

    Indem Indexe auf häufig verwendete Attribute und auf Schlüssel erstellt werden

7.  Wie viele DB-Tabellen können standardmässig gleichzeitig geöffnet sein?

    table_cache = 64

8.  Wie schalten Sie den Query Cache ein bzw. aus?

    „query_cache_size = XXM“ in die Konfigutrationsdatei einfügen   
      
    query_cache_type = 0 oder 1;
