![](../x_res/tbz_logo.png)

# M141 - DB-Systeme in Betrieb nehmen  (3.Tag)

*Autoren: Gerd Gesell ![](../x_res/bee.png) (Kellenberger Michael) 2024*

**Inhalt**

[TOC]

> ([Lösung 2.Tag](../Loesungen/2T_CP_Loes.md))


#  Tabellentypen und Transaktionen

![](../x_res/Learn.png)

Die Entwickler von MySQL haben sich lange geweigert Mechanismen einzubauen, die nur dem Komfort der Entwickler dienen und sich auch auf andere Weise lösen liessen. Dazu gehört der Einbau der referentiellen Integrität genauso wie die Möglichkeit Transaktionen zu verwenden. Das macht den Standardtabellentyp von MySQL zwar schnell und schlank, ist für einige Anwendungen aber nicht optimal. Seit Mai 2000 ist der Tabellentyp **BDB** (Berkley Database – Key/Value-DB) integriert, der auch Transaktionen unterstützt. Mittlerweile ist das Repertoire noch um **InnoDB** und **Gemini** erweitert worden, die ebenfalls transaktionstauglich sind. Damit sind jetzt auch Techniken möglich, die jedes professionelle relationale Datenbanksystem einfach integriert haben muss.


## Tabellentypen 

Ob **Transaktionen** (siehe weiter unten) und **referentielle Integrität** (*Checkt die Richtigkeit einer Relation*) verwendet werden können, hängt also vom verwendeten Tabellentyp ab. Der Tabellentyp von MySQL ist **MyISAM** und kennt *keine* Transaktionsunterstützung. Die drei oben genannten schon!


Sie können innerhalb einer Datenbank Tabellen *unterschiedlicher Typen* verwenden und so das Tabellenformat Ihren Anforderungen anpassen. Dieses Konzept ist schlau, da Transaktionen nicht für jede Anwendung benötigt werden. Sicherheit und Komfort gehen natürlich zu Lasten von Geschwindigkeit. 

Es gibt verschiedene Möglichkeiten, Transaktionen zu verwalten. Die Tabellentreiber (Engines) der BDB-, InnoDB- und Gemini-Tabellen verwenden jeweils unterschiedliche Transaktionsmodelle. Die Unterschiede betreffen in erster Linie die Art und Weise, **wie Datensätze während einer Transaktion vor Änderungen durch andere Anwender geschützt werden**.


|               | **MyISAM** <br> oder **Aria** bei MariaDB                                                                                                       | InnoDB                                                                                                                                                                        |
|---------------|-------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Eigenschaften | - stabil und schnell <br> - braucht wenig Speicherplatz  <br> - unterstützt Table locking                                                            | - unterstützt Transaktionen und referentielle Integrität  <br> - braucht mehr Speicherplatz  <br> - unterstützt **Row Level locking**                                                                |
| Einsatz       | - wenn Geschwindigkeit wichtig  <br> - bei vielen Daten  <br> - für effiziente Verwaltung                                                             | - wenn Sicherheit wichtig ist  <br> - wenn viele Anwender gleichzeitig Daten ändern                                                                                                     |
| Speicherung   | Jede **MyISAM**-Tabelle wird in 3 Tabellen gespeichert:  <br> - `*.FRM` (Tabellenbeschreibung),  <br> - `*.MYD`  oder `MAD` (Daten)  <br> - `*.MYI`  oder `MAI` (Indexe) | -  `*.FRM`-Datei für jede Tabelle  <br> - Daten in einer gemeinsamen Datei - der **Tablespace**-Datei  <br> - Bei `innodb_file_per_table=1` wird zusätzlich ein Tablespace pro Tabelle angelegt (`*.ibd`) |

Tab. 5.1: Die beiden wichtigsten Tabellentypen von MySQL: MyISAM und InnoDB im Vergleich.

Die Beschreibung jeder **MyISAM**-Tabelle wird in einer `*.FRM`-Datei gespeichert, die in einem Verzeichnis liegt, die dem Namen der DB entspricht. (Und wo sich dieses Verzeichnis befindet ist in der Optionsdatei `my.ini` definiert.)

**InnoDB**-Tabellen werden nicht wie alle anderen Tabellentypen in einfachen Dateien gespeichert. Stattdessen dient eine zentrale Datei, die als **Tablespace** bezeichnet wird, als **virtueller Speicher** für alle `InnoDB`-Tabellen und -Indizes. 

###  InnoDB-Tabellen müssen in der Optionsdatei freigeschaltet sein

Die Standareinstellungen in der `my.ini`-Optionsdatei sieht so aus:

```
### skip-innodb.                                 <<<< muss mit # auskommentiert sein!

innodb_data_home_dir="C:/xampp/mysql/data"    

innodb_data_file_path = ibdata1:10M:autoextend   <<<< Einstellung TABLESPACE

innodb_log_group_home_dir="C:/xampp/mysql/data"
#innodb_log_arch_dir = "C:/xampp/mysql/data"
## You can set .._buffer_pool_size up to 50 - 80 %
## of RAM but beware of setting memory usage too high
innodb_buffer_pool_size=16M
## Set .._log_file_size to 25 % of buffer pool size
innodb_log_file_size=5M
innodb_log_buffer_size=8M
innodb_flush_log_at_trx_commit=1
innodb_lock_wait_timeout=50

innodb_table_locks = 1                           <<<< Standard
```



### Tabellenformat InnoDB und die Tablespace

![](../x_res/Train_R1.png)

![](../x_res/ToDo.png) [Datenpool](../Daten)

- **InnoDB-Tabelle erzeugen**: Um eine InnoDB-Tabelle zu erstellen, wird dies im `CREATE TABLE`-Befehl angegeben. Im phpMyAdmin können Sie diese Angabe in einem Auswahlfenster angeben. Das folgende Beispiel erstellt eine neue DB und die InnoDB-Tabelle *Konten*:

```sql
mysql> CREATE DATABASE innotest;  
mysql> USE innotest;
mysql> CREATE TABLE tbl_konto (id_k INT AUTO_INCREMENT, Name VARCHAR(30), Saldo DECIMAL(10,2), PRIMARY KEY (id_k)) ENGINE = InnoDB;
```
<br><br>


-   **Tabellentyp ändern**: Sie können auch bestehende Tabellen nachträglich in InnoDB-Tabellen umwandeln. Das geht entweder mit dem SQL-Befehl `ALTER TABLE ...` oder unter `Operationen` im phpMyAdmin.

```sql
mysql> ALTER TABLE tblname ENGINE = InnoDB;
```

> ![Achtung](../x_res/caution.png) HINWEIS: Ändern Sie auf keinen Fall den Tabellentyp der Tabellen der Datenbank *mysql*! Diese Tabellen mit MySQL-internen Verwaltungsinformationen (Benutzer- und Zugriffsrechte) müssen immer im MyISAM-Format bleiben!

DB [hotel](../Daten/hotel.sql):

-   Ändern Sie in der Datenbank `hotel` den Typ der Tabelle `benutzer` zu `InnoDB`.

-   Lassen Sie sich den Typ jeder Tabelle anzeigen. Kontrollieren Sie, dass die Tabelle `benutzer ` das Format `InnoDB` hat. 
-   Geben Sie Werte ein und kontrollieren Sie die Veränderungen in Ihrem `data`-Verzeichnis und in dem Verzeichnis der Datenbank `hotel`. 
-   Kontrollieren Sie die Dateien in der Dateiordnerstruktur des Datenbank-Verzeichnisses. 
-   Studieren Sie als Ergänzung die folgenden Themen im [MySQL-Manual](https://mariadb.com/kb/en/innodb/).

### Tablespace

Standardmässig wird beim ersten Start von MySQL die Datei `ibdata1` mit einer Grösse von 10 MByte (oder 12MB je nach Version) erzeugt. Sobald die darin gespeicherten InnoDB-Tabellen mehr Platz beanspruchen, wird die Datei in 8-MByte-Schritten vergrössert (*autoextend*, Siehe `Einstellung` oben). Zusätzlich werden auch die Logging-Dateien `ib_logfile0`, `-1` und `ib_arch_log_000000000` erzeugt. Alle vier Dateien liegen im binären Format vor und dürfen nicht verändert werden.

![Tablespace](../x_res/Tablespace.png)

Abb. 15: Tablespace als Schicht zwischen DB-Tabellen und DB-Dateien

Wenn Sie feststellen möchten, wie viel Speicherplatz innerhalb des *Tablespace* noch frei ist, führen Sie folgenden SQL-Befehl aus:

`SELECT SPACE,NAME,ROUND((ALLOCATED_SIZE/1024/1024), 2) as "Tablespace Size (MB)"  FROM information_schema.INNODB_SYS_TABLESPACES ORDER BY 3 DESC;`

Der freie Platz (insgesamt und für alle InnoDB-Tabellen einzeln), wird dann in der dritten Spalte (ALLOCATED_SIZE) angezeigt.

Mit der Workbench lässt sich die Tablespace via *Dashboard* visuell analysieren:

![Workbench Dashboard TableSpace](../x_res/Dashboard.png)

> ![Hinweis](../x_res/Hinweis.png) Eine Verkleinerung des Tablespace ist nicht vorgesehen. Der einzige Weg besteht darin, die Tabellen mit `mysqldump` und dem Parameter `--all-databases` zu exportieren, einen verkleinerten Tablespace neu zu erzeugen und die Tabellen dorthin wieder zu importieren.

---


## Transaktionen

![](../x_res/Learn.png)

Mit Transaktionen können **mehrere SQL-Anweisungen in einem Block gekapselt** werden, um diesen Block dann "sicher" auszuführen. Sicher bedeutet hier „**ganz oder gar nicht**“. Transaktionen erhöhen in vielen Fällen die *Sicherheit und Konsistenz* der Daten, erleichtern die Programmierung und erhöhen eventuell auch die Effizienz von Anwendungen. Wir verwenden im Folgenden den Tabellentyp `InnoDB`.

### Wozu Transaktionen?

Dieser Abschnitt erklärt, warum Transaktionen wichtig sind und wie sie unter MySQL eingesetzt werden. Transaktionen stellen sicher, dass eine Gruppe von SQL-Kommandos, die mit `BEGIN` beginnt und mit `COMMIT` endet, entweder vollständig oder gar nicht ausgeführt wird!

**(1) Transaktionen stellen sicher, dass die Daten nicht quasi gleichzeitig von anderen Anwendern verändert werden.**  

**Beispiel:** Eine derartige Gruppierung mehrerer Kommandos ist beispielsweise dann notwendig, wenn Sie einen Betrag von Konto 1 auf Konto 2 umbuchen möchten. Es darf nicht passieren, dass die Abbuchung von Konto 1 durchgeführt wird, nicht aber die Buchung auf Konto 2. Nehmen Sie also an, Sie müssen CHF 1000.- von Herrn *Vontobel* an Herrn *Nachhut* überweisen.

Die Ausgangslage DB innotest: (Siehe oben oder [Datenpool](../Daten/innotest.sql))

```sql
USE innotest;
INSERT INTO tbl_konto (id_K, Name, Saldo) VALUES (1,'Von', 10000), (2,'Nach',0);
```

Die Saldi zweier Konten sind betroffen und müssen nacheinander bearbeitet werden. 

```sql
UPDATE tbl_konto SET Saldo = Saldo - 1000 WHERE name = 'Von';
```

Nach der Abbuchung die Buchung auf das zweite Konto. Dazwischen könnte viel geschehen ...

```sql
UPDATE tbl_konto SET Saldo = Saldo + 1000 WHERE name = 'Nach';
```

... Ein anderer Client könnte genau zwischen diesen beiden Befehlen ebenfalls abbuchen:

```sql
UPDATE tbl_konto SET Saldo = Saldo - 500 WHERE name = 'Von';
```

Die Folge hier wäre nicht so schlimm, "nur" ne Saldo-Überziehung. Schlimmer wäre es mit Produkten, die eben nicht 2x verkauft werden können!

> ![Achtung](../x_res/caution.png) Wenn eine Änderungen Fremdschlüssel betrifft, dann könnte es sein, dass ein andersweitiger geänderter Fremdschlüssel auf einen unterdessen gelöschten Datensatz verweist! (=> Inkonsistenz!)
 
**(2) Wenn während der Operation der Strom ausfällt oder der Rechner abstürzt, kann es nicht passieren, dass nur ein Teil der Kommandos ausgeführt wird.**

Transaktionen erhöhen schliesslich auch den Programmierkomfort. Eine Transaktion kann jederzeit abgebrochen werden, was die Fehlerabsicherung sehr vereinfacht. Wenn also nach mehreren SQL-Kommandos ein Fehler auftritt, können Sie alle bisher im Rahmen einer Transaktion durchgeführten Kommandos durch ein einfaches `ROLLBACK` widerrufen.

Ob Sie nun tatsächlich Transaktionen benötigen oder nicht, hängt sehr stark davon ab, welche Anwendungen Sie mit MySQL entwickeln. Ein einfaches Diskussionsforum im Internet funktioniert auch ohne Transaktionen zuverlässig. Wenn Sie dagegen ein Buchhaltungsprogramm, eine komplexe Lagerverwaltung oder ähnliche Anwendungen entwickeln oder wenn die von Ihnen verwalteten Daten relativ sensitiv sind - also etwa medizinische oder finanzielle Daten -, dann sind Transaktionen und eine entsprechende Programmierung unabdingbar. Das lange Fehlen von Transaktionen in MySQL ist ein Grund dafür, warum MySQL für derartige Anwendungen bis jetzt kaum zum Einsatz kam.

### BEGIN, COMMIT und ROLLBACK

Indem Sie eine Tabelle vom Typ BDB, InnoDB oder Gemini erzeugen, haben Sie noch nichts gewonnen. Damit Sie den Vorteil von Transaktionen nutzen können, müssen Sie diese mit `BEGIN` (oder `START TRANSACTION`) einleiten und mit `COMMIT;` beenden. `COMMIT;` führt alle seit `BEGIN` angegebenen SQL-Kommandos tatsächlich aus.

Obig beschriebenes Problem könnte also so realisiert werden:


```sql
SET @uebertrag_var = 1000;   -- Übertrag in User-Var speichern

BEGIN;           -- oder START TRANSACTION

  -- Überprüfung:
  SELECT IF(Saldo >= @uebertrag_var, @uebertrag_var, 0)  INTO  @uebertrag_var
    FROM   tbl_konto WHERE  name = 'Von';  -- Schauen ob Saldo genug hoch ist, sonst Übertrag nullen.

  -- Übertrag:
  UPDATE tbl_konto SET Saldo = Saldo - @uebertrag_var WHERE name = 'Von';  
  UPDATE tbl_konto SET Saldo = Saldo + @uebertrag_var WHERE name = 'Nach';
  
COMMIT; --- oder ROLLBACK;
```

Statt `COMMIT` auszuführen, können Sie die gesamte Transaktion aber auch mit `ROLLBACK` (manuell) widerrufen. `ROLLBACK` wird *automatisch ausgeführt*, falls es zu einem *Verbindungsabbruch* kommt.


> ![Hinweis](../x_res/Hinweis.png) Wie weit offene Transaktionen den *Lesezugriff* auf eine Tabelle und deren Veränderung durch andere Clients blockieren, hängt von der Implementierung der Transaktionsunterstützung ab. (Siehe weiter unten *)


### Der Autocommit-Modus

MySQL befindet sich normalerweise im Autocommit-Modus (`AUTOCOMMIT=1`). Alle SQL-Kommandos, die nicht durch ein vorheriges `BEGIN` als Transaktion gekennzeichnet sind, werden daher sofort ausgeführt.

Wenn Sie das SQL-Kommando `SET AUTOCOMMIT=0` ausführen, wird der Autocommit-Modus ausgeschaltet. Das bedeutet, dass alle SQL-Kommandos als eine grosse Transaktion gelten. Die Transaktion wird durch `COMMIT` oder `ROLLBACK` abgeschlossen. Damit beginnt automatisch eine neue Transaktion. Sie müssen also kein `BEGIN` mehr ausführen. Allein diese Bequemlichkeit ist oft Grund genug, um mit `SET AUTOCOMMIT=0` zu arbeiten.

Eine wichtige Konsequenz von `AUTOCOMMIT=0` besteht darin, dass bei einem Verbindungsabbruch - egal ob er beabsichtigt oder unbeabsichtigt eingetreten ist - alle SQL-Kommandos, die nicht durch `COMMIT` bestätigt sind, widerrufen werden.

Beachten Sie auch, dass durch `AUTOCOMMIT=0` sehr lange Transaktionen entstehen, wenn Sie nicht regelmässig `COMMIT` oder `ROLLBACK` ausführen. Manche Tabellentreiber kommen mit derart langen Transaktionen schlecht zurecht, das heisst es können Locking-Probleme auftreten, die die Effizienz beeinträchtigen. Aufgrund seiner Architektur kommt der InnoDB-Tabellentreiber mit derartigen Transaktionen überdurchschnittlich gut zurecht.

### Locking

Ohne Transaktionen können Sie einen Schutz gegen oben beschriebene Probleme nur durch `LOCK`-Kommandos erreichen. Damit wird aber kurzzeitig eine *ganze Tabelle* für alle Clients blockiert, was sehr ineffizient ist. Bei den transaktionstauglichen Tabellentypen werden *nur die Datensätze blockiert, die tatsächlich verändert werden* sollen. 

Bei **MyISAM**-Tabellen kann nur die gesamte Tabelle durch `LOCK` geschützt werden. Bei **BDB**-Tabellen werden während einer Transaktion automatisch (also ohne ein explizites `LOCK`-Kommando) alle erforderlichen Datensätze gesperrt. Intern werden dabei allerdings ganze Speicherseiten gesperrt, weshalb es passieren kann, dass einige in der Nähe gespeicherte Datensätze während der Transaktion ebenfalls nicht mehr zugänglich sind.

Auch bei **InnoDB**- und **Gemini**-Tabellen werden die Datensätze **automatisch** gesperrt. Allerdings ist das Locking hier durch zusätzliche SQL-Kommandos genauer steuerbar als bei **BDB**-Tabellen. Ein weiterer Vorteil besteht darin, dass hier wirklich nur die betroffenen Datensätze gesperrt werden. Diese intelligenteste Form des Locking hat insbesondere den Vorteil, dass andere MySQL-Anwender so wenig wie möglich behindert werden.

Alle drei Tabellentreiber erkennen im Regelfall automatisch **Deadlock**-Situationen, bei denen einzelne Prozesse endlos aufeinander warten. In solchen Fällen wird bei dem Prozess, der den Deadlock ausgelöst hat, automatisch ein `ROLLBACK` (siehe weiter unten) ausgeführt.

> \*) Ein Klient kann durch eine offen gehaltene Transaktion einen anhaltenden Ausschluss erzwingen. In solchen Situationen hilft der Befehl `SHOW ENGINE INNODB STATUS;` <br> ![](../x_res/Deadlock.png) <br> Dann muss der Klient (Prozess) gelöscht (`kill`) oder die Transaktion beendet werden. Um dies automatisch einzuleiten müsste die Einstellung `innodb_rollback_on_timeout` auf `ON` gesetzt werden. (Check: `SHWO VARIABLES LIKE innodb_rollback_on_timeout;`)

Als zusätzliches Feature versprechen sowohl InnoDB als auch Gemini ein automatisches **Crash-Recovery**. Das bedeutet, dass alle Tabellen beim nächsten Start nach einem Stromausfall automatisch wiederhergestellt werden, wobei die Software alle bis zum Stromausfall vollständig ausgeführten Transaktionen berücksichtigt.

| Locking-Mechanismen |                                    |
|---------------------|------------------------------------|
| MyISAM              | **Table**-Locking                      |
| BDB                 | **Page-Level**-Locking - Speicherseite |
| Gemini              | **Page-Level**-Locking – Speicherseite      |
| InnoDB              | **Row-Level**-Locking - Datensatz      |

### Locking-Verhalten von InnoDB

**(1) Auto locking:** Der InnoDB-Tabellentreiber kümmert sich selbständig um alle notwendigen Locking-Operationen. ![Hinweis:](../x_res/Hinweis.png) Beachten Sie also, dass Sie das Kommando `LOCK TABLE` nicht ausführen sollten! Alle durch `INSERT`, `UPDATE`, `DELETE` veränderten Datensätze werden *automatisch* bis zum Ende der *Transaktion* durch einen so genannten **Exclusive Lock** gesperrt. Das heisst: Andere Transaktionen können diese Datensätze nicht verändern.

**Eine Besonderheit von InnoDB besteht darin, dass gewöhnliche `SELECT`-Kommandos trotz gesperrter Datensätze immer sofort ausgeführt werden.** Allerdings ignorieren die zurückgegebenen Resultate noch laufende Transaktionen anderer Klienten, liefern also eventuell veraltete Daten (Siehe weiter unten im Kasten "Transaktionsbeispiel": Verbindung B zum Zeitpunkt 1). 

**(2) LOCK TABLE[S]:** MySQL/MariaDB ermöglicht es den Klienten (bei MyISAM-Tabellen), explizit Tabellen zu sperren, um mit anderen Sitzungen für den Zugriff auf Tabellen zusammenzuarbeiten oder um zu verhindern, dass andere Sitzungen Tabellen in Zeiten ändern, in denen eine Sitzung exklusiven Zugriff auf sie benötigt. Sperren können verwendet werden, um Transaktionen zu emulieren oder um eine *höhere Geschwindigkeit* bei der Aktualisierung von Tabellen zu erreichen. **UNLOCK-TABLE** hebt die Sperrung wieder auf. [Siehe Manual](https://mariadb.com/kb/en/lock-tables/)

![Hinweis:](../x_res/Hinweis.png) **LOCK TABLES** funktioniert bei InnoDB-Tabellen nur, wenn die Systemvariable `innodb_table_locks` auf `1` (Standard) und `autocommit` auf `0` (`1` ist Standard) gesetzt ist. Bitte beachten Sie, dass bei LOCK TABLES mit `innodb_table_locks = 0` keine Fehlermeldung ausgegeben wird.

**(3) SELECT ...FOR UPDATE:** Wenn dieses Default-Verhalten für Ihre Anwendung nicht optimal ist, bieten zwei Erweiterungen des `SELECT`-Kommandos die Möglichkeit, das Locking-Verhalten gezielt zu steuern: Zum einen können Sie einzelne Datensätze auch ohne eine tatsächliche Veränderung exklusiv sperren. Das empfiehlt sich, wenn Sie die Datensätze zuerst lesen und anschliessend ändern möchten. Dazu verwenden Sie das `SELECT`-Kommando mit der Erweiterung `FOR UPDATE`:

```sql
BEGIN
SELECT * FROM table WHERE x>10 FOR UPDATE
-- die ausgewählten Datensätze
-- sind jetzt exklusiv gesperrt
COMMIT; -- oder ROLLBACK;
```

**(4) SELECT ... LOCK IN SHARE MODE:** Sie können auch mit `SELECT ... LOCK IN SHARE MODE` explizit darauf warten, bis alle noch offenen Transaktionen abgeschlossen sind - genau genommen wartet `SELECT`, bis alle anderen **Exclusive Locks** auf den Datensatz aufgelöst sind.

Gleichzeitig werden die von `SELECT` gefundenen Datensätze nun mit einem so genannten **Shared Lock** gesperrt. Ein Shared Lock ist nicht ganz so stark wie ein Exclusive Lock. Die Daten können von anderen Anwendern ebenfalls nicht verändert werden, lassen sich aber mit `SELECT ... LOCK IN SHARE MODE` lesen.

```sql
BEGIN

SELECT * FROM table WHERE x>10 LOCK IN SHARE MODE
-- die ausgewählten Datensätze sind
-- jetzt durch shared locks gesperrt

COMMIT; -- oder ROLLBACK;
```

`SELECT ...LOCK IN SHARE MODE` sollte dann eingesetzt werden, wenn Sie die Datensätze zwar selbst nicht verändern, aber sicher sein möchten, dass die Daten auch von anderen Anwendern nicht geändert werden.

Die maximale Wartezeit auf die Freigabe gesperrter Datensätze wird in der MySQL-Konfigurationsdatei durch `set-variable=innodb_lock_wait_timeout=n` eingestellt (in Sekunden). Verstreicht diese Zeit, wird die gesamte Transaktion durch `ROLLBACK` abgebrochen.


### Nachteile von Transaktionen

Normale Tabellenoperationen sind in Tabellen mit Transaktionsunterstützung im Regelfall langsamer, weil der Verwaltungsaufwand grösser ist. Genau das ist auch der Grund, warum sich die MySQL-Entwickler lange geweigert haben, Transaktionen zu unterstützen.

Transaktionen stellen schliesslich grössere Anforderungen an den Programmierer: Zum einen muss er sich mit dem Transaktionsmodell des jeweiligen Tabellentreibers auseinandersetzen und die SQL-Kommandos zur Steuerung kennen und verstehen lernen. Zum anderen ist es nötig, den Ablauf besser als bisher abzusichern: Aufgrund von Transaktionen kann es dazu kommen, dass SQL-Kommandos momentan nicht ausgeführt werden können, weil andere Anwender die Daten gerade ändern. Wenn es dabei zu Verzögerungen kommt, wird Ihr SQL-Kommando nach einer bestimmten Zeit abgebrochen.

## Transaktionen ausprobieren

![](../x_res/Train_R1.png)

![](../x_res/ToDo.png) Das Beispiel geht davon aus, dass es in der Datenbank *test* die **InnoDB**-Tabelle *table1* gibt. In dieser Tabelle muss sich mindestens ein Datensatz befinden. Natürlich werden Sie in der Praxis mehr Datensätze haben, aber zur Demonstration des Mechanismus reicht ein Datensatz aus.


```sql
USE test;
CREATE TABLE table1 (colA INT AUTO_INCREMENT, colB INT, PRIMARY KEY (colA)) ENGINE=InnoDB;
INSERT INTO table1 (colB) VALUES (10);
```

Um Transaktionen ausprobieren zu können, müssen Sie **zwei** Verbindungen zur Datenbank `test` herstellen. Am einfachsten führen Sie dazu den Monitor `mysql.exe` in zwei Fenstern aus. Anschliessend führen Sie die im Kasten "Transaktionsbeispiel" dargestellten Kommandos zeitlich abgestimmt mal in dem einen, mal im anderen Fenster aus.

Zum **Zeitpunkt 1** sieht der Inhalt von *table1* aus Sicht der beiden Verbindungen unterschiedlich aus. Für Verbindung A gilt für den Datensatz mit *colA=1* bereits *colB=11*. Da diese Transaktion T1 aber noch nicht tatsächlich ausgeführt ist, sieht Verbindung B für denselben Datensatz *colB=10*.

Zum **Zeitpunkt 2** beginnt B eine Transaktion T2; *colB* des Datensatzes mit *colA=1* soll um drei vergrössert werden. Der InnoDB-Tabellentreiber erkennt, dass er dieses Kommando zur Zeit nicht ausführen kann, und blockiert B, die jetzt darauf wartet, dass A die Transaktion beendet.

Zum **Zeitpunkt 3** schliesst A die Transaktion T1 mit `COMMIT`. Damit enthält *colB* definitiv den Wert 11. Jetzt kann auch das `UPDATE`-Kommando von B abgeschlossen werden.

Zum **Zeitpunkt 4** sieht A *colB=11*. Für B sieht es so aus, als hätte *colB* bereits den Wert *14*. Nun widerruft B die Transaktion T2.

Damit sehen A und B zum **Zeitpunkt 5** beide den tatsächlich gespeicherten Wert *colB=11*.

![](../x_res/Transaktion.png)

Abb. 16: Ablauf Transaktion


---

## ACID

![](../x_res/Learn.png)

Die Abkürzung **ACID** steht für die notwendigen Eigenschaften, um Transaktionen auf Datenbankmanagementsystemen (DBMS) einsetzen zu können. Es steht für **A**tomarität (atomicity), **K**onsistenz (consistency), **I**soliertheit (isolation) und **D**auerhaftigkeit (durability). Im Deutschen spricht man gelegentlich auch von AKID.

### Atomarität

Ein Block von SQL-Anweisungen (eine oder mehrere) wird entweder ganz oder gar nicht ausgeführt: Das Datenbanksystem behandelt diese Anweisungen wie eine einzelne, unteilbare (daher atomare) Anweisungen. Gibt es bei einem Statement technische Probleme oder tritt ein definierter Zustand ein, der einen Abbruch erfordert (z. B. Kontoüberziehung), werden auch die bereits durchgeführten Operationen nicht auf der Datenbank wirksam.

### Konsistenz

Nach Abschluss der Transaktion befindet sich die Datenbank in einem konsistenten Zustand. Das bedeutet, dass Widerspruchsfreiheit der Daten gewährleistet sein muss. Das gilt für alle definierten Integritätsbedingungen genauso, wie für die Schlüssel- und Fremdschlüsselverknüpfungen.

### Isoliertheit

Die Ausführungen verschiedener Datenbankmanipulationen dürfen sich nicht gegenseitig beeinflussen. Mittels Sperrkonzepten oder Timestamps wird sichergestellt, dass durch eine Transaktion verwendete Daten nicht vor Beendigung dieser Transaktion verändert werden. Die Sperrungen müssen so kurz und begrenzt wie möglich gehalten werden, da sie die Performance der anderen Operationen beeinflusst.

### Dauerhaftigkeit

Nach Abschluss einer Transaktion müssen die Manipulationen dauerhaft in der Datenbank gespeichert sein. Der Pufferpool speichert häufig verwendete Teile einer Datenbank im Arbeitsspeicher. Diese müssen aber nach Abschluss auf der Festplatte gespeichert werden.

## Locking ausprobieren

![](../x_res/Train_D1.png)

![](../x_res/ToDo.png) Probieren Sie die oben beschrieben **Lock**-Mechanismen mit ebenfalls zwei Konsolen `user1` und `user2` aus. Erstellen Sie dazu kleine SQL-Scripte, z.B. auf die DB `hotel`:

-  Stellen Sie zuerst die zu benutzenden Tabellen in `hotel` auf **InnoDB** um!

-  Einfaches **LOCK**ing: Verhalten SELECT und UPDATE durch User2


-  Locking **SELECT ... FOR UPDATE**: Verhalten SELECT und UPDATE durch User2


-  **SELECT ... LOCK IN SHARE MODE**: Verhalten SELECT und UPDATE durch User2


<br><br><br>



---



# ![](../x_res/CP.png) Checkpoints

[Checkpoint](./3T_CheckPoint.md)

[Lösung Demo Transaktion und Lockings](./Demo_Transaktionen.sql)

---

## Literatur und Linkverzeichnis

![](../x_res/Buch.png)

[Verzeichnis](../Literatur.md)