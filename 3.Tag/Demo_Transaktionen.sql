-- Transaktionsbeispiel #2
-- -----------------------


-- >cd c:\XAMPP\mysql\bin
-- Client a und B starten:
-- >mysql -u root -p



CREATE DATABASE transaktion2;

USE transaktion2;
DROP TABLE table1;

CREATE TABLE table1 (
 ID_t1 INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 data INT NOT NULL 
) ENGINE= InnoDB;

INSERT INTO table1 (ID_t1, data) VALUES (1, 10);
INSERT INTO table1 (data) VALUES (20);



-- Demo Transaktion Locking
-- ------------------------

-- in Client A (& B):
use transaktion2; 

SHOW VARIABLES LIKE 'autocom%'; -- Zeigt gesetzte Systemwerte für AUTOCOMMIT an
SET AUTOCOMMIT=1; -- BEGIN für Transaktionen notwendig
SHOW VARIABLES LIKE 'innodb_lock%'; -- Zeigt gesetzte Systemwerte für InnoDB-Locking an
SET innodb_lock_wait_timeout = 50; -- = Standard in My.ini (kann hier verkürzt werden für Demo!)

SELECT * FROM table1; 

-- Client A mit Transaktion:
BEGIN; 
UPDATE table1 SET data=21 WHERE ID_t1=2; -- DS wird gesperrt
SELECT * FROM table1; 

   -- Client B ohne Transaktion: Muss warten
   UPDATE table1 SET data=22 WHERE ID_t1=2;

-- in Client A:
SELECT * FROM table1; 
COMMIT;

   -- Client B: Beobachten
   SELECT * FROM table1; -- Client B hat DS nach Transaktion geändert

-- in Client A:
SELECT * FROM table1; 





-- Demo: SELECT FOR UPDATE
-- -----------------------

-- in Client A:
INSERT INTO table1 (ID_t1, data) VALUES (3, 500);    
BEGIN;
SELECT * FROM table1 WHERE ID_t1=3; -- SELECT sperrt DS nicht!

   -- Client B ohne Transaktion: Muss nicht warten
   UPDATE table1 SET data=data+1 WHERE ID_t1=3;

SELECT * FROM table1 WHERE ID_t1=3 FOR UPDATE; -- SELECT sperrt DS bereits

   -- Client B ohne Transaktion: Muss nun warten
   UPDATE table1 SET data=302 WHERE ID_t1=3;

-- in Client A:
UPDATE table1 SET data=303 WHERE ID_t1=3;
SELECT * FROM table1; 
COMMIT;

SELECT * FROM table1; -- Client B hat DS nach Transaktion geändert



-- Demo: SELECT LOCK IN SHARE MODE
-- -------------------------------

-- in Client A:
INSERT INTO table1 (ID_t1, data) VALUES (4, 4000);
BEGIN;
SELECT * FROM table1 WHERE ID_t1=4 LOCK IN SHARE MODE; -- DS kann weiter ausgelesen werden
INSERT INTO table1 (ID_t1, data) VALUES (5, 5000);

   -- Client B ohne Transaktion: 
   SELECT * FROM table1 WHERE ID_t1=4 LOCK IN SHARE MODE; -- Möglich, da L-I-S-M
   UPDATE table1 SET data=12 WHERE ID_t1=1; -- Möglich: Neue Daten in table1, da DS (ID_t1=1) nicht gesperrt!
   SELECT * FROM table1; 
   UPDATE table1 SET data=2001 WHERE ID_t1=4; -- Muss nun warten

SELECT * FROM table1; -- Anzeige der "veralteten" Arbeitskopie!
UPDATE table1 SET data=20002 WHERE ID_t1=4;
INSERT INTO table1 (ID_t1, data) VALUES (6, 600);
DELETE FROM table1 WHERE ID_t1=2;
COMMIT;

   -- Client B: Beobachten
   SELECT * FROM table1; 

-- in Client A:
SELECT * FROM table1; 
